// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: '0.0.1',
  build: 1,

  // apiBaseUrl: 'http://192.168.10.125:8000/api/', // Raju Bhai

  // apiBaseUrl: 'http://192.168.10.121:8000/api/', // Salman Bhai 

  apiBaseUrl: 'http://dss.aws.simecsystem.com/api/', // Live URL
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
