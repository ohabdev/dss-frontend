import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
// import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'DSS';
 //  translateService: TranslateService;

  constructor(private titleService: Title,activatedRoute:ActivatedRoute, router:Router) {
    // this.translateService = translate;
    const appTitle = this.titleService.getTitle();
    router
      .events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          let child = activatedRoute.firstChild;
          while (child.firstChild) {
            child = child.firstChild;
          }
          if (child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          }
          return appTitle;
        })
      ).subscribe((title: string) => {
        if(!title){
          this.titleService.setTitle(this.title);
        }else{
          this.titleService.setTitle(title+' :: '+this.title);
        }
      });

       
      
  }
  ngOnInit() {
      // this.translateService.addLangs(['en', 'nl']);
      // this.translateService.setDefaultLang('en');
      // this.translateService.use('en');
  }

   
  

}
