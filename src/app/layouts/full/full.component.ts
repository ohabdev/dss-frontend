import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.css']
})
export class FullComponent implements OnInit {
  
  public isHome: boolean = false;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.isHome = this.router.url.indexOf('home') > -1;
  }

}
