import { Routes, RouterModule } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { AuthGuard } from './guard/auth.guard';
import { ConfigResolver } from './shared/resolver/config.resolver';
import { BlankComponent } from './layouts/blank/blank.component';

export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    canActivate: [AuthGuard],
    resolve: { appConfig: ConfigResolver },
    children: [
      { path: '', loadChildren: () => import('./starter/starter.module').then(m => m.StarterModule) },
      { path: '', loadChildren: () => import('./modules/main.module').then(m => m.MainModule) }, 
    ]
  },
  
  {
    path: '',
    component: BlankComponent,
    resolve: { appConfig: ConfigResolver },
    children: [
      { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) }
    ]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];
