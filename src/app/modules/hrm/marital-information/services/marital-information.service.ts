import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaritalInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private MaritalInformation: any = null;
  private getMaritalInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('marital-status').toPromise();
  }

  createMaritalInformation(data: any): Promise<any> {
    return this.restangular.all('auth/marital-status').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleMaritalInformation($id): Promise<any> {
    return this.restangular.all('auth/marital-status').customGET($id).toPromise();
  }

  updateMaritalInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/marital-status',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteMaritalInformation($id): Promise<any> {
    return this.restangular.one('auth/marital-status/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
