import { TestBed } from '@angular/core/testing';

import { MaritalInformationService } from './marital-information.service';

describe('MaritalInformationService', () => {
  let service: MaritalInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MaritalInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
