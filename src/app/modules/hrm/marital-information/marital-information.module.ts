import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaritalInformationListComponent } from './marital-information-list/marital-information-list.component';
import { CreateMaritalInformationComponent } from './create-marital-information/create-marital-information.component';
import { EditMaritalInformationComponent } from './edit-marital-information/edit-marital-information.component';


const routes: Routes = [{
  path: '',
  component: MaritalInformationListComponent,
  data: {title: 'Marital Information List'} 
},
{
  path: 'create',
  component: CreateMaritalInformationComponent,
  data: {title: 'Create Marital Information'} 
},
{
  path: 'edit/:id',
  component: EditMaritalInformationComponent,
  data: {title: 'Edit Marital Information'} 
}
];


@NgModule({
  declarations: [MaritalInformationListComponent, CreateMaritalInformationComponent, EditMaritalInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class MaritalInformationModule { }
