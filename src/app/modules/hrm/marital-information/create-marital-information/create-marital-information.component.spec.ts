import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMaritalInformationComponent } from './create-marital-information.component';

describe('CreateMaritalInformationComponent', () => {
  let component: CreateMaritalInformationComponent;
  let fixture: ComponentFixture<CreateMaritalInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMaritalInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMaritalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
