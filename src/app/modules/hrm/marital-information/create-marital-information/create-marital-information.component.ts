import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ MaritalInformationService } from '../services/marital-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-marital-information',
  templateUrl: './create-marital-information.component.html',
  styleUrls: ['./create-marital-information.component.css']
})
export class CreateMaritalInformationComponent implements OnInit {
  private MaritalInformation: MaritalInformationService;
  private Toasty: TostyService;
  public MaritalInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(maritalInformationService: MaritalInformationService, private toasty: TostyService, private location: Location) { 
    this.MaritalInformation = maritalInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createMaritalInformationOnSubmit(maritalInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(maritalInformationFrm);

    if (maritalInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.MaritalInformation.createMaritalInformation(this.MaritalInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
