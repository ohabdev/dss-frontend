import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { MaritalInformationService } from '../services/marital-information.service';

@Component({
  selector: 'app-edit-marital-information',
  templateUrl: './edit-marital-information.component.html',
  styleUrls: ['./edit-marital-information.component.css']
})
export class EditMaritalInformationComponent implements OnInit {
  private MaritalInformation: MaritalInformationService;
  private Toasty: TostyService;
  public MaritalInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    maritalInformationService: MaritalInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.MaritalInformation = maritalInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.MaritalInformation.singleMaritalInformation(this.id).then((res) => {
          console.log(res.data);
          this.MaritalInformationData = res.data;
          console.log(this.MaritalInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editMaritalInformationOnSubmit(maritalInformationFrm: NgForm) {
    this.submitted = true;
    if (maritalInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.MaritalInformation.updateMaritalInformation(this.id, this.MaritalInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
