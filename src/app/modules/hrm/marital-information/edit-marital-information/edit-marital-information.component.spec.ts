import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMaritalInformationComponent } from './edit-marital-information.component';

describe('EditMaritalInformationComponent', () => {
  let component: EditMaritalInformationComponent;
  let fixture: ComponentFixture<EditMaritalInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMaritalInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMaritalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
