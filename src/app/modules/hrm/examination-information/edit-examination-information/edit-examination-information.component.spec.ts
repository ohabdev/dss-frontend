import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditExaminationInformationComponent } from './edit-examination-information.component';

describe('EditExaminationInformationComponent', () => {
  let component: EditExaminationInformationComponent;
  let fixture: ComponentFixture<EditExaminationInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditExaminationInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExaminationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
