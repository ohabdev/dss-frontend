import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { ExaminationInformationService } from '../services/examination-information.service';

@Component({
  selector: 'app-edit-examination-information',
  templateUrl: './edit-examination-information.component.html',
  styleUrls: ['./edit-examination-information.component.css']
})
export class EditExaminationInformationComponent implements OnInit {
  private ExaminationInformation: ExaminationInformationService;
  private Toasty: TostyService;
  public ExaminationInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    examinationInformationService: ExaminationInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.ExaminationInformation = examinationInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.ExaminationInformation.singleExaminationInformation(this.id).then((res) => {
          console.log(res.data);
          this.ExaminationInformationData = res.data;
          console.log(this.ExaminationInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editExaminationInformationOnSubmit(examinationInformationFrm: NgForm) {
    this.submitted = true;
    if (examinationInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.ExaminationInformation.updateExaminationInformation(this.id, this.ExaminationInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
