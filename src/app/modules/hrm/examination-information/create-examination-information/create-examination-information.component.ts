import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ ExaminationInformationService } from '../services/examination-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-examination-information',
  templateUrl: './create-examination-information.component.html',
  styleUrls: ['./create-examination-information.component.css']
})
export class CreateExaminationInformationComponent implements OnInit {
  private ExaminationInformation: ExaminationInformationService;
  private Toasty: TostyService;
  public ExaminationInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(examinationInformationService: ExaminationInformationService, private toasty: TostyService, private location: Location) { 
    this.ExaminationInformation = examinationInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createExaminationInformationOnSubmit(examinationInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(examinationInformationFrm);

    if (examinationInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.ExaminationInformation.createExaminationInformation(this.ExaminationInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
