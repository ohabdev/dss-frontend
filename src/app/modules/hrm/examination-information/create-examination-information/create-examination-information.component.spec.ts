import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateExaminationInformationComponent } from './create-examination-information.component';

describe('CreateExaminationInformationComponent', () => {
  let component: CreateExaminationInformationComponent;
  let fixture: ComponentFixture<CreateExaminationInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateExaminationInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateExaminationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
