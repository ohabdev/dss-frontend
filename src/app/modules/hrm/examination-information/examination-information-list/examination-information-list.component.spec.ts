import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationInformationListComponent } from './examination-information-list.component';

describe('ExaminationInformationListComponent', () => {
  let component: ExaminationInformationListComponent;
  let fixture: ComponentFixture<ExaminationInformationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationInformationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationInformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
