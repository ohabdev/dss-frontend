import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExaminationInformationListComponent } from './examination-information-list/examination-information-list.component';
import { CreateExaminationInformationComponent } from './create-examination-information/create-examination-information.component';
import { EditExaminationInformationComponent } from './edit-examination-information/edit-examination-information.component';


const routes: Routes = [{
  path: '',
  component: ExaminationInformationListComponent,
  data: {title: 'Examination Information List'} 
},
{
  path: 'create',
  component: CreateExaminationInformationComponent,
  data: {title: 'Create Examination Information'} 
},
{
  path: 'edit/:id',
  component: EditExaminationInformationComponent,
  data: {title: 'Edit Examination Information'} 
}
];


@NgModule({
  declarations: [ExaminationInformationListComponent, CreateExaminationInformationComponent, EditExaminationInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class ExaminationInformationModule { }
