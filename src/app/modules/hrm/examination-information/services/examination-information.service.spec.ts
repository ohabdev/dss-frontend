import { TestBed } from '@angular/core/testing';

import { ExaminationInformationService } from './examination-information.service';

describe('ExaminationInformationService', () => {
  let service: ExaminationInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExaminationInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
