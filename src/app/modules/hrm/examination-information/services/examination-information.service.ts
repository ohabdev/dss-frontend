import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExaminationInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private ExaminationInformation: any = null;
  private getExaminationInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('examination').toPromise();
  }

  createExaminationInformation(data: any): Promise<any> {
    return this.restangular.all('auth/examination').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleExaminationInformation($id): Promise<any> {
    return this.restangular.all('auth/examination').customGET($id).toPromise();
  }

  updateExaminationInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/examination',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteExaminationInformation($id): Promise<any> {
    return this.restangular.one('auth/examination/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
