import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { TravelTypeInformationService } from '../services/travel-type-information.service';

@Component({
  selector: 'app-edit-travel-type-information',
  templateUrl: './edit-travel-type-information.component.html',
  styleUrls: ['./edit-travel-type-information.component.css']
})
export class EditTravelTypeInformationComponent implements OnInit {
  private TravelTypeInformation: TravelTypeInformationService;
  private Toasty: TostyService;
  public TravelTypeInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    travelTypeInformationService: TravelTypeInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.TravelTypeInformation = travelTypeInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.TravelTypeInformation.singleTravelTypeInformation(this.id).then((res) => {
          console.log(res.data);
          this.TravelTypeInformationData = res.data;
          console.log(this.TravelTypeInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editTravelTypeInformationOnSubmit(travelTypeInformationFrm: NgForm) {
    this.submitted = true;
    if (travelTypeInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.TravelTypeInformation.updateTravelTypeInformation(this.id, this.TravelTypeInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
