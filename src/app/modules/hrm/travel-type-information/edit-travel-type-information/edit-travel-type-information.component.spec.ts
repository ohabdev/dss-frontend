import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTravelTypeInformationComponent } from './edit-travel-type-information.component';

describe('EditTravelTypeInformationComponent', () => {
  let component: EditTravelTypeInformationComponent;
  let fixture: ComponentFixture<EditTravelTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTravelTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTravelTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
