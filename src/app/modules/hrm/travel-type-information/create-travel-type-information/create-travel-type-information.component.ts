import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ TravelTypeInformationService } from '../services/travel-type-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-travel-type-information',
  templateUrl: './create-travel-type-information.component.html',
  styleUrls: ['./create-travel-type-information.component.css']
})
  export class CreateTravelTypeInformationComponent implements OnInit {
    private TravelTypeInformation: TravelTypeInformationService;
    private Toasty: TostyService;
    public TravelTypeInformationData = {
      name: '',
      name_bn: '',
      status: 1
    };
    public submitted: boolean = false;
    router: any;
    private _location: any;
  
    constructor(travelTypeInformationService: TravelTypeInformationService, private toasty: TostyService, private location: Location) { 
      this.TravelTypeInformation = travelTypeInformationService;
      this.Toasty = toasty;
      this._location = location;
    }
    ngOnInit(): void {
    }
  
  
    createTravelTypeInformationOnSubmit(travelTypeInformationFrm: NgForm) {
      this.submitted = true;
  
      if (travelTypeInformationFrm.invalid) {
        return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
      }
  
      this.TravelTypeInformation.createTravelTypeInformation(this.TravelTypeInformationData).then(() => {
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success',);
      })
      .catch((res) => { 
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!',);
      });
    }
     
  
  }
