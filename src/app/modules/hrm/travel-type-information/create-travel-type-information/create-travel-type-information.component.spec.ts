import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTravelTypeInformationComponent } from './create-travel-type-information.component';

describe('CreateTravelTypeInformationComponent', () => {
  let component: CreateTravelTypeInformationComponent;
  let fixture: ComponentFixture<CreateTravelTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTravelTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTravelTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
