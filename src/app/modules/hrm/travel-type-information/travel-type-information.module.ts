import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TravelTypeInformationListComponent } from './travel-type-information-list/travel-type-information-list.component';
import { CreateTravelTypeInformationComponent } from './create-travel-type-information/create-travel-type-information.component';
import { EditTravelTypeInformationComponent } from './edit-travel-type-information/edit-travel-type-information.component';


const routes: Routes = [{
  path: '',
  component: TravelTypeInformationListComponent,
  data: {title: 'Travel Type Information List'} 
},
{
  path: 'create',
  component: CreateTravelTypeInformationComponent,
  data: {title: 'Create Travel Type Information'} 
},
{
  path: 'edit/:id',
  component: EditTravelTypeInformationComponent,
  data: {title: 'Edit Travel Type Information'} 
}
];


@NgModule({
  declarations: [TravelTypeInformationListComponent, CreateTravelTypeInformationComponent, EditTravelTypeInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class TravelTypeInformationModule { }
