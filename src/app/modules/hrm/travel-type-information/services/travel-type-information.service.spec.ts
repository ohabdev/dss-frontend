import { TestBed } from '@angular/core/testing';

import { TravelTypeInformationService } from './travel-type-information.service';

describe('TravelTypeInformationService', () => {
  let service: TravelTypeInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TravelTypeInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
