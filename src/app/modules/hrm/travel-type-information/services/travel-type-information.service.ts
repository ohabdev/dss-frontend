import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TravelTypeInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private TravelTypeInformation: any = null;
  private getTravelTypeInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('travel-type').toPromise();
  }

  createTravelTypeInformation(data: any): Promise<any> {
    return this.restangular.all('auth/travel-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleTravelTypeInformation($id): Promise<any> {
    return this.restangular.all('auth/travel-type').customGET($id).toPromise();
  }

  updateTravelTypeInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/travel-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteTravelTypeInformation($id): Promise<any> {
    return this.restangular.one('auth/travel-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
