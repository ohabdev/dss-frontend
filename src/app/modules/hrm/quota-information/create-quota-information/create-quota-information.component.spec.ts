import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQuotaInformationComponent } from './create-quota-information.component';

describe('CreateQuotaInformationComponent', () => {
  let component: CreateQuotaInformationComponent;
  let fixture: ComponentFixture<CreateQuotaInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateQuotaInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQuotaInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
