import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ QuotaInformationService } from '../services/quota-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-quota-information',
  templateUrl: './create-quota-information.component.html',
  styleUrls: ['./create-quota-information.component.css']
})
export class CreateQuotaInformationComponent implements OnInit {
  private QuotaInformation: QuotaInformationService;
  private Toasty: TostyService;
  public QuotaInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(quotaInformationService: QuotaInformationService, private toasty: TostyService, private location: Location) { 
    this.QuotaInformation = quotaInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createQuotaInformationOnSubmit(quotaInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(quotaInformationFrm);

    if (quotaInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.QuotaInformation.createQuotaInformation(this.QuotaInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
