import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuotaListComponent } from './quota-list/quota-list.component';
import { CreateQuotaInformationComponent } from './create-quota-information/create-quota-information.component';
import { EditQuotaInformationComponent } from './edit-quota-information/edit-quota-information.component';
// Shared Components
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [{
  path: '',
  component: QuotaListComponent,
  data: {title: 'Quota Information List'} 
},
{
  path: 'create',
  component: CreateQuotaInformationComponent,
  data: {title: 'Create Quota Information'} 
},
{
  path: 'edit/:id',
  component: EditQuotaInformationComponent,
  data: {title: 'Edit Quota Information'} 
}
];


@NgModule({
  declarations: [QuotaListComponent, CreateQuotaInformationComponent, EditQuotaInformationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ],
})
export class QuotaInformationModule { }
