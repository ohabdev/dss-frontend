import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuotaInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private QuotaInformation: any = null;
  private getQuotaInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('quota-information').toPromise();
  }

  createQuotaInformation(data: any): Promise<any> {
    return this.restangular.all('auth/quota-information').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleQuotaInformation($id): Promise<any> {
    return this.restangular.all('auth/quota-information').customGET($id).toPromise();
  }

  updateQuotaInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/quota-information',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteQuotaInformation($id): Promise<any> {
    return this.restangular.one('auth/quota-information/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
