import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { QuotaInformationService } from '../services/quota-information.service';

@Component({
  selector: 'app-edit-quota-information',
  templateUrl: './edit-quota-information.component.html',
  styleUrls: ['./edit-quota-information.component.css'],
})
export class EditQuotaInformationComponent implements OnInit {
  private QuotaInformation: QuotaInformationService;
  private Toasty: TostyService;
  public QuotaInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    quotaInformationService: QuotaInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.QuotaInformation = quotaInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.QuotaInformation.singleQuotaInformation(this.id).then((res) => {
          console.log(res.data);
          this.QuotaInformationData = res.data;
          console.log(this.QuotaInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editQuotaInformationOnSubmit(quotaInformationFrm: NgForm) {
    this.submitted = true;
    if (quotaInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.QuotaInformation.updateQuotaInformation(this.id, this.QuotaInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
