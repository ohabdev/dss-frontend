import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpazilaInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private UpazilaInformation: any = null;
  private getUpazilaInformation: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('upazila?page='+page).toPromise();
  }

  getDivisionList() {
    return this.restangular.all('auth').customGET('division').toPromise();
  }

  getDistrictList() {
    return this.restangular.all('auth').customGET('district/list').toPromise();
  }


  createUpazilaInformation(data: any): Promise<any> {
    return this.restangular.all('auth/upazila').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleUpazilaInformation($id): Promise<any> {
    return this.restangular.all('auth/upazila').customGET($id).toPromise();
  }

  updateUpazilaInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/upazila',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteUpazilaInformation($id): Promise<any> {
    return this.restangular.one('auth/upazila/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
