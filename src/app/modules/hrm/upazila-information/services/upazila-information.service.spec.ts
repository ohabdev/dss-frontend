import { TestBed } from '@angular/core/testing';

import { UpazilaInformationService } from './upazila-information.service';

describe('UpazilaInformationService', () => {
  let service: UpazilaInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpazilaInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
