import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpazilaInformationListComponent } from './upazila-information-list/upazila-information-list.component';
import { CreateUpazilaInformationComponent } from './create-upazila-information/create-upazila-information.component';
import { EditUpazilaInformationComponent } from './edit-upazila-information/edit-upazila-information.component';


const routes: Routes = [{
  path: '',
  component: UpazilaInformationListComponent,
  data: {title: 'Upazila Information List'} 
},
{
  path: 'create',
  component: CreateUpazilaInformationComponent,
  data: {title: 'Create Upazila Information'} 
},
{
  path: 'edit/:id',
  component: EditUpazilaInformationComponent,
  data: {title: 'Edit Upazila Information'} 
}
];


@NgModule({
  declarations: [UpazilaInformationListComponent, CreateUpazilaInformationComponent, EditUpazilaInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class UpazilaInformationModule { }
