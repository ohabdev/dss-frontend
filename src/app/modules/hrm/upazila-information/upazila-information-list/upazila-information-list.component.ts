import { Component, OnInit } from '@angular/core';
import { UpazilaInformationService } from '../services/upazila-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { remove } from 'lodash';

@Component({
  selector: 'app-upazila-information-list',
  templateUrl: './upazila-information-list.component.html',
  styleUrls: ['./upazila-information-list.component.css'],
})
export class UpazilaInformationListComponent implements OnInit {
  public isLoading: boolean = true;
  public items: any = [];
  public pagination: any = [];
  Toasty: any;

  constructor(
    private upazilaInformationService: UpazilaInformationService,
    private toasty: TostyService
  ) {
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getPageDataFn();
  }

  getPageDataFn(page: number = 1) {
    this.isLoading = true;
    this.upazilaInformationService.getList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
      this.isLoading = false;
    });
  }

  deleteUpazilaInformation(id: any) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.upazilaInformationService
          .deleteUpazilaInformation(id)
          .then((res) => {
            this.getPageDataFn();
            Swal.fire({
              icon: 'success',
              title: 'Deleted!',
              text: 'Your imaginary file has been deleted.',
              showCancelButton: false,
              showConfirmButton: false,
              timer: 1000,
            });
          });
      }
    });
  }
}
