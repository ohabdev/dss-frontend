import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpazilaInformationComponent } from './create-upazila-information.component';

describe('CreateUpazilaInformationComponent', () => {
  let component: CreateUpazilaInformationComponent;
  let fixture: ComponentFixture<CreateUpazilaInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateUpazilaInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpazilaInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
