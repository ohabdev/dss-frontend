import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import { UpazilaInformationService } from '../services/upazila-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-upazila-information',
  templateUrl: './create-upazila-information.component.html',
  styleUrls: ['./create-upazila-information.component.css'],
})
export class CreateUpazilaInformationComponent implements OnInit {
  public isLoading: boolean = true;
  private UpazilaInformation: UpazilaInformationService;
  private Toasty: TostyService;
  public UpazilaInformationData = {
    name: '',
    name_bn: '',
    district_id: '',
    status: 1,
  };
  public district_list: any = [];
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(
    upazilaInformationService: UpazilaInformationService,
    private toasty: TostyService,
    private location: Location
  ) {
    this.UpazilaInformation = upazilaInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.UpazilaInformation.getDistrictList().then((res) => {
      this.district_list = res.data;
      this.isLoading = false;
    });
  }

  createUpazilaInformationOnSubmit(upazilaInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(upazilaInformationFrm);

    if (upazilaInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.isLoading = true;
    this.UpazilaInformation.createUpazilaInformation(
      this.UpazilaInformationData
    )
      .then(() => {
        this.isLoading = false;
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
