import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { UpazilaInformationService } from '../services/upazila-information.service';

@Component({
  selector: 'app-edit-upazila-information',
  templateUrl: './edit-upazila-information.component.html',
  styleUrls: ['./edit-upazila-information.component.css']
})
export class EditUpazilaInformationComponent implements OnInit {
  public isLoading: boolean = true;
  private UpazilaInformation: UpazilaInformationService;
  private Toasty: TostyService;
  public UpazilaInformationData = {
    name: '',
    name_bn: '',
    district_id: '',
    status: 1
  };
  public district_list: any = []; 
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    upazilaInformationService: UpazilaInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.UpazilaInformation = upazilaInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.UpazilaInformation.singleUpazilaInformation(this.id).then((res) => {
          this.UpazilaInformationData = res.data;
          this.isLoading = false;
        }).catch((res) => { 
          this.isLoading = false;
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });

        this.UpazilaInformation.getDistrictList().then((res) => {
          this.district_list = res.data;
        });
        
      }

    });

  }

  editUpazilaInformationOnSubmit(upazilaInformationFrm: NgForm) {
    this.submitted = true;
    if (upazilaInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }
    this.isLoading = true;
    this.UpazilaInformation.updateUpazilaInformation(this.id, this.UpazilaInformationData)
      .then(() => {
        this.isLoading = false;
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        this.isLoading = false;
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
