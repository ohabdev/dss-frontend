import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUpazilaInformationComponent } from './edit-upazila-information.component';

describe('EditUpazilaInformationComponent', () => {
  let component: EditUpazilaInformationComponent;
  let fixture: ComponentFixture<EditUpazilaInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUpazilaInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUpazilaInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
