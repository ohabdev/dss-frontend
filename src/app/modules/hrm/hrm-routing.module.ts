import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'blood', loadChildren: () => import('./blood-group/blood-group.module').then(m => m.BloodGroupModule) },
      { path: 'quota', loadChildren: () => import('./quota-information/quota-information.module').then(m => m.QuotaInformationModule) },  
      { path: 'designation', loadChildren: () => import('./designation-information/designation-information.module').then(m => m.DesignationInformationModule) }, 
      { path: 'section', loadChildren: () => import('./section-information/section-information.module').then(m => m.SectionInformationModule) }, 
      { path: 'department', loadChildren: () => import('./department-information/department-information.module').then(m => m.DepartmentInformationModule) }, 
      { path: 'rank', loadChildren: () => import('./rank-information/rank-information.module').then(m => m.RankInformationModule) }, 
      { path: 'division', loadChildren: () => import('./division-information/division-information.module').then(m => m.DivisionInformationModule) }, 
      { path: 'district', loadChildren: () => import('./district-information/district-information.module').then(m => m.DistrictInformationModule) }, 
      { path: 'upazila', loadChildren: () => import('./upazila-information/upazila-information.module').then(m => m.UpazilaInformationModule) }, 
      { path: 'marital-status', loadChildren: () => import('./marital-information/marital-information.module').then(m => m.MaritalInformationModule) }, 
      { path: 'gender', loadChildren: () => import('./gender-information/gender-information.module').then(m => m.GenderInformationModule) }, 
      { path: 'examination', loadChildren: () => import('./examination-information/examination-information.module').then(m => m.ExaminationInformationModule) }, 
      { path: 'salary-grade', loadChildren: () => import('./salary-information/salary-information.module').then(m => m.SalaryInformationModule) }, 
      { path: 'complaint-type', loadChildren: () => import('./complaint-type-information/complaint-type-information.module').then(m => m.ComplaintTypeInformationModule) }, 
      { path: 'posting-type', loadChildren: () => import('./posting-type-information/posting-type-information.module').then(m => m.PostingTypeInformationModule) }, 
      { path: 'travel-type', loadChildren: () => import('./travel-type-information/travel-type-information.module').then(m => m.TravelTypeInformationModule) }, 
      { path: 'vehicle-engine-type', loadChildren: () => import('./vehicle-engine-type-information/vehicle-engine-type-information.module').then(m => m.VehicleEngineTypeInformationModule) }, 
      
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HRMRoutingModule { }