import { Component, OnInit } from '@angular/core';
import{ DesignationInformationService } from '../services/designation-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-designation-information-list',
  templateUrl: './designation-information-list.component.html',
  styleUrls: ['./designation-information-list.component.css']
})
export class DesignationInformationListComponent  implements OnInit {

  public items: any = []; 
  Toasty: any;


  constructor(private designationInformationService: DesignationInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.designationInformationService.getList().then((res) => {
      this.items = res.data.data;
    });
  }
  

  deleteDesignationInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.designationInformationService.deleteDesignationInformation(id).then((res) => {
          this.designationInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
