import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesignationInformationListComponent } from './designation-information-list/designation-information-list.component';
import { CreateDesignationInformationComponent } from './create-designation-information/create-designation-information.component';
import { EditDesignationInformationComponent } from './edit-designation-information/edit-designation-information.component';

const routes: Routes = [{
  path: '',
  component: DesignationInformationListComponent,
  data: {title: 'Designation Information List'} 
},
{
  path: 'create',
  component: CreateDesignationInformationComponent,
  data: {title: 'Create Designation Information'} 
},
{
  path: 'edit/:id',
  component: EditDesignationInformationComponent,
  data: {title: 'Edit Designation Information'} 
}
];


@NgModule({
  declarations: [DesignationInformationListComponent, CreateDesignationInformationComponent, EditDesignationInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DesignationInformationModule { }
