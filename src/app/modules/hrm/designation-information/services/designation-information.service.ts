import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DesignationInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private DesignationInformation: any = null;
  private getDesignationInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('designation').toPromise();
  }

  createDesignationInformation(data: any): Promise<any> {
    return this.restangular.all('auth/designation').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDesignationInformation($id): Promise<any> {
    return this.restangular.all('auth/designation').customGET($id).toPromise();
  }

  updateDesignationInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/designation',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDesignationInformation($id): Promise<any> {
    return this.restangular.one('auth/designation/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
