import { TestBed } from '@angular/core/testing';

import { DesignationInformationService } from './designation-information.service';

describe('DesignationInformationService', () => {
  let service: DesignationInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DesignationInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
