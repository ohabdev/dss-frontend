import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DesignationInformationService } from '../services/designation-information.service';

@Component({
  selector: 'app-edit-designation-information',
  templateUrl: './edit-designation-information.component.html',
  styleUrls: ['./edit-designation-information.component.css']
})
export class EditDesignationInformationComponent implements OnInit {
  private DesignationInformation: DesignationInformationService;
  private Toasty: TostyService;
  public DesignationInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    designationInformationService: DesignationInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.DesignationInformation = designationInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.DesignationInformation.singleDesignationInformation(this.id).then((res) => {
          console.log(res.data);
          this.DesignationInformationData = res.data;
          console.log(this.DesignationInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editDesignationInformationOnSubmit(designationInformationFrm: NgForm) {
    this.submitted = true;
    if (designationInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.DesignationInformation.updateDesignationInformation(this.id, this.DesignationInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
