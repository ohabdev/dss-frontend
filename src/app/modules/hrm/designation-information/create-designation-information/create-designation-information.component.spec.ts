import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDesignationInformationComponent } from './create-designation-information.component';

describe('CreateDesignationInformationComponent', () => {
  let component: CreateDesignationInformationComponent;
  let fixture: ComponentFixture<CreateDesignationInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDesignationInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDesignationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
