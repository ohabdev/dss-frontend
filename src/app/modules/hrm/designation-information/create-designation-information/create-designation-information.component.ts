import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ DesignationInformationService } from '../services/designation-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-designation-information',
  templateUrl: './create-designation-information.component.html',
  styleUrls: ['./create-designation-information.component.css']
})
export class CreateDesignationInformationComponent implements OnInit {
  private DesignationInformation: DesignationInformationService;
  private Toasty: TostyService;
  public DesignationInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(designationInformationService: DesignationInformationService, private toasty: TostyService, private location: Location) { 
    this.DesignationInformation = designationInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createDesignationInformationOnSubmit(designationInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(designationInformationFrm);

    if (designationInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.DesignationInformation.createDesignationInformation(this.DesignationInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
