import { TestBed } from '@angular/core/testing';

import { SalaryInformationService } from './salary-information.service';

describe('SalaryInformationService', () => {
  let service: SalaryInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalaryInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
