import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private SalaryInformation: any = null;
  private getSalaryInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('salary-grade').toPromise();
  }

  createSalaryInformation(data: any): Promise<any> {
    return this.restangular.all('auth/salary-grade').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleSalaryInformation($id): Promise<any> {
    return this.restangular.all('auth/salary-grade').customGET($id).toPromise();
  }

  updateSalaryInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/salary-grade',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteSalaryInformation($id): Promise<any> {
    return this.restangular.one('auth/salary-grade/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
