import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSalaryInformationComponent } from './create-salary-information.component';

describe('CreateSalaryInformationComponent', () => {
  let component: CreateSalaryInformationComponent;
  let fixture: ComponentFixture<CreateSalaryInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSalaryInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSalaryInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
