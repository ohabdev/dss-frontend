import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ SalaryInformationService } from '../services/salary-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-salary-information',
  templateUrl: './create-salary-information.component.html',
  styleUrls: ['./create-salary-information.component.css']
})
export class CreateSalaryInformationComponent implements OnInit {
  private SalaryInformation: SalaryInformationService;
  private Toasty: TostyService;
  public SalaryInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(salaryInformationService: SalaryInformationService, private toasty: TostyService, private location: Location) { 
    this.SalaryInformation = salaryInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createSalaryInformationOnSubmit(salaryInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(salaryInformationFrm);

    if (salaryInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.SalaryInformation.createSalaryInformation(this.SalaryInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
