import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { SalaryInformationService } from '../services/salary-information.service';

@Component({
  selector: 'app-edit-salary-information',
  templateUrl: './edit-salary-information.component.html',
  styleUrls: ['./edit-salary-information.component.css']
})
export class EditSalaryInformationComponent implements OnInit {
  private SalaryInformation: SalaryInformationService;
  private Toasty: TostyService;
  public SalaryInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    salaryInformationService: SalaryInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.SalaryInformation = salaryInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.SalaryInformation.singleSalaryInformation(this.id).then((res) => {
          console.log(res.data);
          this.SalaryInformationData = res.data;
          console.log(this.SalaryInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editSalaryInformationOnSubmit(salaryInformationFrm: NgForm) {
    this.submitted = true;
    if (salaryInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.SalaryInformation.updateSalaryInformation(this.id, this.SalaryInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
