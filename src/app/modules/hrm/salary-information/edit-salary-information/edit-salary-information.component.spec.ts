import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSalaryInformationComponent } from './edit-salary-information.component';

describe('EditSalaryInformationComponent', () => {
  let component: EditSalaryInformationComponent;
  let fixture: ComponentFixture<EditSalaryInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSalaryInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSalaryInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
