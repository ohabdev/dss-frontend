import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SalaryInformationListComponent } from './salary-information-list/salary-information-list.component';
import { CreateSalaryInformationComponent } from './create-salary-information/create-salary-information.component';
import { EditSalaryInformationComponent } from './edit-salary-information/edit-salary-information.component';


const routes: Routes = [{
  path: '',
  component: SalaryInformationListComponent,
  data: {title: 'Salary Information List'} 
},
{
  path: 'create',
  component: CreateSalaryInformationComponent,
  data: {title: 'Create Salary Information'} 
},
{
  path: 'edit/:id',
  component: EditSalaryInformationComponent,
  data: {title: 'Edit Salary Information'} 
}
];


@NgModule({
  declarations: [SalaryInformationListComponent, CreateSalaryInformationComponent, EditSalaryInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class SalaryInformationModule { }
