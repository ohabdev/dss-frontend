import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRankInformationComponent } from './create-rank-information.component';

describe('CreateRankInformationComponent', () => {
  let component: CreateRankInformationComponent;
  let fixture: ComponentFixture<CreateRankInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateRankInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRankInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
