import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ RankInformationService } from '../services/rank-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-rank-information',
  templateUrl: './create-rank-information.component.html',
  styleUrls: ['./create-rank-information.component.css']
})
export class CreateRankInformationComponent implements OnInit {
  private RankInformation: RankInformationService;
  private Toasty: TostyService;
  public RankInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(rankInformationService: RankInformationService, private toasty: TostyService, private location: Location) { 
    this.RankInformation = rankInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createRankInformationOnSubmit(rankInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(rankInformationFrm);

    if (rankInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.RankInformation.createRankInformation(this.RankInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
