import { TestBed } from '@angular/core/testing';

import { RankInformationService } from './rank-information.service';

describe('RankInformationService', () => {
  let service: RankInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RankInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
