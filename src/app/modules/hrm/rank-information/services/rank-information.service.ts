import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RankInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private RankInformation: any = null;
  private getRankInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('rank').toPromise();
  }

  createRankInformation(data: any): Promise<any> {
    return this.restangular.all('auth/rank').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleRankInformation($id): Promise<any> {
    return this.restangular.all('auth/rank').customGET($id).toPromise();
  }

  updateRankInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/rank',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteRankInformation($id): Promise<any> {
    return this.restangular.one('auth/rank/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
