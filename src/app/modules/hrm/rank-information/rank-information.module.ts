import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RankInformationListComponent } from './rank-information-list/rank-information-list.component';
import { CreateRankInformationComponent } from './create-rank-information/create-rank-information.component';
import { EditRankInformationComponent } from './edit-rank-information/edit-rank-information.component';


const routes: Routes = [{
  path: '',
  component: RankInformationListComponent,
  data: {title: 'Rank Information List'} 
},
{
  path: 'create',
  component: CreateRankInformationComponent,
  data: {title: 'Create Rank Information'} 
},
{
  path: 'edit/:id',
  component: EditRankInformationComponent,
  data: {title: 'Edit Rank Information'} 
}
];


@NgModule({
  declarations: [RankInformationListComponent, CreateRankInformationComponent, EditRankInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class RankInformationModule { }
