import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { RankInformationService } from '../services/rank-information.service';

@Component({
  selector: 'app-edit-rank-information',
  templateUrl: './edit-rank-information.component.html',
  styleUrls: ['./edit-rank-information.component.css']
})
export class EditRankInformationComponent implements OnInit {
  private RankInformation: RankInformationService;
  private Toasty: TostyService;
  public RankInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    rankInformationService: RankInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.RankInformation = rankInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.RankInformation.singleRankInformation(this.id).then((res) => {
          console.log(res.data);
          this.RankInformationData = res.data;
          console.log(this.RankInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editRankInformationOnSubmit(rankInformationFrm: NgForm) {
    this.submitted = true;
    if (rankInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.RankInformation.updateRankInformation(this.id, this.RankInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
