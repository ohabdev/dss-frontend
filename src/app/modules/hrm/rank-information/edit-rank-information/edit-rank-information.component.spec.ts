import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRankInformationComponent } from './edit-rank-information.component';

describe('EditRankInformationComponent', () => {
  let component: EditRankInformationComponent;
  let fixture: ComponentFixture<EditRankInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditRankInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRankInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
