import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DivisionInformationListComponent } from './division-information-list/division-information-list.component';
import { CreateDivisionInformationComponent } from './create-division-information/create-division-information.component';
import { EditDivisionInformationComponent } from './edit-division-information/edit-division-information.component';


const routes: Routes = [{
  path: '',
  component: DivisionInformationListComponent,
  data: {title: 'Division Information List'} 
},
{
  path: 'create',
  component: CreateDivisionInformationComponent,
  data: {title: 'Create Division Information'} 
},
{
  path: 'edit/:id',
  component: EditDivisionInformationComponent,
  data: {title: 'Edit Division Information'} 
}
];


@NgModule({
  declarations: [DivisionInformationListComponent, CreateDivisionInformationComponent, EditDivisionInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class DivisionInformationModule { }
