import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DivisionInformationService } from '../services/division-information.service';

@Component({
  selector: 'app-edit-division-information',
  templateUrl: './edit-division-information.component.html',
  styleUrls: ['./edit-division-information.component.css']
})
export class EditDivisionInformationComponent implements OnInit {
  private DivisionInformation: DivisionInformationService;
  private Toasty: TostyService;
  public DivisionInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    divisionInformationService: DivisionInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.DivisionInformation = divisionInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.DivisionInformation.singleDivisionInformation(this.id).then((res) => {
          console.log(res.data);
          this.DivisionInformationData = res.data;
          console.log(this.DivisionInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editDivisionInformationOnSubmit(divisionInformationFrm: NgForm) {
    this.submitted = true;
    if (divisionInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.DivisionInformation.updateDivisionInformation(this.id, this.DivisionInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
