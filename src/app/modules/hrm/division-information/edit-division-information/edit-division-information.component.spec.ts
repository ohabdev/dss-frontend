import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDivisionInformationComponent } from './edit-division-information.component';

describe('EditDivisionInformationComponent', () => {
  let component: EditDivisionInformationComponent;
  let fixture: ComponentFixture<EditDivisionInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDivisionInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDivisionInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
