import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DivisionInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private DivisionInformation: any = null;
  private getDivisionInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('division').toPromise();
  }

  createDivisionInformation(data: any): Promise<any> {
    return this.restangular.all('auth/division').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDivisionInformation($id): Promise<any> {
    return this.restangular.all('auth/division').customGET($id).toPromise();
  }

  updateDivisionInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/division',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDivisionInformation($id): Promise<any> {
    return this.restangular.one('auth/division/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
