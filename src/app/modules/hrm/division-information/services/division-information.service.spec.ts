import { TestBed } from '@angular/core/testing';

import { DivisionInformationService } from './division-information.service';

describe('DivisionInformationService', () => {
  let service: DivisionInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DivisionInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
