import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionInformationListComponent } from './division-information-list.component';

describe('DivisionInformationListComponent', () => {
  let component: DivisionInformationListComponent;
  let fixture: ComponentFixture<DivisionInformationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionInformationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionInformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
