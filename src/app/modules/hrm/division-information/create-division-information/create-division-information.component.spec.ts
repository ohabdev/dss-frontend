import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDivisionInformationComponent } from './create-division-information.component';

describe('CreateDivisionInformationComponent', () => {
  let component: CreateDivisionInformationComponent;
  let fixture: ComponentFixture<CreateDivisionInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDivisionInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDivisionInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
