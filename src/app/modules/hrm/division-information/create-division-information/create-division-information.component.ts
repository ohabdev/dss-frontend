import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ DivisionInformationService } from '../services/division-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-division-information',
  templateUrl: './create-division-information.component.html',
  styleUrls: ['./create-division-information.component.css']
})
export class CreateDivisionInformationComponent implements OnInit {
  private DivisionInformation: DivisionInformationService;
  private Toasty: TostyService;
  public DivisionInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(divisionInformationService: DivisionInformationService, private toasty: TostyService, private location: Location) { 
    this.DivisionInformation = divisionInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createDivisionInformationOnSubmit(divisionInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(divisionInformationFrm);

    if (divisionInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.DivisionInformation.createDivisionInformation(this.DivisionInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
