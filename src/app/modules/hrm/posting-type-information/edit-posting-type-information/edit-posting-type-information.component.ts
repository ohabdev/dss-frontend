import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { PostingTypeInformationService } from '../services/posting-type-information.service';

@Component({
  selector: 'app-edit-posting-type-information',
  templateUrl: './edit-posting-type-information.component.html',
  styleUrls: ['./edit-posting-type-information.component.css']
})
export class EditPostingTypeInformationComponent implements OnInit {
  private PostingTypeInformation: PostingTypeInformationService;
  private Toasty: TostyService;
  public PostingTypeInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    postingTypeInformationService: PostingTypeInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.PostingTypeInformation = postingTypeInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.PostingTypeInformation.singlePostingTypeInformation(this.id).then((res) => {
          console.log(res.data);
          this.PostingTypeInformationData = res.data;
          console.log(this.PostingTypeInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editPostingTypeInformationOnSubmit(postingTypeInformationFrm: NgForm) {
    this.submitted = true;
    if (postingTypeInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.PostingTypeInformation.updatePostingTypeInformation(this.id, this.PostingTypeInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
