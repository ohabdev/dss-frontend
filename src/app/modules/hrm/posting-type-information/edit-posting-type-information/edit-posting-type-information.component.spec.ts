import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPostingTypeInformationComponent } from './edit-posting-type-information.component';

describe('EditPostingTypeInformationComponent', () => {
  let component: EditPostingTypeInformationComponent;
  let fixture: ComponentFixture<EditPostingTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPostingTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPostingTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
