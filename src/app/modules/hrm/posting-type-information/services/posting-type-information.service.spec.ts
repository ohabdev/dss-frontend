import { TestBed } from '@angular/core/testing';

import { PostingTypeInformationService } from './posting-type-information.service';

describe('PostingTypeInformationService', () => {
  let service: PostingTypeInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostingTypeInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
