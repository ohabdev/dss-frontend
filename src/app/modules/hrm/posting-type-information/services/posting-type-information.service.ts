import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostingTypeInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private PostingTypeInformation: any = null;
  private getPostingTypeInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('posting-type').toPromise();
  }

  createPostingTypeInformation(data: any): Promise<any> {
    return this.restangular.all('auth/posting-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singlePostingTypeInformation($id): Promise<any> {
    return this.restangular.all('auth/posting-type').customGET($id).toPromise();
  }

  updatePostingTypeInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/posting-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deletePostingTypeInformation($id): Promise<any> {
    return this.restangular.one('auth/posting-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
