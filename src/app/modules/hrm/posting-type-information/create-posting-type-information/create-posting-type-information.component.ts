import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ PostingTypeInformationService } from '../services/posting-type-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-posting-type-information',
  templateUrl: './create-posting-type-information.component.html',
  styleUrls: ['./create-posting-type-information.component.css']
})
  export class CreatePostingTypeInformationComponent implements OnInit {
    private PostingTypeInformation: PostingTypeInformationService;
    private Toasty: TostyService;
    public PostingTypeInformationData = {
      name: '',
      name_bn: '',
      status: 1
    };
    public submitted: boolean = false;
    router: any;
    private _location: any;
  
    constructor(postingTypeInformationService: PostingTypeInformationService, private toasty: TostyService, private location: Location) { 
      this.PostingTypeInformation = postingTypeInformationService;
      this.Toasty = toasty;
      this._location = location;
    }
    ngOnInit(): void {
    }
  
  
    createPostingTypeInformationOnSubmit(postingTypeInformationFrm: NgForm) {
      this.submitted = true;
  
      //console.log(postingTypeInformationFrm);
  
      if (postingTypeInformationFrm.invalid) {
        return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
      }
  
      this.PostingTypeInformation.createPostingTypeInformation(this.PostingTypeInformationData).then(() => {
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success',);
      })
      .catch((res) => { 
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!',);
      });
    }
     
  
  }
