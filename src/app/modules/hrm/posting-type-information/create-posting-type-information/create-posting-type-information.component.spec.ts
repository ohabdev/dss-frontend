import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePostingTypeInformationComponent } from './create-posting-type-information.component';

describe('CreatePostingTypeInformationComponent', () => {
  let component: CreatePostingTypeInformationComponent;
  let fixture: ComponentFixture<CreatePostingTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePostingTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePostingTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
