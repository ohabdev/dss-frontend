import { Component, OnInit } from '@angular/core';
import{ PostingTypeInformationService } from '../services/posting-type-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-posting-type-information-list',
  templateUrl: './posting-type-information-list.component.html',
  styleUrls: ['./posting-type-information-list.component.css']
})
export class PostingTypeInformationListComponent  implements OnInit {

  public items: any = []; 
  Toasty: any;


  constructor(private postingTypeInformationService: PostingTypeInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.postingTypeInformationService.getList().then((res) => {
      this.items = res.data.data;
    });
  }
  

  deletePostingTypeInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.postingTypeInformationService.deletePostingTypeInformation(id).then((res) => {
          this.postingTypeInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
