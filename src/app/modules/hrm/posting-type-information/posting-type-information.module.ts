import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostingTypeInformationListComponent } from './posting-type-information-list/posting-type-information-list.component';
import { CreatePostingTypeInformationComponent } from './create-posting-type-information/create-posting-type-information.component';
import { EditPostingTypeInformationComponent } from './edit-posting-type-information/edit-posting-type-information.component';


const routes: Routes = [{
  path: '',
  component: PostingTypeInformationListComponent,
  data: {title: 'Posting Type Information List'} 
},
{
  path: 'create',
  component: CreatePostingTypeInformationComponent,
  data: {title: 'Create Posting Type Information'} 
},
{
  path: 'edit/:id',
  component: EditPostingTypeInformationComponent,
  data: {title: 'Edit Posting Type Information'} 
}
];


@NgModule({
  declarations: [PostingTypeInformationListComponent, CreatePostingTypeInformationComponent, EditPostingTypeInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class PostingTypeInformationModule { }
