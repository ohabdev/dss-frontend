import { TestBed } from '@angular/core/testing';

import { ComplaintTypeInformationService } from './complaint-type-information.service';

describe('ComplaintTypeInformationService', () => {
  let service: ComplaintTypeInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComplaintTypeInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
