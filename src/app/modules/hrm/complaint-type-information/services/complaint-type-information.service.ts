import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintTypeInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private ComplaintTypeInformation: any = null;
  private getComplaintTypeInformation: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('complaint-type?page='+page).toPromise();
  }

  createComplaintTypeInformation(data: any): Promise<any> {
    return this.restangular.all('auth/complaint-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleComplaintTypeInformation($id): Promise<any> {
    return this.restangular.all('auth/complaint-type').customGET($id).toPromise();
  }

  updateComplaintTypeInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/complaint-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteComplaintTypeInformation($id): Promise<any> {
    return this.restangular.one('auth/complaint-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
