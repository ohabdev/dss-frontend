import { Component, OnInit } from '@angular/core';
import{ ComplaintTypeInformationService } from '../services/complaint-type-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-complaint-type-information-list',
  templateUrl: './complaint-type-information-list.component.html',
  styleUrls: ['./complaint-type-information-list.component.css']
})
export class ComplaintTypeInformationListComponent  implements OnInit {

  public items: any = []; 
  public pagination: any = [];
  Toasty: any;


  constructor(private complaintTypeInformationService: ComplaintTypeInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getPageDataFn();
    
  }
  getPageDataFn(page: number = 1) {
    this.complaintTypeInformationService.getList().then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
    });
  }

  deleteComplaintTypeInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.complaintTypeInformationService.deleteComplaintTypeInformation(id).then((res) => {
          this.complaintTypeInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
