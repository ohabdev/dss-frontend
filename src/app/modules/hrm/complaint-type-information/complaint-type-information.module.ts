import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComplaintTypeInformationListComponent } from './complaint-type-information-list/complaint-type-information-list.component';
import { CreateComplaintTypeInformationComponent } from './create-complaint-type-information/create-complaint-type-information.component';
import { EditComplaintTypeInformationComponent } from './edit-complaint-type-information/edit-complaint-type-information.component';


const routes: Routes = [{
  path: '',
  component: ComplaintTypeInformationListComponent,
  data: {title: 'Complaint Type Information List'} 
},
{
  path: 'create',
  component: CreateComplaintTypeInformationComponent,
  data: {title: 'Create Complaint Type Information'} 
},
{
  path: 'edit/:id',
  component: EditComplaintTypeInformationComponent,
  data: {title: 'Edit Complaint Type Information'} 
}
];


@NgModule({
  declarations: [ComplaintTypeInformationListComponent, CreateComplaintTypeInformationComponent, EditComplaintTypeInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class ComplaintTypeInformationModule { }
