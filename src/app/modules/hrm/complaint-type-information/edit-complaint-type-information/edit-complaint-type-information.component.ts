import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { ComplaintTypeInformationService } from '../services/complaint-type-information.service';

@Component({
  selector: 'app-edit-complaint-type-information',
  templateUrl: './edit-complaint-type-information.component.html',
  styleUrls: ['./edit-complaint-type-information.component.css']
})
export class EditComplaintTypeInformationComponent implements OnInit {
  private ComplaintTypeInformation: ComplaintTypeInformationService;
  private Toasty: TostyService;
  public ComplaintTypeInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    complaintTypeInformationService: ComplaintTypeInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.ComplaintTypeInformation = complaintTypeInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.ComplaintTypeInformation.singleComplaintTypeInformation(this.id).then((res) => {
          console.log(res.data);
          this.ComplaintTypeInformationData = res.data;
          console.log(this.ComplaintTypeInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editComplaintTypeInformationOnSubmit(complaintTypeInformationFrm: NgForm) {
    this.submitted = true;
    if (complaintTypeInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.ComplaintTypeInformation.updateComplaintTypeInformation(this.id, this.ComplaintTypeInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
