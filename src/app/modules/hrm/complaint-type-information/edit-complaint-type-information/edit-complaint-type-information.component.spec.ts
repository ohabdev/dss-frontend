import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditComplaintTypeInformationComponent } from './edit-complaint-type-information.component';

describe('EditComplaintTypeInformationComponent', () => {
  let component: EditComplaintTypeInformationComponent;
  let fixture: ComponentFixture<EditComplaintTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditComplaintTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditComplaintTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
