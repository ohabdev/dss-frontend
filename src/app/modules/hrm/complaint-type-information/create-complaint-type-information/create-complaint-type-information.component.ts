import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ ComplaintTypeInformationService } from '../services/complaint-type-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-complaint-type-information',
  templateUrl: './create-complaint-type-information.component.html',
  styleUrls: ['./create-complaint-type-information.component.css']
})
  export class CreateComplaintTypeInformationComponent implements OnInit {
    private ComplaintTypeInformation: ComplaintTypeInformationService;
    private Toasty: TostyService;
    public ComplaintTypeInformationData = {
      name: '',
      name_bn: '',
      status: 1
    };
    public submitted: boolean = false;
    router: any;
    private _location: any;
  
    constructor(complaintTypeInformationService: ComplaintTypeInformationService, private toasty: TostyService, private location: Location) { 
      this.ComplaintTypeInformation = complaintTypeInformationService;
      this.Toasty = toasty;
      this._location = location;
    }
    ngOnInit(): void {
    }
  
  
    createComplaintTypeInformationOnSubmit(complaintTypeInformationFrm: NgForm) {
      this.submitted = true;
  
      //console.log(complaintTypeInformationFrm);
  
      if (complaintTypeInformationFrm.invalid) {
        return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
      }
  
      this.ComplaintTypeInformation.createComplaintTypeInformation(this.ComplaintTypeInformationData).then(() => {
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success',);
      })
      .catch((res) => { 
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!',);
      });
    }
     
  
  }
