import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateComplaintTypeInformationComponent } from './create-complaint-type-information.component';

describe('CreateComplaintTypeInformationComponent', () => {
  let component: CreateComplaintTypeInformationComponent;
  let fixture: ComponentFixture<CreateComplaintTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateComplaintTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateComplaintTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
