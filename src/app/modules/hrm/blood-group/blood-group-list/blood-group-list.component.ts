import { Component, OnInit } from '@angular/core';
import{ BloodGroupService } from '../services/blood-group.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-blood-group-list',
  templateUrl: './blood-group-list.component.html',
  styleUrls: ['./blood-group-list.component.css']
})
export class BloodGroupListComponent implements OnInit {
  public items: any = []; 
  Toasty: any;


  constructor(private bloodGroupService: BloodGroupService,  private toasty: TostyService) { 
    this.Toasty = toasty;
    this.bloodGroupService.getList().then((res) => {
      this.items = res.data.data;
    });
  }

  ngOnInit(): void {
  }

  deleteBloodGroup( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.bloodGroupService.deleteBloodGroup(id).then((res) => {
          this.bloodGroupService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }


}
