import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BloodGroupService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private BloodGroup: any = null;
  private getBloodGroup: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('blood-group').toPromise();
  }

  createBloodGroup(data: any): Promise<any> {
    return this.restangular.all('auth/blood-group').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleBloodGroup($id): Promise<any> {
    return this.restangular.all('auth/blood-group').customGET($id).toPromise();
  }

  updateBloodGroup($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/blood-group',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteBloodGroup($id): Promise<any> {
    return this.restangular.one('auth/blood-group/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
