import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { BloodGroupService } from '../services/blood-group.service';

@Component({
  selector: 'app-edit-blood-group',
  templateUrl: './edit-blood-group.component.html',
  styleUrls: ['./edit-blood-group.component.css'],
})
export class EditBloodGroupComponent implements OnInit {
  private BloodGroup: BloodGroupService;
  private Toasty: TostyService;
  public BloodGroupData = {
    name: '',
    name_bn: '',
    status: 1,
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    bloodGroupService: BloodGroupService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.BloodGroup = bloodGroupService;
    this.Toasty = toasty;
    this._location = location;
    }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');

      if (this.id) {
        this.BloodGroup.singleBloodGroup(this.id)
          .then((res) => {
            this.BloodGroupData = res.data;
          })
          .catch((res) => {
            console.log(res.data.message);
            return this.Toasty.showError(res.data.message, 'Sorry!');
          });
      }
    });
  }

  editBloodGroupOnSubmit(bloodGroupFrm: NgForm) {
    this.submitted = true;
    //console.log(bloodGroupFrm);
    if (bloodGroupFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.BloodGroup.updateBloodGroup(this.id, this.BloodGroupData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
