import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BloodGroupListComponent } from './blood-group-list/blood-group-list.component';
import { CreateBloodGroupComponent } from './create-blood-group/create-blood-group.component';
import { EditBloodGroupComponent } from './edit-blood-group/edit-blood-group.component';

const routes: Routes = [
  {
    path: '',
    component: BloodGroupListComponent,
    data: { title: 'Blood Group List' },
  },
  {
    path: 'create',
    component: CreateBloodGroupComponent,
    data: { title: 'Create Blood Group' },
  },
  {
    path: 'edit/:id',
    component: EditBloodGroupComponent,
    data: { title: 'Edit Blood Group' },
  },
];

@NgModule({
  declarations: [
    BloodGroupListComponent,
    CreateBloodGroupComponent,
    EditBloodGroupComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class BloodGroupModule {}
