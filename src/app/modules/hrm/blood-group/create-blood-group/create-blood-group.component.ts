import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ BloodGroupService } from '../services/blood-group.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-create-blood-group',
  templateUrl: './create-blood-group.component.html',
  styleUrls: ['./create-blood-group.component.css']
})
export class CreateBloodGroupComponent implements OnInit {
  private BloodGroup: BloodGroupService;
  private Toasty: TostyService;
  public BloodGroupData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(bloodGroupService: BloodGroupService, private toasty: TostyService, private location: Location) { 
    this.BloodGroup = bloodGroupService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createBloodGroupOnSubmit(bloodGroupFrm: NgForm) {
    this.submitted = true;

    //console.log(bloodGroupFrm);

    if (bloodGroupFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.BloodGroup.createBloodGroup(this.BloodGroupData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
   

}
