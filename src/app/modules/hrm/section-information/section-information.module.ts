import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SectionInformationListComponent } from './section-information-list/section-information-list.component';
import { CreateSectionInformationComponent } from './create-section-information/create-section-information.component';
import { EditSectionInformationComponent } from './edit-section-information/edit-section-information.component';

const routes: Routes = [{
  path: '',
  component: SectionInformationListComponent,
  data: {title: 'Section Information List'} 
},
{
  path: 'create',
  component: CreateSectionInformationComponent,
  data: {title: 'Create Section Information'} 
},
{
  path: 'edit/:id',
  component: EditSectionInformationComponent,
  data: {title: 'Edit Section Information'} 
}
];


@NgModule({
  declarations: [SectionInformationListComponent, CreateSectionInformationComponent, EditSectionInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SectionInformationModule { }
