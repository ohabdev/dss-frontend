import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SectionInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private SectionInformation: any = null;
  private getSectionInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('section').toPromise();
  }

  createSectionInformation(data: any): Promise<any> {
    return this.restangular.all('auth/section').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleSectionInformation($id): Promise<any> {
    return this.restangular.all('auth/section').customGET($id).toPromise();
  }

  updateSectionInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/section',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteSectionInformation($id): Promise<any> {
    return this.restangular.one('auth/section/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
