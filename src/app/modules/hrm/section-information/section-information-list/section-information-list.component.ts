import { Component, OnInit } from '@angular/core';
import{ SectionInformationService } from '../services/section-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-section-information-list',
  templateUrl: './section-information-list.component.html',
  styleUrls: ['./section-information-list.component.css']
})
export class SectionInformationListComponent  implements OnInit {

  public items: any = []; 
  Toasty: any;


  constructor(private sectionInformationService: SectionInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.sectionInformationService.getList().then((res) => {
      this.items = res.data.data;
    });
  }
  

  deleteSectionInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.sectionInformationService.deleteSectionInformation(id).then((res) => {
          this.sectionInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
