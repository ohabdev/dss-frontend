import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { SectionInformationService } from '../services/section-information.service';

@Component({
  selector: 'app-edit-section-information',
  templateUrl: './edit-section-information.component.html',
  styleUrls: ['./edit-section-information.component.css']
})
export class EditSectionInformationComponent implements OnInit {
  private SectionInformation: SectionInformationService;
  private Toasty: TostyService;
  public SectionInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    sectionInformationService: SectionInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.SectionInformation = sectionInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.SectionInformation.singleSectionInformation(this.id).then((res) => {
          console.log(res.data);
          this.SectionInformationData = res.data;
          console.log(this.SectionInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editSectionInformationOnSubmit(sectionInformationFrm: NgForm) {
    this.submitted = true;
    if (sectionInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.SectionInformation.updateSectionInformation(this.id, this.SectionInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
