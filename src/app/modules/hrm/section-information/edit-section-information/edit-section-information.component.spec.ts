import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDesignationInformationComponent } from './edit-designation-information.component';

describe('EditDesignationInformationComponent', () => {
  let component: EditDesignationInformationComponent;
  let fixture: ComponentFixture<EditDesignationInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDesignationInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDesignationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
