import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ SectionInformationService } from '../services/section-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-section-information',
  templateUrl: './create-section-information.component.html',
  styleUrls: ['./create-section-information.component.css']
})
export class CreateSectionInformationComponent implements OnInit {
  private SectionInformation: SectionInformationService;
  private Toasty: TostyService;
  public SectionInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(sectionInformationService: SectionInformationService, private toasty: TostyService, private location: Location) { 
    this.SectionInformation = sectionInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createSectionInformationOnSubmit(sectionInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(sectionInformationFrm);

    if (sectionInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.SectionInformation.createSectionInformation(this.SectionInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
