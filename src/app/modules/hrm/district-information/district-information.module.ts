import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DistrictInformationListComponent } from './district-information-list/district-information-list.component';
import { CreateDistrictInformationComponent } from './create-district-information/create-district-information.component';
import { EditDistrictInformationComponent } from './edit-district-information/edit-district-information.component';


const routes: Routes = [{
  path: '',
  component: DistrictInformationListComponent,
  data: {title: 'District Information List'} 
},
{
  path: 'create',
  component: CreateDistrictInformationComponent,
  data: {title: 'Create District Information'} 
},
{
  path: 'edit/:id',
  component: EditDistrictInformationComponent,
  data: {title: 'Edit District Information'} 
}
];


@NgModule({
  declarations: [DistrictInformationListComponent, CreateDistrictInformationComponent, EditDistrictInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class DistrictInformationModule { }
