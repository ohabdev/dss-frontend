import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ DistrictInformationService } from '../services/district-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-district-information',
  templateUrl: './create-district-information.component.html',
  styleUrls: ['./create-district-information.component.css']
})
export class CreateDistrictInformationComponent implements OnInit {
  private DistrictInformation: DistrictInformationService;
  private Toasty: TostyService;
  public DistrictInformationData = {
    name: '',
    name_bn: '',
    division_id: '',
    status: 1
  };
  public division_list: any = []; 
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(districtInformationService: DistrictInformationService, private toasty: TostyService, private location: Location) { 
    this.DistrictInformation = districtInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
    this.DistrictInformation.getDivisionList().then((res) => {
      this.division_list = res.data;
    });
  }


  createDistrictInformationOnSubmit(districtInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(districtInformationFrm);

    if (districtInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.DistrictInformation.createDistrictInformation(this.DistrictInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
