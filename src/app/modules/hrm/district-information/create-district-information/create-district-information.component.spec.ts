import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDistrictInformationComponent } from './create-district-information.component';

describe('CreateDistrictInformationComponent', () => {
  let component: CreateDistrictInformationComponent;
  let fixture: ComponentFixture<CreateDistrictInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDistrictInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDistrictInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
