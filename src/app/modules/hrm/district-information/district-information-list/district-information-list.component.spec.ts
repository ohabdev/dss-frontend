import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictInformationListComponent } from './district-information-list.component';

describe('DistrictInformationListComponent', () => {
  let component: DistrictInformationListComponent;
  let fixture: ComponentFixture<DistrictInformationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistrictInformationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictInformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
