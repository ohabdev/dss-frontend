import { Component, OnInit } from '@angular/core';
import { DistrictInformationService } from '../services/district-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-district-information-list',
  templateUrl: './district-information-list.component.html',
  styleUrls: ['./district-information-list.component.css'],
})
export class DistrictInformationListComponent implements OnInit {
  public items: any = [];
  public pagination: any = [];
  Toasty: any;

  constructor(
    private districtInformationService: DistrictInformationService,
    private toasty: TostyService
  ) {
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getPageDataFn();
  }

  getPageDataFn(page: number = 1) {
    this.districtInformationService.getList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
    });
  }

  deleteDistrictInformation(id: any) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.districtInformationService
          .deleteDistrictInformation(id)
          .then((res) => {
            this.getPageDataFn();
            Swal.fire({
              icon: 'success',
              title: 'Deleted!',
              text: 'Your imaginary file has been deleted.',
              showCancelButton: false,
              showConfirmButton: false,
              timer: 1000,
            });
          });
      }
    });
  }
}
