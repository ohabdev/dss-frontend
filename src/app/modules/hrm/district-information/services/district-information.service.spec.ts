import { TestBed } from '@angular/core/testing';

import { DistrictInformationService } from './district-information.service';

describe('DistrictInformationService', () => {
  let service: DistrictInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DistrictInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
