import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DistrictInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private DistrictInformation: any = null;
  private getDistrictInformation: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('district?page='+page).toPromise();
  }

  getDivisionList() {
    return this.restangular.all('auth').customGET('division/list').toPromise();
  }

  createDistrictInformation(data: any): Promise<any> {
    return this.restangular.all('auth/district').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDistrictInformation($id): Promise<any> {
    return this.restangular.all('auth/district').customGET($id).toPromise();
  }

  updateDistrictInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/district',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDistrictInformation($id): Promise<any> {
    return this.restangular.one('auth/district/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
