import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DistrictInformationService } from '../services/district-information.service';

@Component({
  selector: 'app-edit-district-information',
  templateUrl: './edit-district-information.component.html',
  styleUrls: ['./edit-district-information.component.css']
})
export class EditDistrictInformationComponent implements OnInit {
  private DistrictInformation: DistrictInformationService;
  private Toasty: TostyService;
  public DistrictInformationData = {
    name: '',
    name_bn: '',
    division_id: '',
    status: 1
  };
  public division_list: any = []; 
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    districtInformationService: DistrictInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.DistrictInformation = districtInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.DistrictInformation.singleDistrictInformation(this.id).then((res) => {
          console.log(res.data);
          this.DistrictInformationData = res.data;
        })
        .catch((res) => { 
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });

        this.DistrictInformation.getDivisionList().then((res) => {
          this.division_list = res.data;
        });
        
      }

    });

  }

  editDistrictInformationOnSubmit(districtInformationFrm: NgForm) {
    this.submitted = true;
    if (districtInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.DistrictInformation.updateDistrictInformation(this.id, this.DistrictInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
