import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDistrictInformationComponent } from './edit-district-information.component';

describe('EditDistrictInformationComponent', () => {
  let component: EditDistrictInformationComponent;
  let fixture: ComponentFixture<EditDistrictInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDistrictInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDistrictInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
