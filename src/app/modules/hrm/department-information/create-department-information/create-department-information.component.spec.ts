import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDepartmentInformationComponent } from './create-department-information.component';

describe('CreateDepartmentInformationComponent', () => {
  let component: CreateDepartmentInformationComponent;
  let fixture: ComponentFixture<CreateDepartmentInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDepartmentInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDepartmentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
