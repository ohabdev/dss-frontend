import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ DepartmentInformationService } from '../services/department-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-department-information',
  templateUrl: './create-department-information.component.html',
  styleUrls: ['./create-department-information.component.css']
})
export class CreateDepartmentInformationComponent implements OnInit {
  private DepartmentInformation: DepartmentInformationService;
  private Toasty: TostyService;
  public DepartmentInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(departmentInformationService: DepartmentInformationService, private toasty: TostyService, private location: Location) { 
    this.DepartmentInformation = departmentInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createDepartmentInformationOnSubmit(departmentInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(departmentInformationFrm);

    if (departmentInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.DepartmentInformation.createDepartmentInformation(this.DepartmentInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
