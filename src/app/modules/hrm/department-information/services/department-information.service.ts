import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private DepartmentInformation: any = null;
  private getDepartmentInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('department').toPromise();
  }

  createDepartmentInformation(data: any): Promise<any> {
    return this.restangular.all('auth/department').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDepartmentInformation($id): Promise<any> {
    return this.restangular.all('auth/department').customGET($id).toPromise();
  }

  updateDepartmentInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/department',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDepartmentInformation($id): Promise<any> {
    return this.restangular.one('auth/department/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
