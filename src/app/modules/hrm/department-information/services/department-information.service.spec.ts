import { TestBed } from '@angular/core/testing';

import { DepartmentInformationService } from './department-information.service';

describe('DepartmentInformationService', () => {
  let service: DepartmentInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DepartmentInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
