import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDepartmentInformationComponent } from './edit-department-information.component';

describe('EditDepartmentInformationComponent', () => {
  let component: EditDepartmentInformationComponent;
  let fixture: ComponentFixture<EditDepartmentInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDepartmentInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDepartmentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
