import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DepartmentInformationService } from '../services/department-information.service';

@Component({
  selector: 'app-edit-department-information',
  templateUrl: './edit-department-information.component.html',
  styleUrls: ['./edit-department-information.component.css']
})
export class EditDepartmentInformationComponent implements OnInit {
  private DepartmentInformation: DepartmentInformationService;
  private Toasty: TostyService;
  public DepartmentInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    departmentInformationService: DepartmentInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.DepartmentInformation = departmentInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.DepartmentInformation.singleDepartmentInformation(this.id).then((res) => {
          console.log(res.data);
          this.DepartmentInformationData = res.data;
          console.log(this.DepartmentInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editDepartmentInformationOnSubmit(departmentInformationFrm: NgForm) {
    this.submitted = true;
    if (departmentInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.DepartmentInformation.updateDepartmentInformation(this.id, this.DepartmentInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
