import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepartmentInformationListComponent } from './department-information-list/department-information-list.component';
import { CreateDepartmentInformationComponent } from './create-department-information/create-department-information.component';
import { EditDepartmentInformationComponent } from './edit-department-information/edit-department-information.component';


const routes: Routes = [{
  path: '',
  component: DepartmentInformationListComponent,
  data: {title: 'Department Information List'} 
},
{
  path: 'create',
  component: CreateDepartmentInformationComponent,
  data: {title: 'Create Department Information'} 
},
{
  path: 'edit/:id',
  component: EditDepartmentInformationComponent,
  data: {title: 'Edit Department Information'} 
}
];


@NgModule({
  declarations: [DepartmentInformationListComponent, CreateDepartmentInformationComponent, EditDepartmentInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class DepartmentInformationModule { }
