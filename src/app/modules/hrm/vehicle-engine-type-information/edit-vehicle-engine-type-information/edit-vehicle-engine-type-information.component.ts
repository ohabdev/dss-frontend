import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { VehicleEngineTypeInformationService } from '../services/vehicle-engine-type-information.service';

@Component({
  selector: 'app-edit-vehicle-engine-type-information',
  templateUrl: './edit-vehicle-engine-type-information.component.html',
  styleUrls: ['./edit-vehicle-engine-type-information.component.css']
})
export class EditVehicleEngineTypeInformationComponent implements OnInit {
  private VehicleEngineTypeInformation: VehicleEngineTypeInformationService;
  private Toasty: TostyService;
  public VehicleEngineTypeInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    complaintTypeInformationService: VehicleEngineTypeInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.VehicleEngineTypeInformation = complaintTypeInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.VehicleEngineTypeInformation.singleVehicleEngineTypeInformation(this.id).then((res) => {
          console.log(res.data);
          this.VehicleEngineTypeInformationData = res.data;
          console.log(this.VehicleEngineTypeInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editVehicleEngineTypeInformationOnSubmit(complaintTypeInformationFrm: NgForm) {
    this.submitted = true;
    if (complaintTypeInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.VehicleEngineTypeInformation.updateVehicleEngineTypeInformation(this.id, this.VehicleEngineTypeInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
