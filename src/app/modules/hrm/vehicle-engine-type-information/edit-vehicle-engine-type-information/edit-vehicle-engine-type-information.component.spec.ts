import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVehicleEngineTypeInformationComponent } from './edit-vehicle-engine-type-information.component';

describe('EditVehicleEngineTypeInformationComponent', () => {
  let component: EditVehicleEngineTypeInformationComponent;
  let fixture: ComponentFixture<EditVehicleEngineTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditVehicleEngineTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVehicleEngineTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
