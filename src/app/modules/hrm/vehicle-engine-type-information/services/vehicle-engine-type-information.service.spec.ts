import { TestBed } from '@angular/core/testing';

import { VehicleEngineTypeInformationService } from './vehicle-engine-type-information.service';

describe('VehicleEngineTypeInformationService', () => {
  let service: VehicleEngineTypeInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehicleEngineTypeInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
