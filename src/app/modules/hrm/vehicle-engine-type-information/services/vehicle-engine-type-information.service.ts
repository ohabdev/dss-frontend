import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehicleEngineTypeInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private VehicleEngineTypeInformation: any = null;
  private getVehicleEngineTypeInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('vehicle-engine-type').toPromise();
  }

  createVehicleEngineTypeInformation(data: any): Promise<any> {
    return this.restangular.all('auth/vehicle-engine-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleVehicleEngineTypeInformation($id): Promise<any> {
    return this.restangular.all('auth/vehicle-engine-type').customGET($id).toPromise();
  }

  updateVehicleEngineTypeInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/vehicle-engine-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteVehicleEngineTypeInformation($id): Promise<any> {
    return this.restangular.one('auth/vehicle-engine-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
