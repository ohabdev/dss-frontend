import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VehicleEngineTypeInformationListComponent } from './vehicle-engine-type-information-list/vehicle-engine-type-information-list.component';
import { CreateVehicleEngineTypeInformationComponent } from './create-vehicle-engine-type-information/create-vehicle-engine-type-information.component';
import { EditVehicleEngineTypeInformationComponent } from './edit-vehicle-engine-type-information/edit-vehicle-engine-type-information.component';


const routes: Routes = [{
  path: '',
  component: VehicleEngineTypeInformationListComponent,
  data: {title: 'Vehicle Engine Type Information List'} 
},
{
  path: 'create',
  component: CreateVehicleEngineTypeInformationComponent,
  data: {title: 'Create Vehicle Engine Type Information'} 
},
{
  path: 'edit/:id',
  component: EditVehicleEngineTypeInformationComponent,
  data: {title: 'Edit Vehicle Engine Type Information'} 
}
];


@NgModule({
  declarations: [VehicleEngineTypeInformationListComponent, CreateVehicleEngineTypeInformationComponent, EditVehicleEngineTypeInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class VehicleEngineTypeInformationModule { }