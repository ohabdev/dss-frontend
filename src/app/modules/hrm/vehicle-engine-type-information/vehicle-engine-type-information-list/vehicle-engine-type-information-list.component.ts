import { Component, OnInit } from '@angular/core';
import{ VehicleEngineTypeInformationService } from '../services/vehicle-engine-type-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-vehicle-engine-type-information-list',
  templateUrl: './vehicle-engine-type-information-list.component.html',
  styleUrls: ['./vehicle-engine-type-information-list.component.css']
})
export class VehicleEngineTypeInformationListComponent  implements OnInit {

  public items: any = []; 
  Toasty: any;


  constructor(private vehicleEngineTypeInformationService: VehicleEngineTypeInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.vehicleEngineTypeInformationService.getList().then((res) => {
      this.items = res.data.data;
    });
  }
  

  deleteVehicleEngineTypeInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.vehicleEngineTypeInformationService.deleteVehicleEngineTypeInformation(id).then((res) => {
          this.vehicleEngineTypeInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
