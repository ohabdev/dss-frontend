import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateVehicleEngineTypeInformationComponent } from './create-vehicle-engine-type-information.component';

describe('CreateVehicleEngineTypeInformationComponent', () => {
  let component: CreateVehicleEngineTypeInformationComponent;
  let fixture: ComponentFixture<CreateVehicleEngineTypeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateVehicleEngineTypeInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateVehicleEngineTypeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
