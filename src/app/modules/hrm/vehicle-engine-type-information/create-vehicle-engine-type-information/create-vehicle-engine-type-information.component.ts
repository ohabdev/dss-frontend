import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ VehicleEngineTypeInformationService } from '../services/vehicle-engine-type-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-vehicle-engine-type-information',
  templateUrl: './create-vehicle-engine-type-information.component.html',
  styleUrls: ['./create-vehicle-engine-type-information.component.css']
})
  export class CreateVehicleEngineTypeInformationComponent implements OnInit {
    private VehicleEngineTypeInformation: VehicleEngineTypeInformationService;
    private Toasty: TostyService;
    public VehicleEngineTypeInformationData = {
      name: '',
      name_bn: '',
      status: 1
    };
    public submitted: boolean = false;
    router: any;
    private _location: any;
  
    constructor(complaintTypeInformationService: VehicleEngineTypeInformationService, private toasty: TostyService, private location: Location) { 
      this.VehicleEngineTypeInformation = complaintTypeInformationService;
      this.Toasty = toasty;
      this._location = location;
    }
    ngOnInit(): void {
    }
  
  
    createVehicleEngineTypeInformationOnSubmit(complaintTypeInformationFrm: NgForm) {
      this.submitted = true;
  
      //console.log(complaintTypeInformationFrm);
  
      if (complaintTypeInformationFrm.invalid) {
        return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
      }
  
      this.VehicleEngineTypeInformation.createVehicleEngineTypeInformation(this.VehicleEngineTypeInformationData).then(() => {
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success',);
      })
      .catch((res) => { 
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!',);
      });
    }
     
  
  }
