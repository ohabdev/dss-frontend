import { TestBed } from '@angular/core/testing';

import { GenderInformationService } from './gender-information.service';

describe('GenderInformationService', () => {
  let service: GenderInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenderInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
