import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenderInformationService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private GenderInformation: any = null;
  private getGenderInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('gender').toPromise();
  }

  createGenderInformation(data: any): Promise<any> {
    return this.restangular.all('auth/gender').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleGenderInformation($id): Promise<any> {
    return this.restangular.all('auth/gender').customGET($id).toPromise();
  }

  updateGenderInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/gender',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteGenderInformation($id): Promise<any> {
    return this.restangular.one('auth/gender/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
