import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGenderInformationComponent } from './create-gender-information.component';

describe('CreateGenderInformationComponent', () => {
  let component: CreateGenderInformationComponent;
  let fixture: ComponentFixture<CreateGenderInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateGenderInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGenderInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
