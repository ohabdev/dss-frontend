import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ GenderInformationService } from '../services/gender-information.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-gender-information',
  templateUrl: './create-gender-information.component.html',
  styleUrls: ['./create-gender-information.component.css']
})
export class CreateGenderInformationComponent implements OnInit {
  private GenderInformation: GenderInformationService;
  private Toasty: TostyService;
  public GenderInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(genderInformationService: GenderInformationService, private toasty: TostyService, private location: Location) { 
    this.GenderInformation = genderInformationService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
  }


  createGenderInformationOnSubmit(genderInformationFrm: NgForm) {
    this.submitted = true;

    //console.log(genderInformationFrm);

    if (genderInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.GenderInformation.createGenderInformation(this.GenderInformationData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }
   

}
