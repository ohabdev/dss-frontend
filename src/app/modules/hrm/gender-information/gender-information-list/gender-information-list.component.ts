import { Component, OnInit } from '@angular/core';
import{ GenderInformationService } from '../services/gender-information.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-gender-information-list',
  templateUrl: './gender-information-list.component.html',
  styleUrls: ['./gender-information-list.component.css']
})
export class GenderInformationListComponent  implements OnInit {

  public items: any = []; 
  Toasty: any;


  constructor(private genderInformationService: GenderInformationService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.genderInformationService.getList().then((res) => {
      this.items = res.data.data;
    });
  }
  

  deleteGenderInformation( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.genderInformationService.deleteGenderInformation(id).then((res) => {
          this.genderInformationService.getList().then((res) => {
            this.items = res.data.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
