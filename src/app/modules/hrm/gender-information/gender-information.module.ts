import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenderInformationListComponent } from './gender-information-list/gender-information-list.component';
import { CreateGenderInformationComponent } from './create-gender-information/create-gender-information.component';
import { EditGenderInformationComponent } from './edit-gender-information/edit-gender-information.component';


const routes: Routes = [{
  path: '',
  component: GenderInformationListComponent,
  data: {title: 'Gender Information List'} 
},
{
  path: 'create',
  component: CreateGenderInformationComponent,
  data: {title: 'Create Gender Information'} 
},
{
  path: 'edit/:id',
  component: EditGenderInformationComponent,
  data: {title: 'Edit Gender Information'} 
}
];


@NgModule({
  declarations: [GenderInformationListComponent, CreateGenderInformationComponent, EditGenderInformationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})

export class GenderInformationModule { }
