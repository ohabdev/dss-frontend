import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { GenderInformationService } from '../services/gender-information.service';

@Component({
  selector: 'app-edit-gender-information',
  templateUrl: './edit-gender-information.component.html',
  styleUrls: ['./edit-gender-information.component.css']
})
export class EditGenderInformationComponent implements OnInit {
  private GenderInformation: GenderInformationService;
  private Toasty: TostyService;
  public GenderInformationData = {
    name: '',
    name_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    genderInformationService: GenderInformationService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.GenderInformation = genderInformationService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.GenderInformation.singleGenderInformation(this.id).then((res) => {
          console.log(res.data);
          this.GenderInformationData = res.data;
          console.log(this.GenderInformationData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editGenderInformationOnSubmit(genderInformationFrm: NgForm) {
    this.submitted = true;
    if (genderInformationFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.GenderInformation.updateGenderInformation(this.id, this.GenderInformationData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        console.log(res.data.message);
        return this.Toasty.showError(res.data.message, 'Sorry!');
      });
  }
}
