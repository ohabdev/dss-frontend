import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HRMRoutingModule } from '../hrm/hrm-routing.module';
import { BloodGroupModule } from './blood-group/blood-group.module';
import { QuotaInformationModule } from './quota-information/quota-information.module';
import { DesignationInformationModule } from './designation-information/designation-information.module';
import { SectionInformationModule } from './section-information/section-information.module';
import { DepartmentInformationModule } from './department-information/department-information.module';
import { DivisionInformationModule } from './division-information/division-information.module';
import { DistrictInformationModule } from './district-information/district-information.module';
import { UpazilaInformationModule } from './upazila-information/upazila-information.module';
import { MaritalInformationModule } from './marital-information/marital-information.module';
import { GenderInformationModule } from './gender-information/gender-information.module';
import { ExaminationInformationModule } from './examination-information/examination-information.module';
import { SalaryInformationModule } from './salary-information/salary-information.module';
import { ComplaintTypeInformationModule } from './complaint-type-information/complaint-type-information.module';
import { PostingTypeInformationModule } from './posting-type-information/posting-type-information.module';
import { TravelTypeInformationModule } from './travel-type-information/travel-type-information.module';
import { VehicleEngineTypeInformationModule } from './vehicle-engine-type-information/vehicle-engine-type-information.module';

import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HRMRoutingModule,
    BloodGroupModule,
    QuotaInformationModule,
    DesignationInformationModule,
    SectionInformationModule,
    DepartmentInformationModule,
    DivisionInformationModule,
    DistrictInformationModule,
    UpazilaInformationModule,
    MaritalInformationModule,
    GenderInformationModule,
    ExaminationInformationModule,
    SalaryInformationModule,
    ComplaintTypeInformationModule,
    PostingTypeInformationModule,
    TravelTypeInformationModule,
    VehicleEngineTypeInformationModule
  ],
  exports: [
    RouterModule,
  ]
})
export class HrmModule { }
