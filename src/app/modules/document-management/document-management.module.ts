import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DocumentTypeModule } from './document-type/document-type.module';
import { DocumentTableModule } from './document-table/document-table.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DocumentTypeModule,
    DocumentTableModule
  ],
  exports: [
    RouterModule,
  ]
})
export class DocumentManagementModule { }
