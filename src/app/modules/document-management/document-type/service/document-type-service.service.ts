import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeServiceService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private documentType: any = null;
  private getdocumentType: any;

  constructor(
    private restangular: Restangular
  ) { }

  getList() {
    return this.restangular.all('auth').customGET('document-type').toPromise();
  }

  createDocumentType(data: any): Promise<any> {
    return this.restangular.all('auth/document-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDocumentType($id): Promise<any> {
    return this.restangular.one('auth/document-type/').customGET($id).toPromise();
  }

  updateDocumentType($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/document-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDocumentType($id): Promise<any> {
    return this.restangular.one('auth/document-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
}
