import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DocumentTypeServiceService } from '../service/document-type-service.service';

@Component({
  selector: 'app-document-type-create',
  templateUrl: './document-type-create.component.html',
  styleUrls: ['./document-type-create.component.css']
})
export class DocumentTypeCreateComponent implements OnInit {
  private DocumentType: DocumentTypeServiceService;
  private Toasty: TostyService;

  public documentTypeData = {
    document_type: '',
    document_type_bn: ''
  };

  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(
    private DocumentTypeService: DocumentTypeServiceService, 
    private toasty: TostyService,
    private location: Location
  ) { 
    this.DocumentType = DocumentTypeService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
  }
  createDocumentTypeOnSubmit(documentTypeFrm: NgForm) {
    this.submitted = true;
    if (documentTypeFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.DocumentType.createDocumentType(this.documentTypeData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
        }else{
          this.Toasty.showError(res.message, 'Sorry!',);
        }
    });


  }
}
