import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentTypeListComponent } from './document-type-list/document-type-list.component';
import { DocumentTypeCreateComponent } from './document-type-create/document-type-create.component';
import { DocumentTypeEditComponent } from './document-type-edit/document-type-edit.component';

const routes: Routes = [{
  path: 'list',
  component: DocumentTypeListComponent,
  data: {title: 'Document Type List'} 
},
{
  path: 'create',
  component: DocumentTypeCreateComponent,
  data: {title: 'Create Document Type'} 
},
{
  path: 'edit/:id',
  component: DocumentTypeEditComponent,
  data: {title: 'Edit Office Type'} 
}
];

@NgModule({
  declarations: [
    DocumentTypeListComponent,
    DocumentTypeCreateComponent, 
    DocumentTypeEditComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DocumentTypeModule { }
