import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DocumentTypeServiceService } from '../service/document-type-service.service';

@Component({
  selector: 'app-document-type-edit',
  templateUrl: './document-type-edit.component.html',
  styleUrls: ['./document-type-edit.component.css']
})
export class DocumentTypeEditComponent implements OnInit {

  private documentType: DocumentTypeServiceService;
  private Toasty: TostyService;

  public documentTypeData = {
    document_type: '',
    document_type_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    private documentTypeService: DocumentTypeServiceService, 
    private toasty: TostyService,
    private location: Location,
    private router : Router,
    private route: ActivatedRoute
  ) {
    this.documentType = documentTypeService;
    this.Toasty = toasty;
    this._location = location;
    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
   }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.documentType.singleDocumentType(this.id).then((res) => {
          this.documentTypeData = res.data;
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editDocumentTypeOnSubmit(officeCategoryFrm: NgForm) {
    this.submitted = true;
    if (officeCategoryFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.documentType.updateDocumentType(this.id, this.documentTypeData)
    .then(() => {
      this._location.back();
      this.Toasty.showSuccess('Update Successfully', 'Success');
    })

    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
   
  }


}
