import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DocumentTypeServiceService } from '../service/document-type-service.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-document-type-list',
  templateUrl: './document-type-list.component.html',
  styleUrls: ['./document-type-list.component.css']
})
export class DocumentTypeListComponent implements OnInit {
  public items: any = []; 
  Toasty: any;

  constructor(
    private documentTypeService: DocumentTypeServiceService, 
    private toasty: TostyService
  ) {
    this.Toasty = toasty;    
    this.documentTypeService.getList().then((res) => {
      this.items = res.data;      
    });
   }

  ngOnInit(): void {
  }
  deletedocumentType( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.documentTypeService.deleteDocumentType(id).then((res) => {
          this.documentTypeService.getList().then((res) => {
            this.items = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
  }
}
