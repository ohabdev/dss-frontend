import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'document-type', loadChildren: () => import('./document-type/document-type.module').then(m => m.DocumentTypeModule) }    
    ]
  },
  {
    path: '',
    children: [
      { path: 'document-table', loadChildren: () => import('./document-table/document-table.module').then(m => m.DocumentTableModule) }    
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentManagementRoutingModule { }