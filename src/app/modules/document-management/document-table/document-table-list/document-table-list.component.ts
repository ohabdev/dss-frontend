import { Component, OnInit } from '@angular/core';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DocumentTableServiceService } from '../services/document-table-service.service';

@Component({
  selector: 'app-document-table-list',
  templateUrl: './document-table-list.component.html',
  styleUrls: ['./document-table-list.component.css']
})
export class DocumentTableListComponent implements OnInit {
  public items: any = []; 
  Toasty: any;

  constructor(
    private documentTableService: DocumentTableServiceService, 
    private toasty: TostyService
  ) {
    this.Toasty = toasty;    
    this.documentTableService.getList().then((res) => {
      this.items = res.data;      
      console.log(res.data);
      
    });
   }

  ngOnInit(): void {
  }
  deletedocumentTable( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.documentTableService.deleteDocumentTable(id).then((res) => {
          this.documentTableService.getList().then((res) => {
            this.items = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })  
    
  }
}
