import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DocumentTableServiceService } from '../services/document-table-service.service'; 

@Component({
  selector: 'app-edit-document-table',
  templateUrl: './edit-document-table.component.html',
  styleUrls: ['./edit-document-table.component.css']
})
export class EditDocumentTableComponent implements OnInit {
  private documentTable: DocumentTableServiceService;
  private Toasty: TostyService;
  
  public documentTableData = {
    employee_id: '',
    document_type_id: '',
    document_title: '',
    document_title_bn: '',
    document_path: '',
    status: 1
  };
  public documentTypeLists: any = []; 
  public employeeLists: any = []; 

  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;


  constructor(
    private documentTableService: DocumentTableServiceService,  
    private toasty: TostyService,
    private location: Location,
    private router : Router,
    private route: ActivatedRoute
  ) { 

    this.documentTable = documentTableService;
    this.Toasty = toasty;
    this._location = location;

    this.documentTable.getDocumentTypeList().then((res) => {
      this.documentTypeLists = res.data;      
    });

    this.documentTable.getEmployeeLists().then((res) => {
      this.employeeLists = res.data;      
    });

  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.documentTable.singleDocumentTable(this.id).then((res) => {
          console.log(res.data);
          this.documentTableData = res.data;
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });
  }

  editDocumentTableOnSubmit(documentTableFrm: NgForm) {
    this.submitted = true;
    if (documentTableFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.documentTable.updateDocumentTable(this.id, this.documentTableData)
    .then(() => {
      this._location.back();
      this.Toasty.showSuccess('Update Successfully', 'Success');
    })

    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
   
  }
}
