import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentTableListComponent } from './document-table-list/document-table-list.component';
import { DocumentTableCreateComponent } from './create-document-table/create-document-table.component';
import { EditDocumentTableComponent } from './edit-document-table/edit-document-table.component';
import { SingleDocumentTableComponent } from './single-document-table/single-document-table.component';


const routes: Routes = [{
  path: 'list',
  component: DocumentTableListComponent,
  data: {title: 'Document Type List'} 
},
{
  path: 'create',
  component: DocumentTableCreateComponent,
  data: {title: 'Create Office Type'} 
},
{
  path: 'list/:id',
  component: SingleDocumentTableComponent,
  data: {title: 'Single Office Type'} 
},
{
  path: 'edit/:id',
  component: EditDocumentTableComponent,
  data: {title: 'Edit Office Type'} 
}
];
@NgModule({
  declarations: [
    DocumentTableListComponent, 
    DocumentTableCreateComponent, EditDocumentTableComponent, SingleDocumentTableComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DocumentTableModule { }
