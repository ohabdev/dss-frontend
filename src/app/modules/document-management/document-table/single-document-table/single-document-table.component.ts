import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DocumentTableServiceService } from '../services/document-table-service.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-single-document-table',
  templateUrl: './single-document-table.component.html',
  styleUrls: ['./single-document-table.component.css']
})
export class SingleDocumentTableComponent implements OnInit {
  public singleItem: any = []; 
  id:any;

  constructor(
    private documentTableService: DocumentTableServiceService,  
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
  }

  ngOnInit(): void {
    this.documentTableService.singleDocumentTable(this.id).then((res) => {
      console.log(res.data);
      
      this.singleItem = res.data;
    });
  }

}
