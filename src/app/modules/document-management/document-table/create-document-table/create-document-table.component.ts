import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { DocumentTableServiceService } from '../services/document-table-service.service'; 

@Component({
  selector: 'app-create-document-table',
  templateUrl: './create-document-table.component.html',
  styleUrls: ['./create-document-table.component.css']
})
export class DocumentTableCreateComponent implements OnInit {
  private documentTable: DocumentTableServiceService;
  private Toasty: TostyService; 

  public documentTableData = {
    employee_id: '',
    document_type_id: '',
    document_title: '',
    document_title_bn: '',
    document_path: '',
    status: 1
  };
  public documentTypeLists: any = []; 
  public employeeLists: any = []; 
  public submitted: boolean = false;
  router: any;
  private _location: any;
  
  constructor(
    private documentTableService: DocumentTableServiceService,  
    private toasty: TostyService,
    private location: Location
  ) { 
    this.documentTable = documentTableService;
    this.Toasty = toasty;
    this._location = location;

    this.documentTable.getDocumentTypeList().then((res) => {
      console.log(res.data)
      this.documentTypeLists = res.data;      
    });

    this.documentTable.getEmployeeLists().then((res) => {
      console.log(res.data)
      this.employeeLists = res.data;      
    });


  }

  ngOnInit(): void {
    
  }

  createDocumentTableOnSubmit(documentTableFrm: NgForm) {
    this.submitted = true;    

    console.log(this.documentTableData);
    console.log(documentTableFrm);
    
    if (documentTableFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.documentTable.createDocumentTable(this.documentTableData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });

  }
 
}