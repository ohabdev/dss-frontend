import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class DocumentTableServiceService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private documentTable: any = null;
  private getdocumentType: any;

  constructor(
    private restangular: Restangular
  ) { }

  getList() {
    return this.restangular.all('auth').customGET('document').toPromise();
  }
  
  createDocumentTable(data: any): Promise<any> {
    return this.restangular.all('auth/document').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleDocumentTable($id): Promise<any> {
    return this.restangular.one('auth/document/').customGET($id).toPromise();
  }

  updateDocumentTable($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/document',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteDocumentTable($id): Promise<any> {
    return this.restangular.one('auth/document/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  getDocumentTypeList() {
    return this.restangular.all('auth').customGET('document-type').toPromise();
  }
  getEmployeeLists() {
    return this.restangular.all('auth').customGET('employee').toPromise();
  }
 
}
