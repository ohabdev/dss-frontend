import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'hrm', loadChildren: () => import('./hrm/hrm-routing.module').then(m => m.HRMRoutingModule) },     
    ]
  },
  {
    path: '',
    children: [
      { path: 'office-management', loadChildren: () => import('./office-management/office-management-routing.module').then(m => m.OfficeManagementRoutingModule) },     
    ]
  },
  {
    path: '',
    children: [
      { path: 'profile-information', loadChildren: () => import('./profile-information/profile-information-routing.module').then(m => m.ProfileInformationRoutingModule) },     
    ]
  },
  {
    path: '',
    children: [
      { path: 'document-management', loadChildren: () => import('./document-management/document-management-routing.module').then(m => m.DocumentManagementRoutingModule) },     
    ]
  },
  {
    path: '',
    children: [
      { path: 'promotion-transfer', loadChildren: () => import('./promotion-transfer/promotion-transfer-routing.module').then(m => m.PromotionTransferRoutingModule) },     
    ]
  },
  {
    path: '',
    children: [
      { path: 'security-management', loadChildren: () => import('./security-management/security-management-routing.module').then(m => m.SecurityManagementRoutingModule) },     
    ]
  } ,
  {
    path: '',
    children: [
      { path: 'reports', loadChildren: () => import('./reports/reports-routing.module').then(m => m.ReportsRoutingModule) },     
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }