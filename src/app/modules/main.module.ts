import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main.routing';
import { HrmModule } from './hrm/hrm.module';
import { OfficeManagementModule } from './office-management/office-management.module';
import { ProfileInformationModule } from './profile-information/profile-information.module';
import { DocumentManagementModule } from './document-management/document-management.module';
import { PromotionTransferModule } from './promotion-transfer/promotion-transfer.module';
import { SecurityManagementModule } from './security-management/security-management.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HrmModule,
    MainRoutingModule,
    OfficeManagementModule,
    ProfileInformationModule,
    DocumentManagementModule,
    PromotionTransferModule,
    SecurityManagementModule
  ]
})
export class MainModule { }
