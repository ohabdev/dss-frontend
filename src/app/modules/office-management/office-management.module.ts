import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OfficeTypeModule } from './office-type/office-type.module';
import { OfficeCategoryModule } from './office-category/office-category.module';
import { OfficeEntryModule } from './office-entry/office-entry.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OfficeTypeModule,
    OfficeCategoryModule,
    OfficeEntryModule
  ],
  exports: [
    RouterModule,
  ]
})
export class OfficeManagementModule { }
