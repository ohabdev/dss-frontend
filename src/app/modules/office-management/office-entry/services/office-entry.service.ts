import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficeEntryService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private OfficeType: any = null;
  private getOfficeType: any;

  constructor(
    private restangular: Restangular
  ) { }

  getList() {
    return this.restangular.all('auth').customGET('office').toPromise();
  }

  getOfficeTypeList() {
    return this.restangular.all('auth').customGET('office-type/list').toPromise();
  }

  getDivisionList() {
    return this.restangular.all('auth').customGET('division/list').toPromise();
  }

  getDistrictList() {
    return this.restangular.all('auth').customGET('district/list').toPromise();
  }

  getDistrictByDivision($id): Promise<any> {
    return this.restangular.all('auth/division').customGET($id).toPromise();
  }

  getUpazilaList() {
    return this.restangular.all('auth').customGET('upazila/list').toPromise();
  }

  getUpazilaByDistrict($id): Promise<any> {
    return this.restangular.all('auth/district').customGET($id).toPromise();
  }
  
  getOfficeCategoryList() {
    return this.restangular.all('auth').customGET('office-category/list').toPromise();
  }

  createOfficeEntry(data: any): Promise<any> {
    return this.restangular.all('auth/office').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
  singleOfficeEntry($id): Promise<any> {
    return this.restangular.one('auth/office').customGET($id).toPromise();
  }

  updateOfficeEntry($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/office',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteOfficeEntry($id): Promise<any> {
    return this.restangular.one('auth/office').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
}