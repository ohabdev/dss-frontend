import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import{ OfficeEntryService } from '../services/office-entry.service';
import { from } from 'rxjs';


@Component({
  selector: 'app-create-office-entry',
  templateUrl: './create-office-entry.component.html',
  styleUrls: ['./create-office-entry.component.css']
})
export class CreateOfficeEntryComponent implements OnInit {
  public isLoading: boolean = true;
  private OfficeEntry: OfficeEntryService;
  private Toasty: TostyService;
  public OfficeEntryData = {
    office_type_id: '',
    office_name: '',
    office_name_bn: '',
    office_code: '',
    division_id: '',
    district_id: '',
    upazila_id: '',
    office_category_id: '',
    status: 1
  };
  
  public office_type_list: any = []; 
  public office_category_list: any = []; 
  public division_list: any = []; 
  public district_list: any = []; 
  public upazila_list: any = []; 
  
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(officeEntryService: OfficeEntryService, private toasty: TostyService, private location: Location) { 
    this.OfficeEntry = officeEntryService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.OfficeEntry.getOfficeTypeList().then((res) => {
      this.office_type_list = res.data;
    });

    this.OfficeEntry.getDivisionList().then((res) => {
      this.division_list = res.data;
    });

    this.OfficeEntry.getDistrictList().then((res) => {
      this.district_list = res.data;
    });

    this.OfficeEntry.getOfficeCategoryList().then((res) => {
      this.office_category_list = res.data;
    });

    this.OfficeEntry.getUpazilaList().then((res) => {
      this.upazila_list = res.data;
      this.isLoading = false;
    });
  }


  createOfficeEntryOnSubmit(officeEntryFrm: NgForm) {
    this.submitted = true;

    //console.log(officeEntryFrm);

    // if (officeEntryFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }

    this.isLoading = true;
    this.OfficeEntry.createOfficeEntry(this.OfficeEntryData).then(() => {
      this.isLoading = false;
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      this.isLoading = false;
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
   
  districtByDivision(event){ 
    this.OfficeEntry.getDistrictByDivision(event.target.value).then((res) => {
      this.district_list = res.data.districts;
    });
  }

  upazilaByDistrict(event){ 
    this.OfficeEntry.getUpazilaByDistrict(event.target.value).then((res) => {
      this.upazila_list = res.data.upazilas;
    });
  }
  

}

