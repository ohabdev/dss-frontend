import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOfficeEntryComponent } from './create-office-entry.component';

describe('CreateOfficeEntryComponent', () => {
  let component: CreateOfficeEntryComponent;
  let fixture: ComponentFixture<CreateOfficeEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateOfficeEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOfficeEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
