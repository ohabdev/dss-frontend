import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { OfficeEntryService } from '../services/office-entry.service';


@Component({
  selector: 'app-office-entry-view',
  templateUrl: './office-entry-view.component.html',
  styleUrls: ['./office-entry-view.component.css']
})
export class OfficeEntryViewComponent implements OnInit {
  public isLoading: boolean = true;
  private OfficeEntry: OfficeEntryService;
  private Toasty: TostyService;
  private _location: any;
  public OfficeEntryData: any = false;
  public id: any = false;

  constructor(
    officeEntryService: OfficeEntryService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.OfficeEntry = officeEntryService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if(this.id){
        
        this.OfficeEntry.singleOfficeEntry(this.id).then((res) => {
          this.OfficeEntryData = res.data;
          this.isLoading = false;

          console.log(this.OfficeEntryData);
        })
        .catch((res) => { 
          this.isLoading = false;
          if(res.data.errors){
            res.data.errors.forEach((_error: any) => {
              this.Toasty.showError(_error.message[0], 'Sorry!',);
            });
          }else{
            this.Toasty.showError(res.message, 'Sorry!',);
          }
        });
      }

    });

  }

  deleteOfficeEntry( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.isLoading = true;
        this.OfficeEntry.deleteOfficeEntry(id).then((res) => {
          this.isLoading = false;
          this._location.back();
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }
}
