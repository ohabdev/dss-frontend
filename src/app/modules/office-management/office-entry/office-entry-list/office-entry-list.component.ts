import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import{ OfficeEntryService } from '../services/office-entry.service';

@Component({
  selector: 'app-office-entry-list',
  templateUrl: './office-entry-list.component.html',
  styleUrls: ['./office-entry-list.component.css']
})
export class OfficeEntryListComponent implements OnInit {
  public isLoading: boolean = true;
  public items: any = []; 
  Toasty: any;


  constructor(private officeEntryService: OfficeEntryService,  private toasty: TostyService) { 
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.officeEntryService.getList().then((res) => {
      this.items = res.data.data;
      this.isLoading = false;
    });
  }
  

  deleteOfficeEntry( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.isLoading = true;
        this.officeEntryService.deleteOfficeEntry(id).then((res) => {
          this.getData();
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
