import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OfficeEntryListComponent } from './office-entry-list/office-entry-list.component';
import { OfficeEntryViewComponent } from './office-entry-view/office-entry-view.component';
import { CreateOfficeEntryComponent } from './create-office-entry/create-office-entry.component';
import { EditOfficeEntryComponent } from './edit-office-entry/edit-office-entry.component';

const routes: Routes = [{
  path: 'list',
  component: OfficeEntryListComponent,
  data: {title: 'Office Entry List'} 
},
{
  path: 'create',
  component: CreateOfficeEntryComponent,
  data: {title: 'Office Entry Add'} 
},
{
  path: 'view/:id',
  component: OfficeEntryViewComponent,
  data: {title: 'Office Entry View'} 
},
{
  path: 'edit/:id',
  component: EditOfficeEntryComponent,
  data: {title: 'Edit Office Type'} 
}
];

@NgModule({
  declarations: [OfficeEntryListComponent, CreateOfficeEntryComponent, EditOfficeEntryComponent, OfficeEntryViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class OfficeEntryModule { }
