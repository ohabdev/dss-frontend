import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { OfficeEntryService } from '../services/office-entry.service';

@Component({
  selector: 'app-edit-office-entry',
  templateUrl: './edit-office-entry.component.html',
  styleUrls: ['./edit-office-entry.component.css']
})
export class EditOfficeEntryComponent implements OnInit {
  public isLoading: boolean = true;
  private OfficeEntry: OfficeEntryService;
  private Toasty: TostyService;
  public OfficeEntryData = {
    office_type_id: '',
    office_name: '',
    office_name_bn: '',
    office_code: '',
    division_id: '',
    district_id: '',
    upazila_id: '',
    office_category_id: '',
    status: 1
  };
  
  public office_type_list: any = []; 
  public office_category_list: any = []; 
  public division_list: any = []; 
  public district_list: any = []; 
  public upazila_list: any = []; 
  
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    officeEntryService: OfficeEntryService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.OfficeEntry = officeEntryService;
    this.Toasty = toasty;
    this._location = location;  
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.isLoading = true;
      this.OfficeEntry.getOfficeTypeList().then((res) => {
        this.office_type_list = res.data;
      });
  
      this.OfficeEntry.getDivisionList().then((res) => {
        this.division_list = res.data;
      });
  
      this.OfficeEntry.getDistrictList().then((res) => {
        this.district_list = res.data;
      });
  
      this.OfficeEntry.getUpazilaList().then((res) => {
        this.upazila_list = res.data;
        this.isLoading = false;
      });
  
      
      this.OfficeEntry.getOfficeCategoryList().then((res) => {
        this.office_category_list = res.data;
      });

      if(this.id){
        this.isLoading = true;
        this.OfficeEntry.singleOfficeEntry(this.id).then((res) => {
          let {office_type_id,office_name,office_name_bn,office_code,division_id,district_id,upazila_id,office_category_id,status} = res.data;
          this.OfficeEntryData.office_type_id = office_type_id;
          this.OfficeEntryData.office_name = office_name;
          this.OfficeEntryData.office_name_bn = office_name_bn;
          this.OfficeEntryData.office_code = office_code;
          this.OfficeEntryData.division_id = division_id;
          this.OfficeEntryData.district_id = district_id;
          this.OfficeEntryData.upazila_id = upazila_id;
          this.OfficeEntryData.office_category_id = office_category_id;
          this.OfficeEntryData.status = status;
          // this.isLoading = false;
        })
        .catch((res) => { 
          this.isLoading = false;
          if(res.data.errors){
            res.data.errors.forEach((_error: any) => {
              this.Toasty.showError(_error.message[0], 'Sorry!',);
            });
          }else{
            this.Toasty.showError(res.message, 'Sorry!',);
          }
        });
      }

    });

  }

  editOfficeEntryOnSubmit(officeEntryFrm: NgForm) {
    this.submitted = true;
    // if (officeEntryFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    // }

    this.isLoading = true;
    this.OfficeEntry.updateOfficeEntry(this.id, this.OfficeEntryData)
      .then(() => {
        this.isLoading = false;
        this._location.back();
        this.Toasty.showSuccess('Update Successfully', 'Success');
      })
      .catch((res) => {
        this.isLoading = false;
        if(res.data.errors){
          res.data.errors.forEach((_error: any) => {
            this.Toasty.showError(_error.message[0], 'Sorry!',);
          });
        }else{
          this.Toasty.showError(res.message, 'Sorry!',);
        }
      });
  }
}
