import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOfficeEntryComponent } from './edit-office-entry.component';

describe('EditOfficeEntryComponent', () => {
  let component: EditOfficeEntryComponent;
  let fixture: ComponentFixture<EditOfficeEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditOfficeEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOfficeEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
