import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { OfficeCategoryServiceService } from '../services/office-category-service.service';

@Component({
  selector: 'app-office-category-update',
  templateUrl: './office-category-update.component.html',
  styleUrls: ['./office-category-update.component.css']
})
export class OfficeCategoryUpdateComponent implements OnInit {
  private OfficeCAtegory: OfficeCategoryServiceService;
  private Toasty: TostyService;

  public OfficeCategoryData = {
    office_category: '',
    office_category_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    private OfficeCAtegoryService: OfficeCategoryServiceService, 
    private toasty: TostyService,
    private location: Location,
    private router : Router,
    private route: ActivatedRoute
  ) { 
    this.OfficeCAtegory = OfficeCAtegoryService;
    this.Toasty = toasty;
    this._location = location;
    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.OfficeCAtegory.singleOfficeCAtegory(this.id).then((res) => {
          console.log(res.data);
          this.OfficeCategoryData = res.data;
          console.log(this.OfficeCategoryData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });

  }

  editOfficeCAtegoryOnSubmit(officeCategoryFrm: NgForm) {
    this.submitted = true;
    if (officeCategoryFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.OfficeCAtegory.updateOfficeCAtegory(this.id, this.OfficeCategoryData)
    .then(() => {
      this._location.back();
      this.Toasty.showSuccess('Update Successfully', 'Success');
    })

    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
   
  }


}
