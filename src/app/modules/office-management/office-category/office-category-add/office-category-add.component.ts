import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { OfficeCategoryServiceService } from '../services/office-category-service.service';
import { TostyService } from 'src/app/shared/services/tosty.service';

@Component({
  selector: 'app-office-category-add',
  templateUrl: './office-category-add.component.html',
  styleUrls: ['./office-category-add.component.css']
})
export class OfficeCategoryAddComponent implements OnInit {
  private OfficeCAtegory: OfficeCategoryServiceService;
  private Toasty: TostyService;

  public OfficeCategoryData = {
    office_category: '',
    office_category_bn: ''
  };

  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(
    private OfficeCAtegoryService: OfficeCategoryServiceService, 
    private toasty: TostyService,
    private location: Location
  ) { 
    this.OfficeCAtegory = OfficeCAtegoryService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
  }
  createOfficeCAtegoryOnSubmit(officeCategoryFrm: NgForm) {
    this.submitted = true;
    if (officeCategoryFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.OfficeCAtegory.createOfficeCAtegory(this.OfficeCategoryData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });


  }
}
