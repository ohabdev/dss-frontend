import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficeCategoryServiceService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private OfficeType: any = null;
  private getOfficeType: any;

  constructor(
    private restangular: Restangular
  ) { }

  getList() {
    return this.restangular.all('auth').customGET('office-category').toPromise();
  }
  createOfficeCAtegory(data: any): Promise<any> {
    return this.restangular.all('auth/office-category').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
  singleOfficeCAtegory($id): Promise<any> {
    return this.restangular.one('auth/office-category/').customGET($id).toPromise();
  }

  updateOfficeCAtegory($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/office-category',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteOfficeCategory($id): Promise<any> {
    return this.restangular.one('auth/office-category/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
}

