import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { OfficeCategoryServiceService } from '../services/office-category-service.service';

@Component({
  selector: 'app-office-category-single',
  templateUrl: './office-category-single.component.html',
  styleUrls: ['./office-category-single.component.css']
})
export class OfficeCategorySingleComponent implements OnInit {
  public item: any = []; 
  id:any;

  constructor(
    private OfficeCAtegoryService: OfficeCategoryServiceService, 
    private route: ActivatedRoute
  ) { 
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
  }

  ngOnInit(): void {
    this.OfficeCAtegoryService.singleOfficeCAtegory(this.id).then((res) => {
      this.item = res.data;
    });
  }

}
