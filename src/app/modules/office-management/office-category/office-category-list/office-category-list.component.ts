
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { OfficeCategoryServiceService } from '../services/office-category-service.service';

@Component({
  selector: 'app-office-category-list',
  templateUrl: './office-category-list.component.html',
  styleUrls: ['./office-category-list.component.css']
})
export class OfficeCategoryListComponent implements OnInit {
  public category: any = []; 
  Toasty: any;

  constructor(
    private OfficeCategoryService: OfficeCategoryServiceService, 
    private toasty: TostyService
  ) { 
    this.Toasty = toasty;    
    this.OfficeCategoryService.getList().then((res) => {
      this.category = res.data.data;      
    });
  }

  ngOnInit(): void {
  }
  deleteOfficeCAtegory( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.OfficeCategoryService.deleteOfficeCategory(id).then((res) => {
          this.OfficeCategoryService.getList().then((res) => {
            this.category = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })   
    
  }
}
