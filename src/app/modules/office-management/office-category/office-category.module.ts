import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OfficeCategoryListComponent } from './office-category-list/office-category-list.component';
import { OfficeCategoryAddComponent } from './office-category-add/office-category-add.component';
import { OfficeCategoryUpdateComponent } from './office-category-update/office-category-update.component';
import { OfficeCategorySingleComponent } from './office-category-single/office-category-single.component';


const routes: Routes = [{
  path: 'list',
  component: OfficeCategoryListComponent,
  data: {title: 'Office Category List'} 
},
{
  path: 'create',
  component: OfficeCategoryAddComponent,
  data: {title: 'Office Category Add'} 
},
{
  path: 'list/:id',
  component: OfficeCategorySingleComponent,
  data: {title: 'Single Office Category'} 
},
{
  path: 'edit/:id',
  component: OfficeCategoryUpdateComponent,
  data: {title: 'Edit Office Type'} 
}
];
@NgModule({
  declarations: [
    OfficeCategoryListComponent, 
    OfficeCategoryAddComponent,
    OfficeCategoryUpdateComponent,
    OfficeCategorySingleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class OfficeCategoryModule { }
