import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OfficeTypeListComponent } from './office-type-list/office-type-list.component';
import { OfficeTypeCreateComponent } from './office-type-create/office-type-create.component';
import { OfficeTypeEditComponent } from './office-type-edit/office-type-edit.component';
import { OfficeTypeSingleComponent } from './office-type-single/office-type-single.component';

const routes: Routes = [{
  path: 'list',
  component: OfficeTypeListComponent,
  data: {title: 'Office Type List'} 
},
{
  path: 'create',
  component: OfficeTypeCreateComponent,
  data: {title: 'Create Office Type'} 
},
{
  path: 'list/:id',
  component: OfficeTypeSingleComponent,
  data: {title: 'Single Office Type'} 
},
{
  path: 'edit/:id',
  component: OfficeTypeEditComponent,
  data: {title: 'Edit Office Type'} 
}
];

@NgModule({
  declarations: [
    OfficeTypeListComponent, 
    OfficeTypeCreateComponent, 
    OfficeTypeEditComponent,
    OfficeTypeSingleComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class OfficeTypeModule { }
