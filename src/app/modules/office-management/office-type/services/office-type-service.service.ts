import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class OfficeTypeService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }
  
  private alias: any = null;
  private OfficeType: any = null;
  private getOfficeType: any;

  constructor(
    private restangular: Restangular
  ) { }

  getList() {
    return this.restangular.all('auth').customGET('office-type').toPromise();
  }

  createOfficeType(data: any): Promise<any> {
    return this.restangular.all('auth/office-type').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleOfficeType($id): Promise<any> {
    return this.restangular.one('auth/office-type/').customGET($id).toPromise();
  }

  updateOfficeType($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/office-type',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteOfficeType($id): Promise<any> {
    return this.restangular.one('auth/office-type/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }
}
