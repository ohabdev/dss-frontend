import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { OfficeTypeService } from '../services/office-type-service.service';

@Component({
  selector: 'app-office-type-edit',
  templateUrl: './office-type-edit.component.html',
  styleUrls: ['./office-type-edit.component.css']
})
export class OfficeTypeEditComponent implements OnInit {
  private OfficeType: OfficeTypeService;
  private Toasty: TostyService;

  public OfficeTypeData = {
    office_type: '',
    office_type_bn: '',
    status: 1
  };
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    private OfficeTypeService: OfficeTypeService, 
    private toasty: TostyService, 
    private location: Location,
    private router : Router,
    private route: ActivatedRoute
  ) { 
    this.OfficeType = OfficeTypeService;
    this.Toasty = toasty;
    this._location = location;

   this.route.paramMap.subscribe((params) => {
    console.log(params.get('id'));
  });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.OfficeType.singleOfficeType(this.id).then((res) => {
          console.log(res.data);
          this.OfficeTypeData = res.data;
          console.log(this.OfficeTypeData);
        })
        .catch((res) => { 
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });
  }

  editOfficeTypeOnSubmit(officeTypeFrm: NgForm) {
    this.submitted = true;
    if (officeTypeFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.OfficeType.updateOfficeType(this.id, this.OfficeTypeData)
    .then(() => {
      this._location.back();
      this.Toasty.showSuccess('Update Successfully', 'Success');
    })

    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
   
  }


}
