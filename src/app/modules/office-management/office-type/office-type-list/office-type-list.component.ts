import { Component, OnInit } from '@angular/core';
import { OfficeTypeService } from '../services/office-type-service.service';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-office-type-list',
  templateUrl: './office-type-list.component.html',
  styleUrls: ['./office-type-list.component.css']
})
export class OfficeTypeListComponent implements OnInit {
  public items: any = []; 
  Toasty: any;

  constructor(
    private OfficeTypeService: OfficeTypeService, 
     private toasty: TostyService
  ) { 
    this.Toasty = toasty;    
    this.OfficeTypeService.getList().then((res) => {
      this.items = res.data.data;      
    });
  }

  ngOnInit(): void {
    
  }

  deleteOfficeType( id:any ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.OfficeTypeService.deleteOfficeType(id).then((res) => {
          this.OfficeTypeService.getList().then((res) => {
            this.items = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }



}
