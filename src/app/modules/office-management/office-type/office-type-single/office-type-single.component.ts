import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { OfficeTypeService } from '../services/office-type-service.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-office-type-single',
  templateUrl: './office-type-single.component.html',
  styleUrls: ['./office-type-single.component.css']
})
export class OfficeTypeSingleComponent implements OnInit {
  public item: any = []; 
  id:any;
  constructor(
    private OfficeTypeService: OfficeTypeService, 
    private route: ActivatedRoute
  ) { 
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });

  }

  ngOnInit(): void {
    this.OfficeTypeService.singleOfficeType(this.id).then((res) => {
      this.item = res.data;
    });
  }

}
