import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Location} from '@angular/common';
import { OfficeTypeService } from '../services/office-type-service.service';
import { TostyService } from 'src/app/shared/services/tosty.service';

@Component({
  selector: 'app-office-type-create',
  templateUrl: './office-type-create.component.html',
  styleUrls: ['./office-type-create.component.css']
})
export class OfficeTypeCreateComponent implements OnInit {
  private OfficeType: OfficeTypeService;
  private Toasty: TostyService;

  public OfficeTypeData = {
    office_type: '',
    office_type_bn: ''
  };

  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(

    private OfficeTypeService: OfficeTypeService, 
    private toasty: TostyService,
    private location: Location
    
  ) { 
    this.OfficeType = OfficeTypeService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
  }

  createfficeTypeOnSubmit(officeTypeFrm: NgForm) {
    this.submitted = true;
    if (officeTypeFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }
    this.OfficeType.createOfficeType(this.OfficeTypeData).then(() => {
      this._location.back();
      this.Toasty.showSuccess('Successfully Created', 'Success',);
    })
    .catch((res) => { 
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });


  }

}
