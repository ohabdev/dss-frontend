import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'office-type', loadChildren: () => import('./office-type/office-type.module').then(m => m.OfficeTypeModule) }    
    ]
  },
  {
    path: '',
    children: [
      { path: 'office-category', loadChildren: () => import('./office-category/office-category.module').then(m => m.OfficeCategoryModule) }    
    ]
  },
  {
    path: '',
    children: [
      { path: 'office-entry', loadChildren: () => import('./office-entry/office-entry.module').then(m => m.OfficeEntryModule) }    
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfficeManagementRoutingModule { }