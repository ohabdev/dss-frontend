import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VacantPositionListComponent } from './vacant-position-list/vacant-position-list.component';
import { WorkingEmployeeListComponent } from './working-employee-list/working-employee-list.component';

export const routes: Routes = [
  {
    path: '',
    children: [
     {
        path: 'working-employee-list',
        component: WorkingEmployeeListComponent,
        data: {title: 'Working Employee List'} 
     },
     {
        path: 'vacant-position-list',
        component: VacantPositionListComponent,
        data: {title: 'Vacant Position List'} 
     }
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
