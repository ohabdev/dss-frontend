import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VacantPositionListComponent } from './vacant-position-list/vacant-position-list.component';
import { WorkingEmployeeListComponent } from './working-employee-list/working-employee-list.component';



@NgModule({
  declarations: [
    VacantPositionListComponent, 
    WorkingEmployeeListComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ReportsModule { }
