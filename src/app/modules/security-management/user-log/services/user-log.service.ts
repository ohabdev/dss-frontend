import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserLogService {

  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private UserLog: any = null;
  private getUserLog: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1,data: any) {
    let queryString = '';
    for (const iterator in data) {
      if(data[iterator]){
        queryString += `&${iterator}=${data[iterator]}`;
      }
    }
    return this.restangular.all('auth').customGET('activity-logs?page='+page+queryString).toPromise();
  }

  getDivisionList() {
    return this.restangular.all('auth').customGET('division').toPromise();
  }

  getDistrictList() {
    return this.restangular.all('auth').customGET('district').toPromise();
  }

}
