import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UserLogService } from '../services/user-log.service';


@Component({
  selector: 'app-user-log-list',
  templateUrl: './user-log-list.component.html',
  styleUrls: ['./user-log-list.component.css']
})
export class UserLogListComponent implements OnInit {

  public items: any = [];
  public pagination: any = [];
  public division_list: any = [];
  public district_list: any = [];
  Toasty: any;
  public submitted: boolean = false;
  public isLoading:boolean = false;
  
  public userLogData = {
    employee_id: '',
    user_name: '',
    division_id: '',
    district_id: '',
  };

  constructor(
    private userLogService: UserLogService,
    private toasty: TostyService
  ) {
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.userLogService.getDivisionList().then((res) => {
      this.division_list = res.data.data;
    });

    this.userLogService.getDistrictList().then((res) => {
      this.district_list = res.data.data;
    });

    //this.getPageDataFn();

  }

  getModuleName(str:string){
    if(str){
      let moduleTitle = '';
      let moduleArray = str.split("\\");
      if(moduleArray.length == 4){
        moduleTitle = moduleArray[2];
      }else if(moduleArray.length == 3){
        moduleTitle = moduleArray[2];
      }else if(moduleArray.length == 2){
        moduleTitle = moduleArray[1];
      }else if(moduleArray.length == 1){
        moduleTitle = moduleArray[0];
      }
      if(moduleTitle){
        moduleTitle = moduleTitle.match(/[A-Z][a-z]+/g).join(" ");
      }
      return moduleTitle;
    }
  }

  getSubModuleName(str:string){
    if(str){
      let subModuleTitle = '';
      let moduleArray = str.split("\\");
      if(moduleArray.length == 4){
        subModuleTitle = moduleArray[3];
      }
      if(subModuleTitle){
        subModuleTitle = subModuleTitle.match(/[A-Z][a-z]+/g).join(" ");
      }
      return subModuleTitle;
    }
  }
  

  getPageDataFn(page: number = 1) {
    this.userLogService.getList(page, this.userLogData).then((res) => {
      this.isLoading = false;
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];

      if (this.items.length<1) {
        return this.Toasty.showError('Data Not Found!!!', 'Error!',);
      }

    });
  } 

  searchUserLogOnSubmit(userLogFrm: NgForm) {
    this.submitted = true;
    this.isLoading = true;

    if (userLogFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    }

    this.getPageDataFn(1);
  }

}
