import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserLogListComponent } from './user-log-list/user-log-list.component';

const routes: Routes = [
  {
    path: '',
    component: UserLogListComponent,
    data: { title: 'User Log' },
  },
];

@NgModule({
  declarations: [UserLogListComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class UserLogModule {}
