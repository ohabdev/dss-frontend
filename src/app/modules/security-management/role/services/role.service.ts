import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private Role: any = null;
  private getRole: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('roles?page='+page).toPromise();
  }

  getPermissionList(page:number=1) {
    return this.restangular.all('auth').customGET('permissions?page='+page).toPromise();
  }
  

  createRole(data: any): Promise<any> {
    return this.restangular.all('auth/roles').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleRole($id): Promise<any> {
    return this.restangular.all('auth/roles').customGET($id).toPromise();
  }

  updateRole($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/roles',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteRole($id): Promise<any> {
    return this.restangular.one('auth/roles/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
