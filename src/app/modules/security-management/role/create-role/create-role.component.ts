import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import { RoleService } from '../services/role.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css'],
})
export class CreateRoleComponent implements OnInit {
  public isLoading: boolean = true;
  private roleService: RoleService;
  private Toasty: TostyService;
  public RoleData = {
    name: '',
    permissions: [],
  };
  public submitted: boolean = false;
  router: any;
  private _location: any;
  public items: any = [];
  public pagination: any = [];

  constructor(
    roleService: RoleService,
    private toasty: TostyService,
    private location: Location
  ) {
    this.roleService = roleService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {
    this.getPermissionDataFn();
  }

  createRoleOnSubmit(roleFrm: NgForm) {
    this.submitted = true;

    //console.log(roleFrm);

    // if (roleFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    // }

    this.isLoading = true;

    this.roleService
      .createRole(this.RoleData)
      .then(() => {
        this._location.back();
        this.Toasty.showSuccess('Successfully Created', 'Success');
        this.isLoading = false;
      })
      .catch((res) => {
        this.isLoading = false;
        console.log(res.data.message);
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!');
        });
        return;
      });
  }

  selectPermission(evt: any, name: string) {
    if (evt.currentTarget.checked) {
      this.RoleData.permissions.push(name);
    } else {
      // let keyIndex = this.RoleData.permissions.indexOf(name);
      // if(keyIndex !== -1){
      //   delete this.RoleData.permissions[keyIndex];
      // }

      var index_to_delete = this.RoleData.permissions.indexOf(name);
      console.log(index_to_delete);
      this.RoleData.permissions = this.RoleData.permissions.filter(function (item, index) {
        return index !== index_to_delete;
      });
    }
    console.log(this.RoleData);
  }

  isChecked(name: string) {
    if (this.RoleData.permissions.indexOf(name) !== -1) {
      return true;
    }
    return false;
  }

  getPermissionDataFn(page: number = 1) {
    this.isLoading = true;
    this.roleService.getPermissionList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
      this.isLoading = false;
    });
  }

  getUserDataFn(page: number = 1) {
    this.isLoading = true;
    this.roleService.getPermissionList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
      this.isLoading = false;
    });
  }
}
