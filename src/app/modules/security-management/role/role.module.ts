import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoleListComponent } from './role-list/role-list.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { ViewRoleComponent } from './view-role/view-role.component';


const routes: Routes = [{
  path: '',
  component: RoleListComponent,
  data: {title: 'Role List'} 
},
{
  path: 'create',
  component: CreateRoleComponent,
  data: {title: 'Create Role'} 
},
{
  path: 'edit/:id',
  component: EditRoleComponent,
  data: {title: 'Edit Role'} 
},
{
  path: 'view/:id',
  component: ViewRoleComponent,
  data: {title: 'View Role'} 
},

];


@NgModule({
  declarations: [RoleListComponent, CreateRoleComponent, EditRoleComponent, ViewRoleComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RoleModule { }
