import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TostyService } from '../../../../shared/services/tosty.service';
import { RoleService } from '../services/role.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.css']
})
export class ViewRoleComponent implements OnInit {
  public isLoading: boolean = true;
  private roleService: RoleService;
  private Toasty: TostyService;
  public id: any = false;
  public RoleData:any = [];
  public submitted: boolean = false;
  router: any;
  private _location: any;

  constructor(
    roleService: RoleService,
    private toasty: TostyService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.roleService = roleService;
    this.Toasty = toasty;
    this._location = location;
  }
  ngOnInit(): void {

    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');

      if (this.id) {
        this.roleService.singleRole(this.id)
          .then((res) => {            
            this.isLoading = false;
            this.RoleData = res.data;
          })
          .catch((res) => {
            this.isLoading = false;
            console.log(res.data.message);
            return this.Toasty.showError(res.data.message, 'Sorry!');
          });
      }
    });

  }

  deleteRole(id: any) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.roleService
          .deleteRole(id)
          .then((res) => {
            this._location.back();
            Swal.fire({
              icon: 'success',
              title: 'Deleted!',
              text: 'Your imaginary file has been deleted.',
              showCancelButton: false,
              showConfirmButton: false,
              timer: 1000,
            });
          });
      }
    });
  }


}