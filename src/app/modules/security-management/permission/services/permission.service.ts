import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private Permission: any = null;
  private getPermission: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('permissions?page='+page).toPromise();
  }

  createPermission(data: any): Promise<any> {
    return this.restangular.all('auth/permissions').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singlePermission($id): Promise<any> {
    return this.restangular.all('auth/permissions').customGET($id).toPromise();
  }

  updatePermission($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/permissions',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deletePermission($id): Promise<any> {
    return this.restangular.one('auth/permissions/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
