import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PermissionListComponent } from './permission-list/permission-list.component';

const routes: Routes = [
  {
    path: '',
    component: PermissionListComponent,
    data: { title: 'Permission List' },
  },
];

@NgModule({
  declarations: [PermissionListComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class PermissionModule {}
