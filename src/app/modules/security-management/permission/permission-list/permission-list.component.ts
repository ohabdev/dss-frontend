import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { PermissionService } from '../services/permission.service';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.css'],
})
export class PermissionListComponent implements OnInit {
  public isLoading: boolean = true;
  public items: any = [];
  public pagination: any = [];
  Toasty: any;
  public submitBtnText: string = 'Create New';
  public PermissionData = {
    id: '',
    name: '',
  };

  public PermissionViewData: any = false;

  constructor(
    private permissionService: PermissionService,
    private toasty: TostyService
  ) {
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getPageDataFn();
  }

  getPageDataFn(page: number = 1) {
    this.isLoading = true;
    this.permissionService.getList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
      this.isLoading = false;
    });
  }

  viewPermission(data: any) {
    this.PermissionViewData = data;
    console.log(this.PermissionViewData);
  }

  submitPermission() {
    if (this.PermissionData.id) {
      this.permissionService
        .updatePermission(this.PermissionData.id, this.PermissionData)
        .then((res) => {
          this.clearPermission();
          this.getPageDataFn();
          this.Toasty.showSuccess(
            res.message,
            'Success'
          );
        })
        .catch((res) => {
          this.isLoading = false;
          console.log(res.data.message);
          res.data.errors.forEach((_error: any) => {
            this.Toasty.showError(_error.message[0], 'Sorry!');
          });
          return;
        });
    } else {
      delete this.PermissionData['id'];
      this.permissionService
        .createPermission(this.PermissionData)
        .then((res) => {
          this.clearPermission();
          this.getPageDataFn();
          this.Toasty.showSuccess(
            res.message,
            'Success'
          );
        })
        .catch((res) => {
          this.isLoading = false;
          console.log(res.data.message);
          res.data.errors.forEach((_error: any) => {
            this.Toasty.showError(_error.message[0], 'Sorry!');
          });
          return;
        });
    }
  }

  clearPermission() {
    this.PermissionData = {
      id: '',
      name: '',
    };
    this.submitBtnText = 'Create New';
  }

  editPermission(id: any, name: any) {
    this.submitBtnText = 'Update';
    this.PermissionData.id = id;
    this.PermissionData.name = name;
  }

  deletePermission(id: any) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.permissionService.deletePermission(id).then((res) => {
          if(this.PermissionData.id == id){
            this.clearPermission();
          }
          this.getPageDataFn();
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          });
        });
      }
    });
  }

}
