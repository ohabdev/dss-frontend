import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


export const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'user-log', loadChildren: () => import('./user-log/user-log.module').then(m => m.UserLogModule) },
      { path: 'permission', loadChildren: () => import('./permission/permission.module').then(m => m.PermissionModule) },
      { path: 'role', loadChildren: () => import('./role/role.module').then(m => m.RoleModule) },
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityManagementRoutingModule { }
