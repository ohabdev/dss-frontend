import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityManagementRoutingModule } from './security-management-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SecurityManagementRoutingModule
  ]
})
export class SecurityManagementModule { }
