import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private accessToken: string = null;
  private currentUser = null;
  private userLoaded = new Subject<any>();
  public userLoaded$ = this.userLoaded.asObservable();

  private _getUser: any;

  constructor(private restangular: Restangular) {}

  getCurrentUser() {
    if (this.currentUser) {
      return new Promise((resolve) => resolve(this.currentUser));
    }

    if (this._getUser && typeof this._getUser.then === 'function') {
      return this._getUser;
    }

    this._getUser = this.restangular
      .one('auth', 'user')
      .get()
      .toPromise()
      .then((resp) => {
        this.currentUser = resp.data;
        this.userLoaded.next(resp.data);
        window.appConfig.currentUser = this.currentUser;
        return resp.data;
      });

    
    return this._getUser;
  }
  login(credentials: any): Promise<any> {
    return this.restangular
      .all('auth/login')
      .post(credentials)
      .toPromise()
      .then((resp) => {
        localStorage.setItem('accessToken', resp.data.token);  
        localStorage.setItem('isLoggedin', 'yes');  
        this.getCurrentUser();     
        return resp;
      });
  }

  getAccessToken(): any {
    if (!this.accessToken) {
      this.accessToken = localStorage.getItem('accessToken');
    }

    return this.accessToken;
  }

  isLoggedin() {
    return localStorage.getItem('isLoggedin') === 'yes';
  }

  removeToken() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('isLoggedin');
  }

  logout() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('isLoggedin');
  }

  me(): Promise<any> {
    return this.restangular.one('auth', 'user').get().toPromise();
  }

  forgot(credentials: any): Promise<any> {
    return this.restangular
      .all('auth/reset-password-otp')
      .post(credentials)
      .toPromise()
      .then((resp) => {
        localStorage.setItem('forgotToken', resp.data.token);
        return resp.data.otp_code;
      });
  }

  verifyOTP(credentials: any): Promise<any> {
    return this.restangular
      .all('auth/verify-otp')
      .post(credentials)
      .toPromise()
      .then((resp) => {
        localStorage.removeItem('forgotToken');
        localStorage.setItem('resetToken', resp.data.reset_token);
      });
  }

  resetPassword(credentials: any): Promise<any> {
    return this.restangular
      .all('auth/reset-password')
      .post(credentials)
      .toPromise()
      .then((resp) => {
        localStorage.removeItem('resetToken');
      });
  }
}
