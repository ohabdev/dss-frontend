import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TostyService } from '../../../../shared/services/tosty.service';
import { SystemService } from '../../../../shared/services/system.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  private Auth: AuthService;
  private Toasty: TostyService;
  private systemService: SystemService

  public credentials = {
    phone_number: '',
  };

  public submitted: boolean = false;
  userLang: any;
  translateService: any;

  constructor(
    private auth: AuthService, 
    public router: Router, 
    private toasty: TostyService, 
    private translate: TranslateService, 
    private system: SystemService) {
      this.Auth = auth;
      this.Toasty = toasty;
      this.systemService = system; 
  }

  ngOnInit(): void {
    if(this.Auth.isLoggedin()){
      this.router.navigate(['/']);
      
    } 
  }

  forgot(forgotFrm: NgForm) {
    this.submitted = true;

    // console.log(this.credentials);

    if (forgotFrm.invalid) {
      return;
    }

    this.Auth.forgot(this.credentials).then((res) => {
      console.log(res);
      this.router.navigate(['/auth/verify-otp']);
      this.Toasty.showSuccess('OTP Send Successfully.', 'Success',);
      alert(res);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }

}
