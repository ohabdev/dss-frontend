import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { SystemService } from 'src/app/shared/services/system.service';
import * as $ from 'jquery';
import * as Localization from './lang.json';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private Auth: AuthService;
  private Toasty: TostyService;
  private systemService: SystemService;
  public localization:any;

  public credentials = {
    email: '',
    password: ''
  };
  public submitted: boolean = false;
  userLang: any;
  translateService: any;

  constructor(
    private auth: AuthService, 
    public router: Router, 
    private toasty: TostyService, 
    private translate: TranslateService, 
    private system: SystemService) {
      this.Auth = auth;
      this.Toasty = toasty;
      this.systemService = system;       
  }
  ngOnInit() {
    if(this.Auth.isLoggedin()){
      this.router.navigate(['/']);      
    }
    this.userLang = this.systemService.getUserLang();
    this.getLocalization();

  }

  changeLang(lang: any) {
    this.systemService.setUserLang(lang);
    this.userLang = lang;
    this.getLocalization();
  }

  getLocalization(){
    this.localization = Localization['default'][this.systemService.getUserLang()];
    console.log(Localization);
    console.log(this.localization);
  }

  login(loginFrm: NgForm) {
    this.submitted = true;

    // console.log(this.credentials);

    // if (loginFrm.invalid) {
    //   return;
    // }

    this.Auth.login(this.credentials).then(() => {
      const redirectUrl = sessionStorage.getItem('redirectUrl');
      if (redirectUrl) {
        sessionStorage.removeItem('redirectUrl');
        this.router.navigate([redirectUrl]);
      } else {
        this.router.navigate(['/']);
      }
      this.Toasty.showSuccess('Login Success', 'Success',);
    })
    .catch((res) => { 
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

}
