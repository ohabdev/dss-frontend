import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TostyService } from '../../../../shared/services/tosty.service';
import { SystemService } from '../../../../shared/services/system.service';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.css']
})
export class VerifyOtpComponent implements OnInit {
  private Auth: AuthService;
  private Toasty: TostyService;
  private systemService: SystemService

  public credentials = {
    token: '',
    otp_code: '',
  };

  public submitted: boolean = false;
  userLang: any;
  translateService: any;

  constructor(
    private auth: AuthService, 
    public router: Router, 
    private toasty: TostyService, 
    private translate: TranslateService, 
    private system: SystemService) {
      this.Auth = auth;
      this.Toasty = toasty;
      this.systemService = system; 
  }

  ngOnInit(): void {
    if(this.Auth.isLoggedin()){
      this.router.navigate(['/']);      
    } 

    this.credentials.token = localStorage.getItem('forgotToken');
    if(!this.credentials.token){
      this.router.navigate(['/auth/login']); 
    }

  }

  verifyOTP(verifyOTPFrm: NgForm) {
    this.submitted = true;

    // console.log(this.credentials);

    if (verifyOTPFrm.invalid) {
      return;
    }

    this.Auth.verifyOTP(this.credentials).then(() => {
      this.router.navigate(['/auth/reset-password']);
      this.Toasty.showSuccess('OTP checked successfully.', 'Success',);
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }

}
