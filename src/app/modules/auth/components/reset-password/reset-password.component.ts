import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TostyService } from '../../../../shared/services/tosty.service';
import { SystemService } from '../../../../shared/services/system.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  private Auth: AuthService;
  private Toasty: TostyService;
  private systemService: SystemService

  public credentials = {
    reset_token: '',
    password: '',
    password_confirmation:''
  };

  public submitted: boolean = false;
  userLang: any;
  translateService: any;

  constructor(
    private auth: AuthService, 
    public router: Router, 
    private toasty: TostyService, 
    private translate: TranslateService, 
    private system: SystemService) {
      this.Auth = auth;
      this.Toasty = toasty;
      this.systemService = system; 
  }

  ngOnInit(): void {
    
    if(this.Auth.isLoggedin()){
      this.router.navigate(['/']);      
    }
    
    this.credentials.reset_token = localStorage.getItem('resetToken');
    if(!this.credentials.reset_token){
      this.router.navigate(['/auth/login']); 
    }
  }

  resetPassword(resetPasswordFrm: NgForm) {
    this.submitted = true;

    // console.log(this.credentials);

    if (resetPasswordFrm.invalid) {
      this.Toasty.showError('Please enter Password and Confirmation Password ', 'Sorry!');
      return;
    }

    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    

    if(this.credentials.password.length < 8){
      this.Toasty.showError('Please enter at least 8 characters', 'Sorry!');
      return;
    }

    if(this.credentials.password != this.credentials.password_confirmation){
      this.Toasty.showError('Password and Confirmation Password does not match. Please try again. ', 'Sorry!');
      return;
    }

    if(!this.credentials.password.match(passw)){
      this.Toasty.showError('8 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter', 'Sorry!');
      return;
    }

    this.Auth.resetPassword(this.credentials).then(() => {
      this.router.navigate(['/auth/login']);
      this.Toasty.showSuccess('Password reset successfully.', 'Success');
    })
    .catch((res) => { 
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!',);
    });
  }

}
