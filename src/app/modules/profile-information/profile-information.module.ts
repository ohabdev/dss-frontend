import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesModule } from './employee/employee.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EmployeesModule
  ]
})
export class ProfileInformationModule { }
