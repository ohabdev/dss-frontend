import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuotaInformationListComponent } from './quota-information-list/quota-information-list.component';
import { CreateQuotaInformationComponent } from './create-quota-information/create-quota-information.component';
import { EditQuotaInformationComponent } from './edit-quota-information/edit-quota-information.component';

const routes: Routes = [
  {
    path: '',
    component: QuotaInformationListComponent,
    data: {title: 'Quota Information List'} 
  },
  {
    path: 'create',
    component: CreateQuotaInformationComponent,
    data: {title: 'Create Quota Information'} 
  },
  {
    path: 'edit/:id',
    component: EditQuotaInformationComponent,
    data: {title: 'Edit Quota Information'} 
  }
];

@NgModule({
  declarations: [
    QuotaInformationListComponent, 
    EditQuotaInformationComponent, 
    CreateQuotaInformationComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class QuotaInformationModule { }
