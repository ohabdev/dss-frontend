import { Component, OnInit } from '@angular/core';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { QuotaInformationService } from '../services/quota-information.service';


@Component({
  selector: 'app-quota-information-list',
  templateUrl: './quota-information-list.component.html',
  styleUrls: ['./quota-information-list.component.css']
})
export class QuotaInformationListComponent implements OnInit {

  items: any = []; 
  lang: any;
  constructor(private quotaInformationService: QuotaInformationService,  private toasty: TostyService) { 
 
    this.toasty = toasty;
    this.quotaInformationService.getList().then((res) => {
      this.items = res.data;
      console.log(this.items);
    });
  }

  ngOnInit(): void {
  }

}
