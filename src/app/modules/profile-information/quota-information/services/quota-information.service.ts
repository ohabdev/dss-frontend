import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class QuotaInformationService {

  

  private alias: any = null;
  private QuotaInformation: any = null;
  private getQuotaInformation: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('job/joining/information').toPromise();
  }
   
  createQuotaInformation(data: any): Promise<any> {
    return this.restangular.all('auth/QuotaInformation').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleQuotaInformation($id): Promise<any> {
    return this.restangular.all('auth/QuotaInformation').customGET($id).toPromise();
  }

  updateQuotaInformation($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/QuotaInformation',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteQuotaInformation($id): Promise<any> {
    return this.restangular.one('auth/job/joining/information/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  
  
}
