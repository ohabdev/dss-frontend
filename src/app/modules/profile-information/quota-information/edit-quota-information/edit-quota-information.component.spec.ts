import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditQuotaInformationComponent } from './edit-quota-information.component';

describe('EditQuotaInformationComponent', () => {
  let component: EditQuotaInformationComponent;
  let fixture: ComponentFixture<EditQuotaInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditQuotaInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditQuotaInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
