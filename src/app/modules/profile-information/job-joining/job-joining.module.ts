import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { JobJoiningListComponent } from './job-joining-list/job-joining-list.component';
import { CreateJobJoiningComponent } from './create-job-joining/create-job-joining.component';
import { EditJobJoiningComponent } from './edit-job-joining/edit-job-joining.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: JobJoiningListComponent,
    data: {title: 'Employee Information List'} 
  },
  {
    path: 'create',
    component: CreateJobJoiningComponent,
    data: {title: 'Create Employee Information'} 
  },
  {
    path: 'edit/:id',
    component: EditJobJoiningComponent,
    data: {title: 'Edit Employee Information'} 
  }
];

@NgModule({
  declarations: [
    CreateJobJoiningComponent, 
    EditJobJoiningComponent, 
    JobJoiningListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class JobJoiningModule { }
