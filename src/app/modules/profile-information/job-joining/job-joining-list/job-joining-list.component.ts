import { Component, OnInit } from '@angular/core';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2';
import { JobJoiningService } from '../services/job-joining.service';

@Component({
  selector: 'app-job-joining-list',
  templateUrl: './job-joining-list.component.html',
  styleUrls: ['./job-joining-list.component.css']
})
export class JobJoiningListComponent implements OnInit {

  items: any = []; 
  lang: any;
  constructor(private jobJoiningService: JobJoiningService,  private toasty: TostyService) { 
 
    this.toasty = toasty;
    this.jobJoiningService.getList().then((res) => {
      this.items = res.data;
      console.log(this.items);
    });
  }


  ngOnInit(): void {
  }

  deleteJobJoining( id ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.jobJoiningService.deleteJobJoining(id).then((res) => {
          this.jobJoiningService.getList().then((res) => {
            this.items = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }
}
