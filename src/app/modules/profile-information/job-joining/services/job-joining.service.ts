import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class JobJoiningService {

  

  private alias: any = null;
  private JobJoining: any = null;
  private getJobJoining: any;
  constructor(private restangular: Restangular) { }

  getList() {
    return this.restangular.all('auth').customGET('job/joining/information').toPromise();
  }
  getGenderList() {
    return this.restangular.all('auth').customGET('gender').toPromise();
  }
  createJobJoining(data: any): Promise<any> {
    return this.restangular.all('auth/JobJoining').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  singleJobJoining($id): Promise<any> {
    return this.restangular.all('auth/JobJoining').customGET($id).toPromise();
  }

  updateJobJoining($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/JobJoining',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteJobJoining($id): Promise<any> {
    return this.restangular.one('auth/job/joining/information/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  
  
}
