import { Component, OnInit } from '@angular/core';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { JobJoiningService } from '../services/job-joining.service';

@Component({
  selector: 'app-edit-job-joining',
  templateUrl: './edit-job-joining.component.html',
  styleUrls: ['./edit-job-joining.component.css']
})
export class EditJobJoiningComponent implements OnInit {

  items: any = []; 
  lang: any;
  constructor(private jobJoiningService: JobJoiningService,  private toasty: TostyService) { 
 
    this.toasty = toasty;
    this.jobJoiningService.getList().then((res) => {
      this.items = res.data;
      console.log(this.items);
    });
  }


  ngOnInit(): void {
  }

}
