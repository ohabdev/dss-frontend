import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    children: [
      { 
        path: 'employee', loadChildren: () => import('./employee/employee.module').then(m => m.EmployeesModule) 
      },
      { 
        path: 'job-joining', loadChildren: () => import('./job-joining/job-joining.module').then(m => m.JobJoiningModule) 
      },
      { 
        path: 'quota-information', loadChildren: () => import('./quota-information/quota-information.module').then(m => m.QuotaInformationModule) 
      }
    ]
  },
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProfileInformationRoutingModule { }
