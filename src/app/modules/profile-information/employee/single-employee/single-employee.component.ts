import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Location} from '@angular/common';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { EmployeeService } from '../services/employee.service'; 
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-employee',
  templateUrl: './single-employee.component.html',
  styleUrls: ['./single-employee.component.css']
})
export class SingleEmployeeComponent implements OnInit {

  private Employee: EmployeeService;
  private Toasty: TostyService; 
  public EmployeeData = {
    profile_id: '',
    name: '',
    name_bn: '', 
    photo: '',
    date_of_birth: '',
    gender_id: '',
    marital_status: '',
    fathers_name: '',
    fathers_name_bn: '',
    mothers_name: '',
    mothers_name_bn: '',
    email: '',
    username: '',
    phone_number: '',
  }; 
  public EmployeeJobData = {
    id: '',
    employee_id: '',
    office_id: '',
    designation_id: '', 
    department_id: '',
    job_type_id: '',
    employee_class_id: '',
    grade_id: '',
    joining_date: '',
    pension_date: '',
    prl_date: ''
  };
  
  public EmployeeQuota = {
    id: '',
    employee_id: '',
    quota_information_id: '',
    quota_information_detail_id: '', 
    description: '',
    description_bn: '',
  };

  public EmployeePresentAddressData = {
    id: '',
    employee_id: '',
    division_id: '',
    district_id: '', 
    upazila_id: '',
    police_station: '',
    police_station_bn: '',
    post_office: '',
    post_office_bn: '',
    road_word_no: '', 
    road_word_no_bn: '',
    village_house_no: '',
    village_house_no_bn: '',
  };

  public EmployeePermanentAddressData = {
    id: '',
    employee_id: '',
    division_id: '',
    district_id: '', 
    upazila_id: '',
    police_station: '',
    police_station_bn: '',
    post_office: '',
    post_office_bn: '',
    road_word_no: '', 
    road_word_no_bn: '',
    village_house_no: '',
    village_house_no_bn: '',
  };


  public EmployeeEducationQualificationData = {
    id: '',
    employee_id: '',
    name_of_degree: '',
    name_of_degree_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    police_station_bn: '',
    board_university: '',
    board_university_bn: '',
    passing_year: '', 
    division_cgpa: '', 
  };

  public EmployeeSpouseData = {
    id: '',
    employee_id: '',
    name: '',
    name_bn: '', 
    relation: '',
    division_id: '',
    district_id: '',
    occupation: '',
    phone_no: '',
    mobile_no: '', 
  };

  public EmployeeChildrensInfoData:any [] = [{
    id: '',
    employee_id: '',
    name_of_children: '',
    name_of_children_bn: '', 
    birth_certificate: '',
    nid: '',
    passport: '',
    gender_id: '', 
  }];

  public EmployeeLanguagesInfoData:any [] = [{
    id: '',
    employee_id: '',
    name_of_language: '',
    name_of_language_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    expertise_level: '',
    certification_date: '', 
    certificate: '', 
  }];

  public EmployeeLocalTrainingsData:any [] = [{
    id: '',
    employee_id: '',
    course_title: '',
    course_title_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    location: '',
    location_bn: '', 
    from_date: '', 
    to_date: '', 
    certificate: '', 
  }];

  public EmployeeForeignTrainingsData:any [] = [{
    id: '',
    employee_id: '',
    course_title: '',
    course_title_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    country_id: '',
    from_date: '', 
    to_date: '', 
    certificate: '', 
  }];
  
  public EmployeeOfficialResidentialData = {
    id: '',
    employee_id: '',
    designation_id: '',
    memo_no: '', 
    memo_no_bn: '',
    memo_date: '',
    office_zone: '',
    location: '', 
    quarter_name: '', 
    flat_no_flat_type: '', 
  };

  public EmployeeForeignTravelsData:any [] = [{
    id: '',
    employee_id: '',
    country_id: '',
    purpose: '', 
    purpose_bn: '',
    from_date: '',
    to_date: '',
  }];

  public EmployeeAdditionalProfessionalQualificationsData:any [] = [{
    id: '',
    employee_id: '',
    qualification_name: '',
    qualification_name_bn: '',
    qualification_details: '',
    qualification_details_bn: '',
  }];

  public EmployeePublicationsData:any [] = [{
    id: '',
    employee_id: '',
    publication_type: '',
    publication_name: '',
    publication_name_bn: '',
    publication_details: '',
    publication_details_bn: '',
  }];

  public EmployeeHonorsAndAwardsData:any [] = [{
    id: '',
    employee_id: '',
    award_title: '',
    award_title_bn: '',
    award_details: '',
    award_details_bn: '',
    award_date: ''
  }];


  public EmployeeDisciplinaryActionsData:any [] = [{
    id: '',
    employee_id: '',
    disciplinary_action_category_id: '',
    disciplinary_action_details: '',
    disciplinary_action_details_bn: '',
    present_status: '',
    judgment: '',
    judgment_bn: '',
    final_judgment: '',
    final_judgment_bn: '',
    remarks: '',
    remarks_bn: '',
    date: ''
  }];

  public EmployeePromotionData = {
    id: '',
    employee_id: '', 
    memo_no: '', 
    memo_no_bn: '',
    memo_date: '',
    division_id: '',
    district_id: '',
    previous_post_id: '',
    current_position_id: '', 
  };

  public EmployeeReferenceData = {
    id: '',
    employee_id: '', 
    name: '', 
    name_bn: '',
    relation: '',
    relation_bn: '',
    contact_no: '',
    contact_no_bn: '',
    address: '', 
    address_bn: '',
  };

  Router: Router; 
  private _location: any;
  public submitted: boolean = false;
  public activeTab: any = 'createEmployee';
  public genderList: any = [];
  public maritalStatusList: any = [];
  public employeeID: any = '';
  public employeeInfo: any= [];
  public officeList: any= [];
  public designationList: any= [];
  public departmentList: any= [];
  public jobTypeList: any= [];
  public employeeClassList: any= [];
  public gradeList: any= [];
  public quotaList: any= [];
  public quotaDetailsList: any= [];
  public divisionList: any= [];
  public districtListByDivision: any= [];
  public upazilaListByDistrict: any= [];
  public countryList: any= [];
  public employeeAllInformationData: any;
  public id: any = false;
  public isViewMode: boolean = true;
  public isEmployeeDataViewMode:boolean = true;
  public isEmployeeJobDataViewMode:boolean = true;
  public isEmployeeQuotaViewMode:boolean = true;
  public isEmployeePresentAddressDataViewMode:boolean = true;
  public isEmployeePermanentAddressDataViewMode:boolean = true;
  public isEmployeeEducationQualificationDataViewMode:boolean = true;
  public isEmployeeSpouseDataViewMode:boolean = true;
  public isEmployeeChildrensInfoDataViewMode:boolean = true;
  public isEmployeeLanguagesInfoDataViewMode:boolean = true;
  public isEmployeeLocalTrainingsDataViewMode:boolean = true;
  public isEmployeeForeignTrainingsDataViewMode:boolean = true;
  public isEmployeeOfficialResidentialDataViewMode:boolean = true;
  public isEmployeeForeignTravelsDataViewMode:boolean = true;
  public isEmployeeAdditionalProfessionalQualificationsDataViewMode:boolean = true;
  public isEmployeePublicationsDataViewMode:boolean = true;
  public isEmployeeHonorsAndAwardsDataViewMode:boolean = true;
  public isEmployeeDisciplinaryActionsDataViewMode:boolean = true;
  public isEmployeePromotionDataViewMode:boolean = true;
  public isEmployeeReferenceDataViewMode:boolean = true;


  constructor(
    EmployeeService: EmployeeService, 
    private toasty: TostyService, 
    private location: Location, 
    private router: Router,
    private route: ActivatedRoute
    ) { 

    this.route.paramMap.subscribe((params) => {
      // console.log(params.get('id'));
    });

    this.Employee = EmployeeService;
    this.Toasty = toasty;
    this.Router = router;
    this._location = location; 
    this.Employee.getGenderList().then((res) => {
      this.genderList = res.data.data;
    });
    this.Employee.getMaritalStatusList().then((res) => {
      this.maritalStatusList = res.data.data;
    });
    this.Employee.getOfficeList().then((res) => {
      this.officeList = res.data.data;
    });

    this.Employee.getDesignationList().then((res) => {
      this.designationList = res.data.data;
    });

    this.Employee.getDepartmentList().then((res) => {
      this.departmentList = res.data.data;
    });

    this.Employee.getJobTypeList().then((res) => {
      this.jobTypeList = res.data.data;
    });

    this.Employee.getEmployeeClassList().then((res) => {
      this.employeeClassList = res.data.data;
    });

    this.Employee.getGradeList().then((res) => {
      this.gradeList = res.data.data;
    });

    this.Employee.getQuotaList().then((res) => {
      this.quotaList = res.data.data;
    });
    this.Employee.getQuotaDetailsList().then((res) => {
      this.quotaDetailsList = res.data.data;
    });
    this.Employee.getAllDivision().then((res) => {
      this.divisionList = res.data;
    });

    this.Employee.getAllDistrict().then((res) => {
      this.districtListByDivision = res.data;
    });

    this.Employee.getAllUpazila().then((res) => {
      this.upazilaListByDistrict = res.data;
    });

    this.Employee.getAllCountries().then((res) => {
      this.countryList = res.data.data;
    });
    


     
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.employeeID = this.id;
      this.EmployeeChildrensInfoData[0]['employee_id'] = this.employeeID;
      this.EmployeeLanguagesInfoData[0]['employee_id'] = this.employeeID
      this.EmployeeLocalTrainingsData[0]['employee_id'] = this.employeeID
      this.EmployeeForeignTrainingsData[0]['employee_id'] = this.employeeID;
      this.EmployeeForeignTravelsData[0]['employee_id'] = this.employeeID;
      this.EmployeeAdditionalProfessionalQualificationsData[0]['employee_id'] = this.employeeID;
      this.EmployeePublicationsData[0]['employee_id'] = this.employeeID;
      
      this.EmployeeHonorsAndAwardsData[0]['employee_id'] = this.employeeID;
      this.EmployeeDisciplinaryActionsData[0]['employee_id'] = this.employeeID;


      if(this.id){
        this.Employee.singleEmployee(this.id).then((res) => {

          this.employeeAllInformationData = res.data;
          this.EmployeeData = this.employeeAllInformationData;
          
          this.employeeAllInformationData['profile_id'] = res?.data?.profile_id;
          this.employeeAllInformationData['name'] = res?.data?.name;
          this.employeeAllInformationData['name_bn'] = res?.data?.name_bn;
          this.employeeAllInformationData['photo'] = res?.data?.photo;
          this.employeeAllInformationData['date_of_birth'] = res?.data?.date_of_birth;
          this.employeeAllInformationData['gender_id'] = res?.data?.gender_id;
          this.employeeAllInformationData['marital_status_id'] = res?.data?.marital_status_id;
          this.employeeAllInformationData['fathers_name'] = res?.data?.fathers_name;
          this.employeeAllInformationData['fathers_name_bn'] = res?.data?.fathers_name_bn;
          this.employeeAllInformationData['mothers_name'] = res?.data?.mothers_name;
          this.employeeAllInformationData['mothers_name_bn'] = res?.data?.mothers_name_bn 

          this.employeeAllInformationData['email'] = res?.data?.user?.email;
          this.employeeAllInformationData['phone_number'] = res?.data?.user?.phone_number;
          this.employeeAllInformationData['username'] = res?.data?.user?.username;
          this.EmployeeJobData = this.employeeAllInformationData['jobjoinings'][0];
          this.EmployeeQuota = this.employeeAllInformationData['employee_quotas'][0];
          this.EmployeePresentAddressData = this.employeeAllInformationData['present_addresses'][0];
          this.EmployeePermanentAddressData = this.employeeAllInformationData['permanent_addresses'][0];
          this.EmployeeEducationQualificationData = this.employeeAllInformationData['educational_qualifications'][0];
          this.EmployeeSpouseData = this.employeeAllInformationData['spouses'][0];
          this.EmployeeChildrensInfoData = this.employeeAllInformationData['childs'];
          this.EmployeeLanguagesInfoData = this.employeeAllInformationData['languages'];
          this.EmployeeLocalTrainingsData = this.employeeAllInformationData['local_trainings'];
          this.EmployeeForeignTrainingsData = this.employeeAllInformationData['foreigntrainings'];
          this.EmployeeOfficialResidentialData = this.employeeAllInformationData['official_residentials'][0];
          this.EmployeeForeignTravelsData = this.employeeAllInformationData['foreign_travels'];
          this.EmployeeAdditionalProfessionalQualificationsData = this.employeeAllInformationData['additional_qualifications'];
          this.EmployeePublicationsData = this.employeeAllInformationData['publications'];
          this.EmployeeHonorsAndAwardsData = this.employeeAllInformationData['honours_awards'];
          this.EmployeeDisciplinaryActionsData = this.employeeAllInformationData['disciplinary_actions'][0];
          this.EmployeePromotionData = this.employeeAllInformationData['promotions'][0];
          this.EmployeeReferenceData = this.employeeAllInformationData['references'][0];


        })
        .catch((res) => {
          if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
        });
      }

    });
  }
  


  updateEmployeeOnSubmit(EmployeeFrm: NgForm) {
    this.submitted = true;
    this.Employee.updateEmployee(this.EmployeeData['id'], this.EmployeeData).then((res) => {
      this.activeTab = 'jobJoiningInformation';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {
      if(res?.data?.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
      return;
    });
  }


  updateEmployeeJobInfoOnSubmit(EmployeeJobFrm: NgForm) {
    this.submitted = true;
    // if (EmployeeJobFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeJobInfo(this.EmployeeJobData['id'], this.EmployeeJobData).then((res) => {
      // this._location.back();
     console.log(res)
      this.activeTab = 'quotaInformation';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  updateEmployeeQuotaOnSubmit(EmployeeQuotaFrm: NgForm) {
    this.submitted = true;
    this.EmployeeQuota['employee_id'] = this.employeeID;
    // if (EmployeeQuotaFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeQuota(this.EmployeeQuota['id'], this.EmployeeQuota).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'presentAddress';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  updateEmployeePresentAddressOnSubmit(EmployeePresentAddressFrm: NgForm) {
    this.submitted = true;
    this.EmployeePresentAddressData['employee_id'] = this.employeeID;
    // if (EmployeePresentAddressFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeePresentAddress(this.EmployeePresentAddressData['id'], this.EmployeePresentAddressData).then((res) => {
      // this._location.back();
      //// console.log(res)
      this.activeTab = 'permanentAddress';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  updateEmployeePermanentAddressOnSubmit(EmployeePermanentAddressFrm: NgForm) {
    this.submitted = true;
    this.EmployeePermanentAddressData['employee_id'] = this.employeeID;
    // if (EmployeePermanentAddressFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeePermanentAddress(this.EmployeePermanentAddressData['id'], this.EmployeePermanentAddressData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'educationQuality';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  updateEmployeeEducationQualificationOnSubmit(EmployeeEducationQualificationFrm: NgForm) {
    this.submitted = true;
    this.EmployeeEducationQualificationData['employee_id'] = '1';
    // if (EmployeeEducationQualificationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeEducationQualification(this.EmployeeEducationQualificationData['id'], this.EmployeeEducationQualificationData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'spouse';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  updateEmployeeSpouseOnSubmit(EmployeeSpouseFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeSpouseData['employee_id'] = this.employeeID;
    // if (EmployeeSpouseFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeSpouse(this.EmployeeSpouseData['id'], this.EmployeeSpouseData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'childrenInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeChildrensInfoData() {
    
    this.EmployeeChildrensInfoData.push({
      employee_id: this.employeeID,
      name_of_children: '',
      name_of_children_bn: '', 
      birth_certificate: '',
      nid: '',
      passport: '',
      gender_id: '', 
    });
    
  }
  removeEmployeeChildrensInfoData(i: number) {
    this.EmployeeChildrensInfoData.splice(i, 1);
  }

  updateEmployeeChildrenInfoOnSubmit(EmployeeChildrenInfoFrm: NgForm) {
    this.submitted = true;
    // this.EmployeeChildrensInfoData['employee_id'] = '1';
    // if (EmployeeChildrenInfoFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    // console.log(this.EmployeeC['id'], this.EmployeeChildrensInfoData)
    // return
    this.Employee.updateEmployeeChildrenInfo(this.EmployeeChildrensInfoData['id'], this.EmployeeChildrensInfoData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'languageInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeLanguageInfoData() {
    this.EmployeeLanguagesInfoData.push({
      employee_id: this.employeeID,
      name_of_language: '',
      name_of_language_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      expertise_level: '',
      certification_date: '', 
      certificate: '', 
    });
  }
  removeEmployeeLanguageInfoData(i: number) {
    this.EmployeeLanguagesInfoData.splice(i, 1);
  }

  updateEmployeeLanguageInfoOnSubmit(EmployeeLanguageInfoFrm: NgForm) {
    this.submitted = true; 
    // this.EmployeeLanguageInfoData['employee_id'] = '1';
    // if (EmployeeLanguageInfoFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeLanguageInfo(this.EmployeeLanguagesInfoData['id'], this.EmployeeLanguagesInfoData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'localTrainig';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeLocalTrainingsData() {
    this.EmployeeLocalTrainingsData.push({
      employee_id: this.employeeID,
      course_title: '',
      course_title_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      location: '',
      location_bn: '', 
      from_date: '', 
      to_date: '', 
      certificate: '', 
    });
  }
  removeEmployeeLocalTrainingsData(i: number) {
    this.EmployeeLocalTrainingsData.splice(i, 1);
  }

  updateEmployeeLocalTrainingOnSubmit(EmployeeLocalTrainingFrm: NgForm) {
    this.submitted = true;
    // this.EmployeeLocalTrainingsData['employee_id'] = '1';
    // if (EmployeeLocalTrainingFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeLocalTraining(this.EmployeeLocalTrainingsData['id'], this.EmployeeLocalTrainingsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'foreignTraining';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeForeignTrainingsData() {
    this.EmployeeForeignTrainingsData.push({
      employee_id: this.employeeID,
      course_title: '',
      course_title_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      country_id: '',
      from_date: '', 
      to_date: '', 
      certificate: '',  
    });
  }
  removeEmployeeForeignTrainingsData(i: number) {
    this.EmployeeForeignTrainingsData.splice(i, 1);
  }

  updateEmployeeForeignTrainingOnSubmit(EmployeeForeignTrainingFrm: NgForm) {
    this.submitted = true;  
    // this.EmployeeForeignTrainingsData['employee_id'] = '1';
    // if (EmployeeForeignTrainingFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeForeignTraining(this.EmployeeForeignTrainingsData['id'], this.EmployeeForeignTrainingsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'officialResidentialInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  
  updateEmployeeOfficialResidentialOnSubmit(EmployeeOfficialResidentialFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeOfficialResidentialData['employee_id'] = this.employeeID;
    // if (EmployeeOfficialResidentialFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeOfficialResidential(this.EmployeeOfficialResidentialData['id'], this.EmployeeOfficialResidentialData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'foreignTravel';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeForeignTravelData() {
    this.EmployeeForeignTravelsData.push({
      employee_id: this.employeeID,
      country_id: '',
      purpose: '', 
      purpose_bn: '',
      from_date: '',
      to_date: '',
    });
  }
  removeEmployeeForeignTravelData(i: number) {
    this.EmployeeForeignTravelsData.splice(i, 1);
  }
  updateEmployeeForeignTravelOnSubmit(EmployeeForeignTravelFrm: NgForm) {
    this.submitted = true; 
    // console.log(this.EmployeeFo['id'], this.EmployeeForeignTravelsData)
    // this.EmployeeForeignTravelsData['employee_id'] = this.employeeID;
    // this.EmployeeForeignTravelsData['employee_id'] = '1';
    // if (EmployeeForeignTravelFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // } 
    this.Employee.updateEmployeeForeignTravel(this.EmployeeForeignTravelsData['id'], this.EmployeeForeignTravelsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'additionalProfessionalQualifications';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
 

  addEmployeeAdditionalProfessionalQualificationData() {
    this.EmployeeAdditionalProfessionalQualificationsData.push({
      employee_id: this.employeeID,
      qualification_name: '',
      qualification_name_bn: '',
      qualification_details: '',
      qualification_details_bn: '',
    });
  }
  removeEmployeeAdditionalProfessionalQualificationData(i: number) {
    this.EmployeeAdditionalProfessionalQualificationsData.splice(i, 1);
  }
  updateEmployeeAdditionalProfessionalQualificationOnSubmit(EmployeeAdditionalProfessionalQualificationFrm: NgForm) {
    this.submitted = true; 
    // this.EmployeeAdditionalProfessionalQualificationssData['employee_id'] = this.employeeID;
    // if (EmployeeAdditionalProfessionalQualificationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeAdditionalProfessionalQualification(this.EmployeeAdditionalProfessionalQualificationsData['id'], this.EmployeeAdditionalProfessionalQualificationsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'publications';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
  
  addEmployeePublicationData() {
    this.EmployeePublicationsData.push({
      employee_id: this.employeeID,
      publication_type: '',
      publication_name: '',
      publication_name_bn: '',
      publication_details: '',
      publication_details_bn: ''
    });
  }
  removeEmployeePublicationData(i: number) {
    this.EmployeePublicationsData.splice(i, 1);
  }

  updateEmployeePublicationOnSubmit(EmployeePublicationFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeePublicationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeePublication(this.EmployeePublicationsData['id'], this.EmployeePublicationsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'honoursAndAward';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeHonorsAndAwardsData() {
    this.EmployeeHonorsAndAwardsData.push({
      employee_id: this.employeeID,
      award_title: '',
      award_title_bn: '',
      award_details: '',
      award_details_bn: '',
      award_date: ''
    });
  }
  removeEmployeeHonorsAndAwardsData(i: number) {
    this.EmployeeHonorsAndAwardsData.splice(i, 1);
  }

  updateEmployeeHonorsAndAwardOnSubmit(EmployeeHonorsAndAwardFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeeHonorsAndAwardFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeHonorsAndAwards(this.EmployeeHonorsAndAwardsData['id'], this.EmployeeHonorsAndAwardsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'desciplinaryAction';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeDisciplinaryActionsData() {
    this.EmployeeDisciplinaryActionsData.push({
      employee_id: this.employeeID,
      disciplinary_action_category_id: '',
      disciplinary_action_details: '',
      disciplinary_action_details_bn: '',
      present_status: '',
      judgment: '',
      judgment_bn: '',
      final_judgment: '',
      final_judgment_bn: '',
      remarks: '',
      remarks_bn: '',
      date: ''
    });
  }
  removeEmployeeDisciplinaryActionsData(i: number) {
    this.EmployeeDisciplinaryActionsData.splice(i, 1);
  }

  updateEmployeeDisciplinaryActionOnSubmit(EmployeeDisciplinaryActionFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeeDisciplinaryActionFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeDisciplinaryActions(this.EmployeeDisciplinaryActionsData['id'], this.EmployeeDisciplinaryActionsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'promotion';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  updateEmployeePromotionOnSubmit(EmployeePromotionFrm: NgForm) {
    this.submitted = true; 
    this.EmployeePromotionData['employee_id'] = this.employeeID;
    // if (EmployeePromotionFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeePromotion(this.EmployeePromotionData['id'], this.EmployeePromotionData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'reference';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  updateEmployeeReferenceOnSubmit(EmployeeReferenceFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeReferenceData['employee_id'] = this.employeeID;
    // if (EmployeeReferenceFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.updateEmployeeReference(this.EmployeeReferenceData['id'], this.EmployeeReferenceData).then((res) => {
      // this._location.back();
     // console.log(res)
     this.Router.navigateByUrl("/profile-information/employee");
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {
      if(res.data.errors){
        res.data.errors.forEach((_error: any) => {
          this.Toasty.showError(_error.message[0], 'Sorry!',);
        });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  districtByDivision(event){ 
    this.Employee.getDistrictByDivision(event.target.value).then((res) => {
      this.districtListByDivision = res.data.districts;
    });
  }

  upazilaByDistrict(event){ 
    this.Employee.getUpazilaByDistrict(event.target.value).then((res) => {
      this.upazilaListByDistrict = res.data.upazilas;
    });
  }

  

  isEditMode($id:any){
    this.isViewMode = $id;
  }

  employeeDataEditMode() {
    this.isEmployeeDataViewMode = !this.isEmployeeDataViewMode;
  }
  employeeJobDataEditMode() {
    this.isEmployeeJobDataViewMode = !this.isEmployeeJobDataViewMode;
  }
  isEmployeeQuotaEditMode() {
    this.isEmployeeQuotaViewMode = !this.isEmployeeQuotaViewMode;
  }
  employeePresentAddressDataEditMode() {
    this.isEmployeePresentAddressDataViewMode = !this.isEmployeePresentAddressDataViewMode;
  }
  employeePermanentAddressDataEditMode() {
    this.isEmployeePermanentAddressDataViewMode = !this.isEmployeePermanentAddressDataViewMode;
  }
  employeeEducationQualificationDataEditMode() {
    this.isEmployeeEducationQualificationDataViewMode = !this.isEmployeeEducationQualificationDataViewMode;
  }
  employeeSpouseDataEditMode() {
    this.isEmployeeSpouseDataViewMode = !this.isEmployeeSpouseDataViewMode;
  }
  employeeChildrensInfoDataEditMode() {
    this.isEmployeeChildrensInfoDataViewMode = !this.isEmployeeChildrensInfoDataViewMode;
  }
  employeeLanguagesInfoDataEditMode() {
    this.isEmployeeLanguagesInfoDataViewMode = !this.isEmployeeLanguagesInfoDataViewMode;
  }
  employeeLocalTrainingsDataEditMode() {
    this.isEmployeeLocalTrainingsDataViewMode = !this.isEmployeeLocalTrainingsDataViewMode;
  }
  employeeForeignTrainingsDataEditMode() {
    this.isEmployeeForeignTrainingsDataViewMode = !this.isEmployeeForeignTrainingsDataViewMode;
  }
  employeeOfficialResidentialDataEditMode() {
    this.isEmployeeOfficialResidentialDataViewMode = !this.isEmployeeOfficialResidentialDataViewMode;
  }
  employeeForeignTravelsDataEditMode() {
    this.isEmployeeForeignTravelsDataViewMode = !this.isEmployeeForeignTravelsDataViewMode;
  }
  employeeAdditionalProfessionalQualificationsDataEditMode() {
    this.isEmployeeAdditionalProfessionalQualificationsDataViewMode = !this.isEmployeeAdditionalProfessionalQualificationsDataViewMode;
  }
  employeePublicationsDataEditMode() {
    this.isEmployeePublicationsDataViewMode = !this.isEmployeePublicationsDataViewMode;
  }
  employeeHonorsAndAwardsDataEditMode() {
    this.isEmployeeHonorsAndAwardsDataViewMode = !this.isEmployeeHonorsAndAwardsDataViewMode;
  }
  employeeDisciplinaryActionsDataEditMode() {
    this.isEmployeeDisciplinaryActionsDataViewMode = !this.isEmployeeDisciplinaryActionsDataViewMode;
  }
  employeePromotionDataEditMode() {
    this.isEmployeePromotionDataViewMode = !this.isEmployeePromotionDataViewMode;
  }
  employeeReferenceDataEditMode() {
    this.isEmployeeReferenceDataViewMode = !this.isEmployeeReferenceDataViewMode;
  }
  
  changeTab($id:any){
    this.activeTab = $id;
  }
  

}
