import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { EmployeeService } from '../services/employee.service';
import { TranslateService } from '../../../../shared/services/translate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  items: any = []; 
  public pagination: any = [];
  lang: any = 'en';

  constructor(private employeeService: EmployeeService,  private toasty: TostyService, private translateService: TranslateService) { 

    this.translateService.translat$.subscribe((data) => {
        this.lang = data;
      }
    );

    this.toasty = toasty;
    // this.employeeService.getList().then((res) => {
    //   this.items = res.data.data;
    //   // console.log(this.items)
    // });
  }

  
  ngOnInit(): void {
    this.getPageDataFn();
  }
  getPageDataFn(page: number = 1) {
    this.employeeService.getList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
    });
  }

  deleteEmployee( id ) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor:'#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(id).then((res) => {
          this.employeeService.getList().then((res) => {
            this.items = res.data; 
          }); 
          Swal.fire({
            icon: 'success',
            title: 'Deleted!',
            text: 'Your imaginary file has been deleted.',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 1000,
          })
        });
      }
    })
    
    
  }

}
