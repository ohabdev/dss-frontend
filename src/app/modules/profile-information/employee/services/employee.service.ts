import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  

  

  private alias: any = null;
  private Employee: any = null;
  private getEmployee: any;
  constructor(private restangular: Restangular) { }


  


  getList(page:number=1) {
    return this.restangular.all('auth').customGET('employee?page='+page).toPromise();
  }

  getGenderList() {
    return this.restangular.all('auth').customGET('gender/list').toPromise();
  }

  getMaritalStatusList() {
    return this.restangular.all('auth').customGET('marital-status/list').toPromise();
  }

  getOfficeList() {
    return this.restangular.all('auth').customGET('office/list').toPromise();
  }

  getDesignationList() {
    return this.restangular.all('auth').customGET('designation/list').toPromise();
  }

  getDepartmentList() {
    return this.restangular.all('auth').customGET('department/list').toPromise();
  }

  getJobTypeList() {
    return this.restangular.all('auth').customGET('job-type/list').toPromise();
  }

  getEmployeeClassList() {
    return this.restangular.all('auth').customGET('employee-class/list').toPromise();
  }

  getGradeList() {
    return this.restangular.all('auth').customGET('salary-grade/list').toPromise();
  }

  getQuotaList() {
    return this.restangular.all('auth').customGET('quota-information/list').toPromise();
  }

  getQuotaDetailsList() {
    return this.restangular.all('auth').customGET('quota-information-details/list').toPromise();
  }
  
  getAllDivision() {
    return this.restangular.all('auth').customGET('division/list').toPromise();
  }
  getAllDistrict() {
    return this.restangular.all('auth').customGET('district/list').toPromise();
  }
  getDistrictByDivision($id): Promise<any> {
    return this.restangular.all('auth/division').customGET($id).toPromise();
  }
  getAllUpazila() {
    return this.restangular.all('auth').customGET('upazila/list').toPromise();
  }
  getUpazilaByDistrict($id): Promise<any> {
    return this.restangular.all('auth/district').customGET($id).toPromise();
  }
  getAllCountries(): Promise<any> {
    return this.restangular.all('auth').customGET('country/list').toPromise();
  }

  getAllDisciplinaryActionCategory(): Promise<any> {
    return this.restangular.all('auth').customGET('disciplinary-action-category/list').toPromise();
  }





  
  createEmployee(data: any): Promise<any> {
    return this.restangular.all('auth/employee').post(data).toPromise();
  }

  singleEmployee($id): Promise<any> {
    return this.restangular.all('auth/employee').customGET($id).toPromise();
  }

  updateEmployee($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/employee',$id).customPUT(data).toPromise();
  }

  deleteEmployee($id): Promise<any> {
    return this.restangular.one('auth/employee/').customDELETE($id).toPromise();
  }


  
  createEmployeeJobInfo(data: any): Promise<any> {
    return this.restangular.all('auth/job/joining/information').post(data).toPromise();
  }
  updateEmployeeJobInfo($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/job/joining/information',$id).customPUT(data).toPromise();
  }

  createEmployeeQuota(data: any): Promise<any> {
    return this.restangular.all('auth/employee-quota').post(data).toPromise();
  }
  updateEmployeeQuota($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/employee-quota',$id).customPUT(data).toPromise();
  }

  createEmployeePresentAddress(data: any): Promise<any> {
    return this.restangular.all('auth/present/address').post(data).toPromise();
  }
  updateEmployeePresentAddress($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/present/address',$id).customPUT(data).toPromise();
  }

  createEmployeePermanentAddress(data: any): Promise<any> {
    return this.restangular.all('auth/permanent/address').post(data).toPromise();
  }
  updateEmployeePermanentAddress($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/permanent/address',$id).customPUT(data).toPromise();
  }

  createEmployeeEducationQualification(data: any): Promise<any> {
    return this.restangular.all('auth/education-qualification').post(data).toPromise();
  }
  updateEmployeeEducationQualification($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/education-qualification',$id).customPUT(data).toPromise();
  }

  createEmployeeSpouse(data: any): Promise<any> {
    return this.restangular.all('auth/spouse').post(data).toPromise();
  }
  updateEmployeeSpouse($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/spouse',$id).customPUT(data).toPromise();
  }

  createEmployeeChildrenInfo(data: any): Promise<any> {
    return this.restangular.all('auth/child').post(data).toPromise();
  }
  updateEmployeeChildrenInfo($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/child',$id).customPUT(data).toPromise();
  }

  createEmployeeLanguageInfo(data: any): Promise<any> {
    return this.restangular.all('auth/language').post(data).toPromise();
  }
  updateEmployeeLanguageInfo($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/language',$id).customPUT(data).toPromise();
  }

  createEmployeeLocalTraining(data: any): Promise<any> {
    return this.restangular.all('auth/local-training').post(data).toPromise();
  }
  updateEmployeeLocalTraining($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/local-training',$id).customPUT(data).toPromise();
  }

  createEmployeeForeignTraining(data: any): Promise<any> {
    return this.restangular.all('auth/foreign-training').post(data).toPromise();
  }
  updateEmployeeForeignTraining($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/foreign-training',$id).customPUT(data).toPromise();
  }

  createEmployeeOfficialResidential(data: any): Promise<any> {
    return this.restangular.all('auth/official-residential').post(data).toPromise();
  }
  updateEmployeeOfficialResidential($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/official-residential',$id).customPUT(data).toPromise();
  }

  createEmployeeForeignTravel(data: any): Promise<any> {
    return this.restangular.all('auth/foreign-travel').post(data).toPromise();
  }
  updateEmployeeForeignTravel($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/foreign-travel',$id).customPUT(data).toPromise();
  }

  createEmployeeAdditionalProfessionalQualification(data: any): Promise<any> {
    return this.restangular.all('auth/additional-qualification').post(data).toPromise();
  }
  updateEmployeeAdditionalProfessionalQualification($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/additional-qualification',$id).customPUT(data).toPromise();
  }

  createEmployeePublication(data: any): Promise<any> {
    return this.restangular.all('auth/publication').post(data).toPromise();
  }
  updateEmployeePublication($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/publication',$id).customPUT(data).toPromise();
  }
  createEmployeeHonorsAndAwards(data: any): Promise<any> {
    return this.restangular.all('auth/honours-award').post(data).toPromise();
  }
  updateEmployeeHonorsAndAwards($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/honours-award',$id).customPUT(data).toPromise();
  }
  createEmployeeDisciplinaryActions(data: any): Promise<any> {
    return this.restangular.all('auth/disciplinary-action').post(data).toPromise();
  }
  updateEmployeeDisciplinaryActions($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/disciplinary-action',$id).customPUT(data).toPromise();
  }
  createEmployeePromotion(data: any): Promise<any> {
    return this.restangular.all('auth/promotion').post(data).toPromise();
  }
  updateEmployeePromotion($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/promotion',$id).customPUT(data).toPromise();
  }
  createEmployeeReference(data: any): Promise<any> {
    return this.restangular.all('auth/reference').post(data).toPromise();
  }
  updateEmployeeReference($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/reference',$id).customPUT(data).toPromise();
  }




}
