import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/shared/formate/formate-datepicker';
import { Router } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { EmployeeService } from '../services/employee.service'; 


@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class CreateEmployeeComponent implements OnInit {

  private Employee: EmployeeService;
  private Toasty: TostyService;
  // public EmployeeData = {
  //   profile_id: '001',
  //   name: 'Riad',
  //   name_bn: 'riad', 
  //   photo: '',
  //   date_of_birth: '1990-01-02',
  //   gender_id: '1',
  //   marital_status: '1',
  //   fathers_name: 'MD',
  //   fathers_name_bn: 'md',
  //   mothers_name: 'MRS',
  //   mothers_name_bn: 'mrs',
  //   email: 'emai001@test.com',
  //   username: 'use001',
  //   phone_number: '01610676744',
  //   password: '12345678',
  //   password_confirmation: '12345678'
  // }; 
  public EmployeeData = {
    profile_id: '',
    name: '',
    name_bn: '', 
    photo: '',
    date_of_birth: '',
    gender_id: '',
    marital_status_id: '',
    fathers_name: '',
    fathers_name_bn: '',
    mothers_name: '',
    mothers_name_bn: '',
    email: '',
    username: '',
    phone_number: '', 
  }; 
  public EmployeeJobData = {
    employee_id: '',
    office_id: '',
    designation_id: '', 
    department_id: '',
    job_type_id: '',
    employee_class_id: '',
    grade_id: '',
    joining_date: '',
    pension_date: '',
    prl_date: ''
  };
  
  public EmployeeQuota = {
    employee_id: '',
    quota_information_id: '',
    quota_information_detail_id: '', 
    description: '',
    description_bn: '',
  };

  public EmployeePresentAddressData = {
    employee_id: '',
    division_id: '',
    district_id: '', 
    upazila_id: '',
    police_station: '',
    police_station_bn: '',
    post_office: '',
    post_office_bn: '',
    road_word_no: '', 
    road_word_no_bn: '',
    village_house_no: '',
    village_house_no_bn: '',
  };

  public EmployeePermanentAddressData = {
    employee_id: '',
    division_id: '',
    district_id: '', 
    upazila_id: '',
    police_station: '',
    police_station_bn: '',
    post_office: '',
    post_office_bn: '',
    road_word_no: '', 
    road_word_no_bn: '',
    village_house_no: '',
    village_house_no_bn: '',
  };


  public EmployeeEducationQualificationData = {
    employee_id: '',
    name_of_degree: '',
    name_of_degree_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    police_station_bn: '',
    board_university: '',
    board_university_bn: '',
    passing_year: '', 
    division_cgpa: '', 
  };

  public EmployeeSpouseData = {
    employee_id: '',
    name: '',
    name_bn: '', 
    relation: '',
    division_id: '',
    district_id: '',
    occupation: '',
    phone_no: '',
    mobile_no: '', 
  };

  public EmployeeChildrensInfoData:any [] = [{
    employee_id: '',
    name_of_children: '',
    name_of_children_bn: '', 
    birth_certificate: '',
    nid: '',
    passport: '',
    gender_id: '', 
  }];

  public EmployeeLanguagesInfoData:any [] = [{
    employee_id: '',
    name_of_language: '',
    name_of_language_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    expertise_level: '',
    certification_date: '', 
    certificate: '', 
  }];

  public EmployeeLocalTrainingsData:any [] = [{
    employee_id: '',
    course_title: '',
    course_title_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    location: '',
    location_bn: '', 
    from_date: '', 
    to_date: '', 
    certificate: '', 
  }];

  public EmployeeForeignTrainingsData:any [] = [{
    employee_id: '',
    course_title: '',
    course_title_bn: '', 
    name_of_institute: '',
    name_of_institute_bn: '',
    country_id: '',
    from_date: '', 
    to_date: '', 
    certificate: '', 
  }];
  
  public EmployeeOfficialResidentialData = {
    employee_id: '',
    designation_id: '',
    memo_no: '', 
    memo_no_bn: '',
    memo_date: '',
    office_zone: '',
    location: '', 
    quarter_name: '', 
    flat_no_flat_type: '', 
  };

  public EmployeeForeignTravelsData:any [] = [{
    employee_id: '',
    country_id: '',
    purpose: '', 
    purpose_bn: '',
    from_date: '',
    to_date: '',
  }];

  public EmployeeAdditionalProfessionalQualificationsData:any [] = [{
    employee_id: '',
    qualification_name: '',
    qualification_name_bn: '',
    qualification_details: '',
    qualification_details_bn: '',
  }];

  public EmployeePublicationsData:any [] = [{
    employee_id: '',
    publication_type: '',
    publication_name: '',
    publication_name_bn: '',
    publication_details: '',
    publication_details_bn: '',
  }];

  public EmployeeHonorsAndAwardsData:any [] = [{
    employee_id: '',
    award_title: '',
    award_title_bn: '',
    award_details: '',
    award_details_bn: '',
    award_date: ''
  }];


  public EmployeeDisciplinaryActionsData:any [] = [{
    employee_id: '',
    disciplinary_action_category_id: '',
    disciplinary_action_details: '',
    disciplinary_action_details_bn: '',
    present_status: '',
    judgment: '',
    judgment_bn: '',
    final_judgment: '',
    final_judgment_bn: '',
    remarks: '',
    remarks_bn: '',
    date: ''
  }];

  public EmployeePromotionData = {
    employee_id: '', 
    memo_no: '', 
    memo_no_bn: '',
    memo_date: '',
    division_id: '',
    district_id: '',
    previous_post_id: '',
    current_position_id: '', 
  };

  public EmployeeReferenceData = {
    employee_id: '', 
    name: '', 
    name_bn: '',
    relation: '',
    relation_bn: '',
    contact_no: '',
    contact_no_bn: '',
    address: '', 
    address_bn: '',
  };

  Router: Router; 
  private _location: any;
  public submitted: boolean = false;
  // public activeTab: any = 'createEmployee';
  public activeTab: any = 'createEmployee';
  public genderList: any = [];
  public maritalStatusList: any = [];
  public employeeID: any = '';
  public employeeInfo: any= [];
  public officeList: any= [];
  public designationList: any= [];
  public departmentList: any= [];
  public jobTypeList: any= [];
  public employeeClassList: any= [];
  public gradeList: any= [];
  public quotaList: any= [];
  public quotaDetailsList: any= [];
  public divisionList: any= [];
  public districtListByDivision: any= [];
  public upazilaListByDistrict: any= [];
  public countryList: any= [];
  public disciplinaryActionCategoryList: any = [];
  

  constructor(EmployeeService: EmployeeService, private toasty: TostyService, private location: Location, private router: Router) { 
    this.Employee = EmployeeService;
    this.Toasty = toasty;
    this.Router = router;
    this._location = location; 
    this.Employee.getGenderList().then((res) => {
      this.genderList = res.data;
    });
    this.Employee.getMaritalStatusList().then((res) => {
      this.maritalStatusList = res.data;
    });
    this.Employee.getOfficeList().then((res) => {
      this.officeList = res.data;
    });

    this.Employee.getDesignationList().then((res) => {
      this.designationList = res.data;
    });

    this.Employee.getDepartmentList().then((res) => {
      this.departmentList = res.data;
    });

    this.Employee.getJobTypeList().then((res) => {
      this.jobTypeList = res.data;
    });

    this.Employee.getEmployeeClassList().then((res) => {
      this.employeeClassList = res.data;
    });

    this.Employee.getGradeList().then((res) => {
      this.gradeList = res.data;
    });

    this.Employee.getQuotaList().then((res) => {
      this.quotaList = res.data;
    });
    this.Employee.getQuotaDetailsList().then((res) => {
      this.quotaDetailsList = res.data;
    });
    this.Employee.getAllDivision().then((res) => {
      this.divisionList = res.data;
    });

    this.Employee.getAllCountries().then((res) => {
      this.countryList = res.data;
    });

    this.Employee.getAllDisciplinaryActionCategory().then((res) => {
      this.disciplinaryActionCategoryList = res.data;
    }); 


     
  }
  ngOnInit(): void {}


  createEmployeeOnSubmit(EmployeeFrm: NgForm) {
    this.submitted = true;
    // if (EmployeeFrm.invalid) { 
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }

    console.log(EmployeeFrm.form.value.date_of_birth)
    return;
    this.Employee.createEmployee(this.EmployeeData).then((res) => {
      // this._location.back();
     console.log(res)
      this.employeeInfo = res.data;
      this.employeeID = this.employeeInfo.employee_id;
      // this.employeeID = 7;
      this.EmployeeChildrensInfoData[0]['employee_id'] = this.employeeID;
      this.EmployeeLanguagesInfoData[0]['employee_id'] = this.employeeID
      this.EmployeeLocalTrainingsData[0]['employee_id'] = this.employeeID
      this.EmployeeForeignTrainingsData[0]['employee_id'] = this.employeeID;
      this.EmployeeForeignTravelsData[0]['employee_id'] = this.employeeID;
      this.EmployeeAdditionalProfessionalQualificationsData[0]['employee_id'] = this.employeeID;
      this.EmployeePublicationsData[0]['employee_id'] = this.employeeID;
      
      this.EmployeeHonorsAndAwardsData[0]['employee_id'] = this.employeeID;
      this.EmployeeDisciplinaryActionsData[0]['employee_id'] = this.employeeID;
      
      this.activeTab = 'jobJoiningInformation';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{ 
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  createEmployeeJobInfoOnSubmit(EmployeeJobFrm: NgForm) {
    this.submitted = true;
    this.EmployeeJobData['employee_id'] = this.employeeID;
    // if (EmployeeJobFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeJobInfo(this.EmployeeJobData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'quotaInformation';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  createEmployeeQuotaOnSubmit(EmployeeQuotaFrm: NgForm) {
    this.submitted = true;
    this.EmployeeQuota['employee_id'] = this.employeeID;
    // if (EmployeeQuotaFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeQuota(this.EmployeeQuota).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'presentAddress';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  createEmployeePresentAddressOnSubmit(EmployeePresentAddressFrm: NgForm) {
    this.submitted = true;
    this.EmployeePresentAddressData['employee_id'] = this.employeeID;
    // if (EmployeePresentAddressFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeePresentAddress(this.EmployeePresentAddressData).then((res) => {
      // this._location.back();
      //// console.log(res)
      this.activeTab = 'permanentAddress';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  createEmployeePermanentAddressOnSubmit(EmployeePermanentAddressFrm: NgForm) {
    this.submitted = true;
    this.EmployeePermanentAddressData['employee_id'] = this.employeeID;
    // if (EmployeePermanentAddressFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeePermanentAddress(this.EmployeePermanentAddressData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'educationQuality';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  createEmployeeEducationQualificationOnSubmit(EmployeeEducationQualificationFrm: NgForm) {
    this.submitted = true;
    this.EmployeeEducationQualificationData['employee_id'] = '1';
    // if (EmployeeEducationQualificationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeEducationQualification(this.EmployeeEducationQualificationData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'spouse';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  createEmployeeSpouseOnSubmit(EmployeeSpouseFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeSpouseData['employee_id'] = this.employeeID;
    // if (EmployeeSpouseFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeSpouse(this.EmployeeSpouseData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'childrenInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeChildrensInfoData() {
    
    this.EmployeeChildrensInfoData.push({
      employee_id: this.employeeID,
      name_of_children: '',
      name_of_children_bn: '', 
      birth_certificate: '',
      nid: '',
      passport: '',
      gender_id: '', 
    });
    console.log(this.EmployeeChildrensInfoData);
  }
  removeEmployeeChildrensInfoData(i: number) {
    this.EmployeeChildrensInfoData.splice(i, 1);
  }

  createEmployeeChildrenInfoOnSubmit(EmployeeChildrenInfoFrm: NgForm) {
    this.submitted = true;
    // this.EmployeeChildrensInfoData['employee_id'] = '1';
    // if (EmployeeChildrenInfoFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    // console.log(this.EmployeeChildrensInfoData)
    // return
    this.Employee.createEmployeeChildrenInfo(this.EmployeeChildrensInfoData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'languageInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeLanguageInfoData() {
    this.EmployeeLanguagesInfoData.push({
      employee_id: this.employeeID,
      name_of_language: '',
      name_of_language_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      expertise_level: '',
      certification_date: '', 
      certificate: '', 
    });
  }
  removeEmployeeLanguageInfoData(i: number) {
    this.EmployeeLanguagesInfoData.splice(i, 1);
  }

  createEmployeeLanguageInfoOnSubmit(EmployeeLanguageInfoFrm: NgForm) {
    this.submitted = true; 
    // this.EmployeeLanguageInfoData['employee_id'] = '1';
    // if (EmployeeLanguageInfoFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeLanguageInfo(this.EmployeeLanguagesInfoData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'localTrainig';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeLocalTrainingsData() {
    this.EmployeeLocalTrainingsData.push({
      employee_id: this.employeeID,
      course_title: '',
      course_title_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      location: '',
      location_bn: '', 
      from_date: '', 
      to_date: '', 
      certificate: '', 
    });
  }
  removeEmployeeLocalTrainingsData(i: number) {
    this.EmployeeLocalTrainingsData.splice(i, 1);
  }

  createEmployeeLocalTrainingOnSubmit(EmployeeLocalTrainingFrm: NgForm) {
    this.submitted = true;
    // this.EmployeeLocalTrainingsData['employee_id'] = '1';
    // if (EmployeeLocalTrainingFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeLocalTraining(this.EmployeeLocalTrainingsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'foreignTraining';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  addEmployeeForeignTrainingsData() {
    this.EmployeeForeignTrainingsData.push({
      employee_id: this.employeeID,
      course_title: '',
      course_title_bn: '', 
      name_of_institute: '',
      name_of_institute_bn: '',
      country_id: '',
      from_date: '', 
      to_date: '', 
      certificate: '',  
    });
  }
  removeEmployeeForeignTrainingsData(i: number) {
    this.EmployeeForeignTrainingsData.splice(i, 1);
  }

  createEmployeeForeignTrainingOnSubmit(EmployeeForeignTrainingFrm: NgForm) {
    this.submitted = true;  
    // this.EmployeeForeignTrainingsData['employee_id'] = '1';
    // if (EmployeeForeignTrainingFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeForeignTraining(this.EmployeeForeignTrainingsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'officialResidentialInfo';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  
  createEmployeeOfficialResidentialOnSubmit(EmployeeOfficialResidentialFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeOfficialResidentialData['employee_id'] = this.employeeID;
    // if (EmployeeOfficialResidentialFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeOfficialResidential(this.EmployeeOfficialResidentialData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'foreignTravel';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeForeignTravelData() {
    this.EmployeeForeignTravelsData.push({
      employee_id: this.employeeID,
      country_id: '',
      purpose: '', 
      purpose_bn: '',
      from_date: '',
      to_date: '',
    });
  }
  removeEmployeeForeignTravelData(i: number) {
    this.EmployeeForeignTravelsData.splice(i, 1);
  }
  createEmployeeForeignTravelOnSubmit(EmployeeForeignTravelFrm: NgForm) {
    this.submitted = true; 
    // console.log(this.EmployeeForeignTravelsData)
    // this.EmployeeForeignTravelsData['employee_id'] = this.employeeID;
    // this.EmployeeForeignTravelsData['employee_id'] = '1';
    // if (EmployeeForeignTravelFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // } 
    this.Employee.createEmployeeForeignTravel(this.EmployeeForeignTravelsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'additionalProfessionalQualifications';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
 

  addEmployeeAdditionalProfessionalQualificationData() {
    this.EmployeeAdditionalProfessionalQualificationsData.push({
      employee_id: this.employeeID,
      qualification_name: '',
      qualification_name_bn: '',
      qualification_details: '',
      qualification_details_bn: '',
    });
  }
  removeEmployeeAdditionalProfessionalQualificationData(i: number) {
    this.EmployeeAdditionalProfessionalQualificationsData.splice(i, 1);
  }
  createEmployeeAdditionalProfessionalQualificationOnSubmit(EmployeeAdditionalProfessionalQualificationFrm: NgForm) {
    this.submitted = true; 
    // this.EmployeeAdditionalProfessionalQualificationssData['employee_id'] = this.employeeID;
    // if (EmployeeAdditionalProfessionalQualificationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeAdditionalProfessionalQualification(this.EmployeeAdditionalProfessionalQualificationsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'publications';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }
  
  addEmployeePublicationData() {
    this.EmployeePublicationsData.push({
      employee_id: this.employeeID,
      publication_type: '',
      publication_name: '',
      publication_name_bn: '',
      publication_details: '',
      publication_details_bn: ''
    });
  }
  removeEmployeePublicationData(i: number) {
    this.EmployeePublicationsData.splice(i, 1);
  }

  createEmployeePublicationOnSubmit(EmployeePublicationFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeePublicationFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeePublication(this.EmployeePublicationsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'honoursAndAward';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeHonorsAndAwardsData() {
    this.EmployeeHonorsAndAwardsData.push({
      employee_id: this.employeeID,
      award_title: '',
      award_title_bn: '',
      award_details: '',
      award_details_bn: '',
      award_date: ''
    });
  }
  removeEmployeeHonorsAndAwardsData(i: number) {
    this.EmployeeHonorsAndAwardsData.splice(i, 1);
  }

  createEmployeeHonorsAndAwardOnSubmit(EmployeeHonorsAndAwardFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeeHonorsAndAwardFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeHonorsAndAwards(this.EmployeeHonorsAndAwardsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'desciplinaryAction';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }


  addEmployeeDisciplinaryActionsData() {
    this.EmployeeDisciplinaryActionsData.push({
      employee_id: this.employeeID,
      disciplinary_action_category_id: '',
      disciplinary_action_details: '',
      disciplinary_action_details_bn: '',
      present_status: '',
      judgment: '',
      judgment_bn: '',
      final_judgment: '',
      final_judgment_bn: '',
      remarks: '',
      remarks_bn: '',
      date: ''
    });
  }
  removeEmployeeDisciplinaryActionsData(i: number) {
    this.EmployeeDisciplinaryActionsData.splice(i, 1);
  }

  createEmployeeDisciplinaryActionOnSubmit(EmployeeDisciplinaryActionFrm: NgForm) {
    this.submitted = true;  
    // if (EmployeeDisciplinaryActionFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeDisciplinaryActions(this.EmployeeDisciplinaryActionsData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'promotion';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  createEmployeePromotionOnSubmit(EmployeePromotionFrm: NgForm) {
    this.submitted = true; 
    this.EmployeePromotionData['employee_id'] = this.employeeID;
    // if (EmployeePromotionFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeePromotion(this.EmployeePromotionData).then((res) => {
      // this._location.back();
     // console.log(res)
      this.activeTab = 'reference';
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  createEmployeeReferenceOnSubmit(EmployeeReferenceFrm: NgForm) {
    this.submitted = true; 
    this.EmployeeReferenceData['employee_id'] = this.employeeID;
    // if (EmployeeReferenceFrm.invalid) {
    //   return this.Toasty.showError('Your Input is Invalid!', 'Error!',);
    // }
    this.Employee.createEmployeeReference(this.EmployeeReferenceData).then((res) => {
      // this._location.back();
     // console.log(res)
     this.Router.navigateByUrl("/profile-information/employee");
      this.Toasty.showSuccess(res.message, 'Success',);
    })
    .catch((res) => {  
      if(res.data.errors){
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      }else{
        this.Toasty.showError(res.message, 'Sorry!',);
      }
    });
  }

  districtByDivision(event){ 
    this.Employee.getDistrictByDivision(event.target.value).then((res) => {
      this.districtListByDivision = res.data.districts;
    });
  }

  upazilaByDistrict(event){ 
    this.Employee.getUpazilaByDistrict(event.target.value).then((res) => {
      this.upazilaListByDistrict = res.data.upazilas;
    });
  }

  
   
  
  changeTab($id:any){
    this.activeTab = $id;
  }
  

  
}
