import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { SingleEmployeeComponent } from './single-employee/single-employee.component'; 
import { MaterialModule } from 'src/app/material.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list'
  },
  {
    path: 'list',
    component: EmployeeListComponent,
    data: {title: 'Employee Information List'} 
  },
  {
    path: 'create',
    component: CreateEmployeeComponent,
    data: {title: 'Create Employee Information'} 
  },
  {
    path: 'edit/:id',
    component: EditEmployeeComponent,
    data: {title: 'Edit Employee Information'} 
  },
  {
    path: ':id',
    component: SingleEmployeeComponent,
    data: {title: 'Single Employee Information'} 
  }
];


@NgModule({
  declarations: [
    EmployeeListComponent, 
    CreateEmployeeComponent, 
    EditEmployeeComponent, SingleEmployeeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class EmployeesModule { }
