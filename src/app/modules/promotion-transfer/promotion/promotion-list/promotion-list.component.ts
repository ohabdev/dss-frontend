import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { TostyService } from 'src/app/shared/services/tosty.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { PromotionService } from '../services/promotion.service';


@Component({
  selector: 'app-promotion-list',
  templateUrl: './promotion-list.component.html',
  styleUrls: ['./promotion-list.component.css']
})
export class PromotionListComponent implements OnInit {
  public isLoading: boolean = true;
  public items: any = [];
  public pagination: any = [];
  Toasty: any;

  constructor(
    private promotionService: PromotionService,
    private toasty: TostyService
  ) {
    this.Toasty = toasty;
  }

  ngOnInit(): void {
    this.getPageDataFn();
  }

  getPageDataFn(page: number = 1) {
    this.isLoading = true;
    this.promotionService.getList(page).then((res) => {
      this.items = res.data.data;
      this.pagination = res.data;
      delete this.pagination['data'];
      this.isLoading = false;
    });
  }

  getApprovalStatus(status:number){
    if(status==1){
      return 'Pending';
    }else if(status==2){
      return 'Forwarded';
    }else if(status==3){
      return 'Approved';
    }else {
      return 'Rejected';
    }
  }

  deletePromotion(id: any) {
    
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.promotionService
          .deletePromotion(id)
          .then((res) => {
            this.getPageDataFn();
            Swal.fire({
              icon: 'success',
              title: 'Deleted!',
              text: 'Your imaginary file has been deleted.',
              showCancelButton: false,
              showConfirmButton: false,
              timer: 1000,
            });
          });
      }
    }).catch((res) => {
      this.isLoading = false;
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!');
    });
  }
}

