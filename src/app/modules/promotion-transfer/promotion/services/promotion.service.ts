import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private Promotion: any = null;
  private getPromotion: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('promotion-proposal?page='+page).toPromise();
  }

  getSelectOptionList(query:string) {
    return this.restangular.all('auth').customGET(query).toPromise();
  }


  createPromotion(data: any): Promise<any> {
    return this.restangular.all('auth/promotion-proposal').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  createPromotionDetails(data: any): Promise<any> {
    return this.restangular.all('auth/promotion-proposal-detail').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  updatePromotionDetails($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/promotion-proposal-detail',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  createPromotionNotes(data: any, current_position:any): Promise<any> {
    return this.restangular.all('auth/promotion-proposal-note/'+current_position).post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  
  


  singlePromotion($id): Promise<any> {
    return this.restangular.all('auth/promotion-proposal').customGET($id).toPromise();
  }

  updatePromotion($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/promotion-proposal',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deletePromotion($id): Promise<any> {
    return this.restangular.one('auth/promotion-proposal/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}
