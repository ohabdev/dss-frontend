import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { from } from 'rxjs';
import { PromotionService } from '../services/promotion.service';

@Component({
  selector: 'app-create-promotion',
  templateUrl: './create-promotion.component.html',
  styleUrls: ['./create-promotion.component.css']
})
export class CreatePromotionComponent implements OnInit {
  public isLoading: boolean = true;
  private promotionManagementService: PromotionService;
  private Toasty: TostyService;
  public promotion_proposal_id = '';
  public activeCreatePromotionEmployee:boolean = true;
  public activeAddPromotionEmployee:boolean = false;
  
  public PromotionData = {
    memo_no: '',
    memo_date: '',
    issue_no: '',
    promotion_date: '',
    posting_type_id: '',
    pay_grade_id: '',
    pay_scale: '',
    basic_pay: '',
    document_path: '',
    current_position: '',
    approval_status: 1,
    status: 1,
  };
  public PromotionDetailsData = {
    promotion_proposal_id: this.promotion_proposal_id,
    employee_id: '',
    previous_designatition_id: '',
    previous_office_id: '',
    proposed_designation_id: '',
    proposed_office_id: '',
  }
 
  public PromotionDetailsDataList: any = {};
  public office_list: any = [];
  public posting_type_list: any = [];
  public pay_grade_list: any = [];
  public employee_list: any = [];
  public designatition_list: any = [];
  public submitted: boolean = false;
  router: any;
  private _location: any;

  
  constructor(
    promotionManagementService: PromotionService,
    private toasty: TostyService,
    private location: Location
  ) {
    this.promotionManagementService = promotionManagementService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
    this.promotionManagementService.getSelectOptionList('office').then((res) => {
      this.office_list = this.genList(res.data.data,'office_name');
    });

    this.promotionManagementService.getSelectOptionList('salary-grade/list').then((res) => {
      this.pay_grade_list = res.data;
    });
    
    this.promotionManagementService.getSelectOptionList('posting-type/list').then((res) => {
      this.posting_type_list = res.data;
    });
    
    this.promotionManagementService.getSelectOptionList('employee').then((res) => {
      this.employee_list =  this.genList(res.data.data);
    });
    
    this.promotionManagementService.getSelectOptionList('designation/list').then((res) => {
      this.designatition_list = this.genList(res.data);
    });

    this.isLoading = false;

  }


  createPromotionOnSubmit(promotionFrm: NgForm) {
    this.submitted = true;   

    if (promotionFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.isLoading = true;
    this.promotionManagementService.createPromotion(
      this.PromotionData
    ).then((resp) => {
      this.isLoading = false;
      this.Toasty.showSuccess('Successfully Created Promotion Memo: '+resp.memo_no, 'Success');
      this.promotion_proposal_id = resp.id;
      this.activeCreatePromotionEmployee = false;
      this.activeAddPromotionEmployee = true;
      this.PromotionDetailsData.promotion_proposal_id = this.promotion_proposal_id;
    }).catch((res) => {
      this.isLoading = false;
      console.log(res.data.message);
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      return;
    });
  } 

  addPromotionEmployeeOnSubmit(promotionEmployeeFrm: NgForm) {
    console.log(this.PromotionDetailsDataList);
    if (promotionEmployeeFrm.invalid || this.isEmptyObject(this.PromotionDetailsDataList)) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.isLoading = true;
    this.promotionManagementService.createPromotionDetails(
      this.PromotionDetailsDataList
    ).then((resp) => {
      this.isLoading = false;
      this._location.back();
      this.Toasty.showSuccess('Successfully Added Employee to Promotion Memo: '+this.PromotionData.memo_no, 'Success');
    }).catch((res) => {
      this.isLoading = false;
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      return;
    });
  } 

  
  // addPromotionNotes() {
  //   this.promotionManagementService.createPromotionNotes(
  //     this.PromotionDetailsDataList[2],
  //     this.PromotionData.current_position
  //   ).then((resp) => {
  //     //this._location.back();
  //     this.Toasty.showSuccess('Successfully Added Notes to Promotion Memo'+this.PromotionData.memo_no, 'Success');
  //   }).catch((res) => {
  //     res.data.errors.forEach((_error: any) => {
  //       this.Toasty.showError(_error.message[0], 'Sorry!',);
  //     });
  //     return;
  //   });
  // } 

  

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  addPromotionEmployee(){   
    if(!this.PromotionDetailsData.employee_id || !this.PromotionDetailsData.previous_designatition_id || !this.PromotionDetailsData.previous_office_id || !this.PromotionDetailsData.proposed_designation_id || !this.PromotionDetailsData.proposed_office_id){
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.PromotionDetailsDataList[this.PromotionDetailsData.employee_id] = this.PromotionDetailsData;
    this.PromotionDetailsData = {
      promotion_proposal_id: this.promotion_proposal_id,
      employee_id: '',
      previous_designatition_id: '',
      previous_office_id: '',
      proposed_designation_id: '',
      proposed_office_id: '',
    }
  }

  editPromotionEmployee(data:any){
    if(data){
      this.PromotionDetailsData = data;
    }
  }

  deletePromotionEmployee(index:number){
    if(this.PromotionDetailsDataList[index]){
      delete this.PromotionDetailsDataList[index];
    }
  }

  genList(data:any,valueLabel:string='name'){
    let newArray:any =[];
    data.forEach(element => {
      newArray[element.id] = element[valueLabel];
    });
    return newArray;
  }
  
  
}
