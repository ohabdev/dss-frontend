import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PromotionListComponent } from './promotion-list/promotion-list.component';
import { CreatePromotionComponent } from './create-promotion/create-promotion.component';
import { EditPromotionComponent } from './edit-promotion/edit-promotion.component';
import { ViewPromotionComponent } from './view-promotion/view-promotion.component';


const routes: Routes = [{
  path: 'list',
  component: PromotionListComponent,
  data: {title: 'Promotion Information List'} 
},
{
  path: 'create',
  component: CreatePromotionComponent,
  data: {title: 'Create Promotion Information'} 
},
{
  path: 'edit/:id',
  component: EditPromotionComponent,
  data: {title: 'Edit Promotion Information'} 
},
{
  path: 'view/:id',
  component: ViewPromotionComponent,
  data: {title: 'Promotion Information Details'} 
}
];

@NgModule({
  declarations: [PromotionListComponent, CreatePromotionComponent, EditPromotionComponent, ViewPromotionComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class PromotionModule { }
