import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { PromotionService } from '../services/promotion.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-view-promotion',
  templateUrl: './view-promotion.component.html',
  styleUrls: ['./view-promotion.component.css']
})
export class ViewPromotionComponent implements OnInit {
  public isLoading: boolean = true;
  private Promotion: PromotionService;
  private Toasty: TostyService;
  public PromotionData : any = [];
  public submitted: boolean = false;
  public id: any = false;
  private _location: any;
  href: string;

  constructor(
    promotionService: PromotionService,
    private toasty: TostyService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.Promotion = promotionService;
    this.Toasty = toasty;
    this._location = location;

    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');

      if(this.id){
        this.isLoading = true;
        this.Promotion.singlePromotion(this.id).then((res) => {
          this.isLoading = false;
          this.PromotionData = res.data;
        })
        .catch((res) => { 
          this.isLoading = false;
          console.log(res.data.message);
          return this.Toasty.showError(res.data.message, 'Sorry!',);
        });
      }

    });
  }

  
  getApprovalStatus(status:number){
    if(status==1){
      return 'Pending';
    }else if(status==2){
      return 'Forwarded';
    }else if(status==3){
      return 'Approved';
    }else {
      return 'Rejected';
    }
  }

  deletePromotion(id: any) {
    
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#dc3545',
      cancelButtonColor: '#1c7430',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.Promotion
          .deletePromotion(id)
          .then((res) => {
            Swal.fire({
              icon: 'success',
              title: 'Deleted!',
              text: 'Your imaginary file has been deleted.',
              showCancelButton: false,
              showConfirmButton: false,
              timer: 1000,
            });
            this._location.back();
          });
      }
    }).catch((res) => {
      this.isLoading = false;
      console.log(res.data.message);
      return this.Toasty.showError(res.data.message, 'Sorry!');
    });
  }
  
}