import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PromotionTransferRoutingModule } from './promotion-transfer-routing.module';
import { PromotionModule } from './promotion/promotion.module';
import { TransferModule } from './transfer/transfer.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PromotionTransferRoutingModule,
    PromotionModule,
    TransferModule
  ],
  exports: [
    RouterModule,
  ]
})
export class PromotionTransferModule { }
