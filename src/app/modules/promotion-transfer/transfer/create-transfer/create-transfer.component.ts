import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { TostyService } from 'src/app/shared/services/tosty.service';
import { from } from 'rxjs';
import { TransferService } from '../services/transfer.service';

@Component({
  selector: 'app-create-transfer',
  templateUrl: './create-transfer.component.html',
  styleUrls: ['./create-transfer.component.css']
})
export class CreateTransferComponent implements OnInit {
  public isLoading: boolean = true;
  private transferManagementService: TransferService;
  private Toasty: TostyService;
  public transfer_proposal_id = '';
  public activeCreateTransferEmployee:boolean = true;
  public activeAddTransferEmployee:boolean = false;
  
  public TransferData = {
    memo_no: '',
    memo_date: '',
    transfer_date: '',
    document_path: '',
    current_position: '',
  };
  public TransferDetailsData = {
    transfer_proposal_id: this.transfer_proposal_id,
    employee_id: '',
    previous_office_id: '',
    proposed_office_id: '',
  }
 
  public TransferDetailsDataList: any = {};
  public office_list: any = [];
  public employee_list: any = [];
  public submitted: boolean = false;
  router: any;
  private _location: any;

  
  constructor(
    transferManagementService: TransferService,
    private toasty: TostyService,
    private location: Location
  ) {
    this.transferManagementService = transferManagementService;
    this.Toasty = toasty;
    this._location = location;
  }

  ngOnInit(): void {
    this.transferManagementService.getSelectOptionList('office').then((res) => {
      this.office_list = this.genList(res.data.data,'office_name');
    });
   
    this.transferManagementService.getSelectOptionList('employee').then((res) => {
      this.employee_list =  this.genList(res.data.data);
    });
   
    this.isLoading = false;
  }


  createTransferOnSubmit(transferFrm: NgForm) {
    this.submitted = true;   
    
    if (transferFrm.invalid) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.isLoading = true;
    this.transferManagementService.createTransfer(
      this.TransferData
    ).then((resp) => {
      this.isLoading = false;
      this.Toasty.showSuccess('Successfully Created Transfer Memo: '+resp.memo_no, 'Success');
      this.transfer_proposal_id = resp.id;
      this.activeCreateTransferEmployee = false;
      this.activeAddTransferEmployee = true;
      this.TransferDetailsData.transfer_proposal_id = this.transfer_proposal_id;
    }).catch((res) => {
      console.log(res.data.message);
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      this.isLoading = false;
      return;
    });
  } 

  addTransferEmployeeOnSubmit(transferEmployeeFrm: NgForm) {
    console.log(this.TransferDetailsDataList);
    if (transferEmployeeFrm.invalid || this.isEmptyObject(this.TransferDetailsDataList)) {
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }
    this.isLoading = true;
    this.transferManagementService.createTransferDetails(
      this.TransferDetailsDataList
    ).then((resp) => {
      this.isLoading = false;
      this._location.back();
      this.Toasty.showSuccess('Successfully Added Employee to Transfer Memo: '+this.TransferData.memo_no, 'Success');
    }).catch((res) => {
      res.data.errors.forEach((_error: any) => {
        this.Toasty.showError(_error.message[0], 'Sorry!',);
      });
      this.isLoading = false;
      return;
    });
  } 


  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  addTransferEmployee(){   
    if(!this.TransferDetailsData.employee_id || !this.TransferDetailsData.previous_office_id || !this.TransferDetailsData.proposed_office_id){
      return this.Toasty.showError('Your Input is Invalid!', 'Error!');
    }

    this.TransferDetailsDataList[this.TransferDetailsData.employee_id] = this.TransferDetailsData;
    this.TransferDetailsData = {
      transfer_proposal_id: this.transfer_proposal_id,
      employee_id: '',
      previous_office_id: '',
      proposed_office_id: '',
    }
  }

  editTransferEmployee(data:any){
    if(data){
      this.TransferDetailsData = data;
    }
  }

  deleteTransferEmployee(index:number){
    if(this.TransferDetailsDataList[index]){
      delete this.TransferDetailsDataList[index];
    }
  }

  genList(data:any,valueLabel:string='name'){
    let newArray:any =[];
    data.forEach(element => {
      newArray[element.id] = element[valueLabel];
    });
    return newArray;
  }
}