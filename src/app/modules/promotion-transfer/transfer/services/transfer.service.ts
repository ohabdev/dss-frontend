import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  static getAll(): any {
    throw new Error('Method not implemented.');
  }

  private alias: any = null;
  private Transfer: any = null;
  private getTransfer: any;
  constructor(private restangular: Restangular) { }

  getList(page:number=1) {
    return this.restangular.all('auth').customGET('transfer-proposal?page='+page).toPromise();
  }

  getSelectOptionList(query:string) {
    return this.restangular.all('auth').customGET(query).toPromise();
  }


  createTransfer(data: any): Promise<any> {
    return this.restangular.all('auth/transfer-proposal').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  createTransferDetails(data: any): Promise<any> {
    return this.restangular.all('auth/transfer-proposal/addEmployee').post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  createTransferNotes(data: any, current_position:any): Promise<any> {
    return this.restangular.all('auth/transfer-proposal-note/'+current_position).post(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  
  


  singleTransfer($id): Promise<any> {
    return this.restangular.all('auth/transfer-proposal').customGET($id).toPromise();
  }

  updateTransfer($id: any, data: any): Promise<any> {
    return this.restangular.one('auth/transfer-proposal',$id).customPUT(data).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

  deleteTransfer($id): Promise<any> {
    return this.restangular.one('auth/transfer-proposal/').customDELETE($id).toPromise()
      .then((resp) => {
        return resp.data;
    });
  }

   

    
}