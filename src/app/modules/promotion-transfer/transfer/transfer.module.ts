import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransferListComponent } from './transfer-list/transfer-list.component';
import { CreateTransferComponent } from './create-transfer/create-transfer.component';
import { EditTransferComponent } from './edit-transfer/edit-transfer.component';
import { TransferViewComponent } from './transfer-view/transfer-view.component';


const routes: Routes = [{
  path: 'list',
  component: TransferListComponent,
  data: {title: 'Transfer Information List'} 
},
{
  path: 'create',
  component: CreateTransferComponent,
  data: {title: 'Create Transfer Information'} 
},
{
  path: 'edit/:id',
  component: EditTransferComponent,
  data: {title: 'Edit Transfer Information'} 
},
{
  path: 'view/:id',
  component: TransferViewComponent,
  data: {title: 'Transfer Information Details'} 
}
];

@NgModule({
  declarations: [TransferListComponent, CreateTransferComponent, EditTransferComponent, TransferViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class TransferModule { }
