import {RouterModule} from "@angular/router";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { PaginationComponent } from './pagination/pagination/pagination.component'; 

import { StatusLabelPipe, DateTimePipe} from './pipes';


@NgModule({
  declarations: [
    FooterComponent, 
    HeaderComponent, 
    SpinnerComponent, 
    SideBarComponent,     
    BreadcrumbComponent,
    StatusLabelPipe,
    PaginationComponent,
    DateTimePipe,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    FooterComponent, 
    HeaderComponent, 
    SpinnerComponent, 
    SideBarComponent, 
    BreadcrumbComponent,
    PaginationComponent,
    StatusLabelPipe
  ]
})
export class SharedModule { }
