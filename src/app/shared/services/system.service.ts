import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SystemService {
  
  public appConfig: any = null;

  private _getConfig: any;
  
  constructor(private restangular: Restangular) { }

  configs(): Promise<any> {
    if (this.appConfig) {
      return Promise.resolve(this.appConfig);
    }

    if (this._getConfig && typeof this._getConfig.then === 'function') {
      return this._getConfig;
    }
  }

  
  setUserLang(lang: string) {
    window.appConfig.userLang = lang;    
    localStorage.setItem('userLang', lang);
    return lang;
  }
  getUserLang(){
    return localStorage.getItem('userLang') || this.setUserLang('bn');
  }
  
 
   
}
