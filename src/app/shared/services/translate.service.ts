import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
 

export class TranslateService {

    translat$: Observable<any>;
    private translatSubject = new Subject<any>();
   
    constructor() {
        this.translat$ = this.translatSubject.asObservable();
    }

    setLanguage(data) {
        this.translatSubject.next(data);
        localStorage.setItem('lang',data);
    }

    getLanguage(){
        return localStorage.getItem('lang');
    }
    
}
