import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { SystemService } from '../services/system.service';

@Injectable()
export class ConfigResolver implements Resolve<Observable<any>> {
  constructor(private systemService: SystemService) {}
 
  resolve(): any {
    return this.systemService.configs();
  }
}
