import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../modules/auth/auth.service';
import { TranslateService } from '../services/translate.service';
import { SystemService } from 'src/app/shared/services/system.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private systemService: SystemService;
  userLang: any;
  currentUser: any;
  private userLoadedSubscription: Subscription;

  constructor(private authService: AuthService, private translatService: TranslateService, private system: SystemService) { 
    this.userLoadedSubscription = authService.userLoaded$.subscribe(data => this.currentUser = data);
    this.systemService = system;  
  }

  ngOnInit() {
    this.userLang = this.systemService.getUserLang();
    if (this.authService.isLoggedin()) {
      this.authService.getCurrentUser().then(resp => this.currentUser = resp);
    }
  }

  changeLang(lang: any) {
    this.systemService.setUserLang(lang);
    this.userLang = lang;
  }
  
  
  logout() {
    this.authService.logout();
    window.location.href = '/';
  }

}
