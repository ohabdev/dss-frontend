import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { empty } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent implements OnInit {
  public pageNumberArray: any = [];
  public currentPage: number = 1;
  public showingNumberOfPageLimit: number = 11;

  @Input() pagination: any;

  @Output() selectPage: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.pagination) {
      this.genPageNumber();
    }
  }

  sendPageNumberFn(page: number = 0) {
    if(page){
      if (page < 1 || page > this.pagination.last_page) {
        this.showMsg();
      } else if (this.currentPage != page) {
        this.currentPage = page;
        this.selectPage.emit(page);
      }
    }
  }

  showMsg() {
    Swal.fire({
      icon: 'warning',
      title: 'Please enter: 1 to ' + this.pagination.last_page,
      //text: 'Message',
      showCancelButton: false,
      showConfirmButton: true,
      //timer: 1000,
    });
  }

  goToPage(page: number) {
    console.log(page);
  }

  genPageNumber() {
    if (this.pagination.last_page !== undefined) {
      let showingNumberOfPageStart: number = 1;
      let currentPage: number = this.pagination.current_page;
      let showingNumberOfPageEnd: number = this.pagination.last_page;
      let showingNumberOfPageLast: number = this.pagination.last_page;
      let midNumber: number = Math.floor(this.showingNumberOfPageLimit / 2);

      if (showingNumberOfPageLast > this.showingNumberOfPageLimit) {
        if (currentPage + midNumber < showingNumberOfPageLast) {
          showingNumberOfPageEnd = currentPage + midNumber;
        } else {
          showingNumberOfPageStart =
            currentPage - this.showingNumberOfPageLimit;
        }

        if (currentPage + midNumber > showingNumberOfPageLast) {
          showingNumberOfPageStart =
            showingNumberOfPageLast - (this.showingNumberOfPageLimit - 1);
        } else if (currentPage - midNumber > 1) {
          showingNumberOfPageStart = currentPage - midNumber;
        } else {
          showingNumberOfPageEnd = this.showingNumberOfPageLimit;
        }
      }

      let pageArray: any = [];
      for (var i = showingNumberOfPageStart; i <= showingNumberOfPageEnd; i++) {
        pageArray[i] = i;
      }

      this.pageNumberArray = pageArray;
      this.pageNumberArray = this.pageNumberArray.slice(
        showingNumberOfPageStart,
        showingNumberOfPageEnd + 1
      );
    }
  }
}
