import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../modules/auth/auth.service';


@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.css']
})
export class StarterComponent implements OnInit {
  currentUser: any;
  private userLoadedSubscription: Subscription;

  constructor(private authService: AuthService) { 
    this.userLoadedSubscription = authService.userLoaded$.subscribe(data => this.currentUser = data);
    
  }

  ngOnInit() {
      
    if (this.authService.isLoggedin()) {
      this.authService.getCurrentUser().then(resp => this.currentUser = resp);
    }
    // console.log(this.currentUser)
  }

  logout() {
    this.authService.logout();
    window.location.href = '/';
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.userLoadedSubscription.unsubscribe();
  }

}
