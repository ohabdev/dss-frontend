import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { StarterComponent } from './starter.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Dashboard'
  },
  component: StarterComponent,
  pathMatch: 'full'
}];

@NgModule({
  
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [StarterComponent]
})
export class StarterModule { }
