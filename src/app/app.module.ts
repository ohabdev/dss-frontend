import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { RestangularModule } from 'ngx-restangular';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './material.module';

import { TranslateLoader, TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { BlankComponent } from './layouts/blank/blank.component';
import { FullComponent } from './layouts/full/full.component';

// Shared Components
import { SharedModule } from './shared/shared.module';

// Auth Module
import { AuthService } from './modules/auth/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { SystemService } from './shared/services/system.service'
import { ConfigResolver } from './shared/resolver/config.resolver';

import { Approutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { from } from 'rxjs';

// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

// Function for setting the default restangular configuration
export function RestangularConfigFactory(RestangularProvider) {
  // TODO - change default config
  RestangularProvider.setBaseUrl(window.appConfig.apiProxy || window.appConfig.apiBaseUrl);
  RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
    // Auto add token to header
    headers.Authorization = 'Bearer ' + localStorage.getItem('accessToken');
    headers.platform = window.appConfig.platform;
    headers['X-localization'] = localStorage.getItem('userLang');

    return {
      headers: headers
    };
  });

  RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {
    // force logout and relogin
    if (response.status === 401) {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('isLoggedin');
      window.location.href = '/auth/login';

      return false; // error handled
    }

    return true; // error not handled
  });
}

// Tostr Settings 
const ToastrModuleSetings:any = {
        closeButton: false,
        debug: false,
        newestOnTop: false,
        progressBar: true,
        positionClass: 'toast-top-right',
        preventDuplicates: true,
        onclick: null,
        showDuration: 300,
        hideDuration: 1000,
        timeOut: 3000,
        extendedTimeOut: 1000, 
        hideEasing: 'linear',
        showMethod: 'slideDown',
        hideMethod: 'slideUp',
      };

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    BlankComponent, 
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(Approutes, { useHash: false, enableTracing: false}),
    ToastrModule.forRoot(ToastrModuleSetings),
    HttpClientModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        multi: true, // This was causing the error
        deps: [HttpClient],
        useValue: TranslatePipe
      }
    }),
    SharedModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    SystemService,
    ConfigResolver
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
